Both allocators
	Need a list of active addresses
		Stored as an std::map or std::unordered_map

	Need a way to get the high-water mark and current-water mark

	Need a way to iterate over all active addresses
	Need a way to delete all active addresses
	Need a way to delete some active addresses

	Need a way to clean addresses during deletion

ArenaAllocator
	Need to get addresses from a big pool
	Needs to keep track of previously allocated addresses

FreeAllocator
	Use ::operator new to get addresses
	Doesn't need to keep track of previously allocated addresses

Shouldn't be able to create both ArenaAllocator<T, N> and FreeAllocator<T>
Shouldn't be able to create both ArenaAllocator<T, N> and ArenaAllocator<T, N + 1>

Have an Allocator<T> interface that inherits from Singleton
	Since ::Get() would return an Allocator<T> reference how do we avoid virtual functions?
		Could static_cast downward, since we (should) know what the actual type of the Allocator is

Both should be usable be a corresponding Factory<T> or Factory<T, N>



template<typename T>
class AddressList
{
	public:
		void AddAddress( T* addr, Debug::Location loc )
		{
			dbAssert( InList(addr) == false, "Address already in list!" );

			activeAddrs.insert( std::make_pair(addr, loc) );

			dbAssert( InList(addr) == true, "Address not added to list!" );
		}

		void RemoveAddress( T* addr )
		{
			dbAssert( InList(addr) == true, "Address not in list!" );

			activeAddrs.erase(addr);

			dbAssert( InList(addr) == true, "Address not removed from list!" );
		}

		bool InList( T* addr )
		{
			return activeAddrs.find(addr) != activeAddrs.end()
		}

		int DeleteAll()
		{
			int numDeleted = DeleteSome( [](){ return true; } )
			dbAssert( activeAddrs.size() == 0, "Didn't delete everything!" );

			return numDeleted;
		}

		int DeleteSome( Pred )
		{
			int numDeleted = 0;
			auto i = activeAddrs.begin();
			while( i != activeAddrs.end() )
			{
				auto curr = i;
				++i;

				if( Pred(curr) ) {
					delete *curr;
					++numDeleted;
				}
			}

			return numDeleted;
		}		

		void ForEach( Fn )
		{
			for( addr : activeAddrs )
			{
				Fn( addr )
			}
		}

	private:
		std::map<T*, Debug::Location> activeAddrs;
		size_t maxActiveAddrs
};

template<typename T>
class ArenaAllocator
{
	public:

	private:
};

template<typenameT>
class FreeAllocator
{
	public:

	private:	
};


Factory<GameObject>::Get().Create<Foo>( dbLocation(), ... )

#define GameObjectFactory Factory<GameObject>::Get()
#define GameObjectFactoryCreate( Type, ...) GameObjectFactory.Create<Type>( dbLocation(), ... )

template<typename T>
SmartCreate(...)
{
	T* t = new T(...);
	createdObjects.push_back( t )
}

template<typename T>
SmartDelete( obj )
{
	delete obj;
	createdObjects.remove( obj )
}

template<typename T>
DumbCreate(...)
{
	T* addr = Allocator<T>::Allocate();
	T* t = new (addr) T(...);
	createdObjects.push_back( t )
}


If we're using the Allocator:
	calling "new" and "delete" use the correct allocator

If we're not using the Allocator:
	calling "new" and "delete" use the regular functions, which still do the correct thing.

Issues:
	Getting line numbers is still a hassle (but it will probably be gross no matter what)
	Cannot use memory pool with objects whos source code we can't change.

For dumb objects, store reference to Allocator with object pointer


All factories use Allocator internally, so no need to have memory leak checking.
	Factory have single list of many child classes, so need functions to iterate over all

// Detach all Components
GameObjectFactory.ForEach(
	[&](GameObject* obj ) {
		obj.RemoveAllComponents();
	}
);

// Delete all Components
ComponentFactory.DeleteAll();

// Delete all GameObjects
GameObjectFactory.DeleteAll()



Allocator
	Override "new" and "delete" for a single class.
	Only works for classes that we can modify the source code for.
	Keeps a list of created objects
	Can either use a memory pool or an allocator.

Factory
	Create and destroy objects of a single type.
	Works with any type.
	Uses an Allocator internally, so doesn't need to keep track of anything.
		Thus will also use a memory pool or an allocator.

AggregateFactory
	Create and destroy objects of a class hierarchy
	For example, used to iterate over all GameObjects or all Components
	Stores all created objects in a big list as pointers to the base class.
	For objects that use the Allocator
		use "new" and "delete" directly on the base pointers, since the correct allocator will be accesses

	For objects that don't use the Allocator
		Use the Factory to create objects and store a reference to the Factory so we know where to delete from when destorying objects (we do a static_cast)