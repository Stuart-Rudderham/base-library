/*
 * Examples of functionality in IO/Print.hpp
 *
 * Author:      Stuart Rudderham
 * Created:     June 5, 2014
 * Last Edited: June 5, 2014
 */

#include "IO/Print.hpp"     // ioPrint, ioPrintln, ioCout, ioCerr, ioVerbose
#include <iostream>         // std::cout, std::cerr
#include <sstream>          // std::stringstream
#include <ios>              // std::hex, std::showbase

int main()
{
    // Prints "This is ioPrint example #1" to stdout
    ioPrint( std::cout, "This is ioPrint example ", '#', 1 );

    // Prints "-1 0 12.5\n" to stderr
    ioPrintln( std::cerr, -1, " 0 ", '1', 2.5f );

    // Can print to any std::ostream
    std::stringstream stream;
    ioPrint( stream, "Printing to a stringstream" );    // No newline at the end
    ioPrintln( std::cout, stream.str() );               // Newline at the end

    // Using formatting flags. Prints "0xff"
    ioPrintln( std::cout, std::hex, std::showbase, 255u );

    // Print to stdout with a newline automatically appended.
    ioCout( "Printing to std::cout" );

    // Print to stderr with a newline automatically appended.
    ioCerr( "Printing to std::cout" );

    // If ENABLE_VERBOSE_OUTPUT is true then print to stdout. If it is false
    // then nothing is printed and the arguments aren't evaluated. A newline
    // is automatically added at the end.
    ioVerbose( "This may or may not be printed. Useful for debugging output" );

    // Can call the function directly if desired, but no real reason
    // not to use the wrapper macros. A newline is not automatically added.
    L3::IO::Print( std::cout, "Direct call" );
}
