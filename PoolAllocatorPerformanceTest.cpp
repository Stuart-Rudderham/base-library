//  g++ -std=c++11 -O3 -Wall -Wextra -Wundef Main.cpp -I. -DTEST_POOL_ALLOCATOR=1 -DENABLE_ASSERTS=0 -DRELEASE -DENABLE_MEMORY_CLEARING=0

#include "Debug/Debug.hpp"
#include "Allocator/PoolAllocator.hpp"
#include <cstdlib>
#include <array>

const unsigned int FooPoolSize = 10000;

unsigned int NumNewCalls    = 0;
unsigned int NumDeleteCalls = 0;

unsigned int Sum = 0;

struct Foo
{
    #if TEST_POOL_ALLOCATOR
        USE_POOL_ALLOCATOR(Foo, FooPoolSize);
    #endif

    Foo( int v )
        : value( v )
    {
        
    }

    int GetValue() const
    {
        return value;
    }

    int value;
    char space[1024];
};

void CreateFoo( Foo*& foo )
{
    ++NumNewCalls;
    int value = NumNewCalls % 1000;    

    #if TEST_POOL_ALLOCATOR
        foo = new ( dbLocation() ) Foo( value );
    #else
        foo = new Foo( value );
    #endif

    Sum += foo->GetValue();
}

void DestroyFoo( Foo*& foo )
{
    Sum -= foo->GetValue();
    ++NumDeleteCalls;

    delete foo;
    foo = nullptr;
}

int main()
{
    srand(10);
    const unsigned int numIters = 10000000;
    std::array<Foo*, FooPoolSize> arr;

    // Fill the array completely.
    for( unsigned int i = 0; i < FooPoolSize; ++i ) {
        CreateFoo( arr[i] );
    }

    // Clear it completely.
    for( unsigned int i = 0; i < FooPoolSize; ++i ) {
        DestroyFoo( arr[i] );
    }

    // Random new and deletes.
    for( unsigned int i = 0; i < numIters; ++i ) {
        int index = rand() % FooPoolSize;

        if( arr[index] == nullptr ) {
            CreateFoo( arr[index] );
        }
        else {
            DestroyFoo( arr[index] );
        }
    }

    // Clean up everything.
    for( unsigned int i = 0; i < FooPoolSize; ++i ) {
        if( arr[i] != nullptr ) {
            DestroyFoo( arr[i] );
        }
    }

    // Print summary.
    dbPrintToConsole( "Number of iterations - ", numIters );
    dbPrintToConsole( "sizeof(Foo) = ", sizeof(Foo) );
    dbPrintToConsole( "Number of calls to Foo::operator new - ", NumNewCalls );
    dbPrintToConsole( "Number of calls to Foo::operator delete - ", NumDeleteCalls );
    dbPrintToConsole( "Sum = ", Sum );

    return 0;
}

#if 0
#include "Debug/Debug.hpp"
#include "Macros/Macros.hpp"
#include "Allocator/ArenaAllocator.hpp"
#include "Allocator/BasicAllocator.hpp"
#include "Factory/Factory.hpp"
#include "Singleton.hpp"
#include <tuple>

struct Foo
{
    SET_ARENA_ALLOCATOR_SIZE(10);
    USE_ARENA_ALLOCATOR(Foo);
};

struct Baz
{
    USE_BASIC_ALLOCATOR(Baz);
};

struct Bar
{
//    SET_ARENA_ALLOCATOR_SIZE(10);
//    USE_ARENA_ALLOCATOR(Bar<T>);
    ~Bar() {
        dbPrintToConsole("DESTROY");
    }

    void* operator new(size_t);
};
/*
void* Bar::operator new( size_t size )
{
    return nullptr;
}
*/
int operator *(const Foo& x, const Foo& y)
{
    return 0;
}

template<typename T>
class A
{
    char x[1024 * 1024];
};

int main( int argc, char** argv )
{
    dbPrintToConsole( "" );
    dbPrintToConsole( "/*" );
    dbPrintToConsole( " * Compiled on ", COMPILE_TIME, " by ", COMPILER_STRING );
    dbPrintToConsole( " */" );
    dbPrintToConsole( "" );

    //dbPrintToConsole( new Bar() );
    //dbPrintToConsole( new Foo() );

    // What I want:
    //     int* a = new int(10);                        // Use Allocator to allocate from pool and detect memory leaks.
    //     int* x = Factory<int>::Get().Create(10);     // Use Factory to detect memory leaks.
    //
    // If it can work on an "int" then it can work on code I don't have access to, which is a very useful feature.
    //
    // Right now the Factory can work on arbitrary types, but the Allocator requires adding the USE_ARENA_ALLOCATOR macro.



    FactoryCreate( int, 10 );
    FactoryCreate( int, 11 );
    FactoryCreate( int, 12 );
    FactoryCreate( int, 13 );
    FactoryCreate( int, 14 );
    FactoryCreate( int, 15 );

FactoryDestroy( A<int>, FactoryCreate(A<int>) );

//    Factory<int>::Get().Create( dbLocation(), 10 );
//    Factory<int>::Get().Create( dbLocation(), 11 );
//    Factory<int>::Get().Create( dbLocation(), 12 );
//    Factory<int>::Get().Create( dbLocation(), 13 );


    //Baz* x = new Baz[10];
#if 1

    Foo* x = FactoryCreate( Foo );


//    dbPrintToConsole( "int = ", *x );

    Factory<Foo>::Get().ForEach( [&](Foo* f) {f;} );

    dbPrintToConsole(
        "DestroyingPred - ",
        Factory<Foo>::Get().DestroyAllObjectsThat(
            [&](Foo* f) -> bool {
                return f != nullptr;
            }
        )
    );

    dbPrintToConsole( "Destroying - ", Factory<Foo>::Get().DestroyAllObjects() );


    dbPrintToConsole(
        "DestroyingPred - ",
        Factory<int>::Get().DestroyAllObjectsThat(
            [&](int* f) -> bool {
                return *f % 2 == 0;
            }
        )
    );


    dbAssert( true, "Message" );

    dbWarn( false, "But this warning should" );

    //dbCrash( "Who exits the program properly?" );
#endif

    return 0;
}
#endif
