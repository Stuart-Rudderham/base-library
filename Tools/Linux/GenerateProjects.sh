#!/bin/bash

# This script will autogenerate Makefiles and Code::Blocks IDE projects to build the executable (both Debug and Release modes)
# It is only intended to work on a Unix-like operating system
# It is intended to be run from the Tools directory
# It assumes that CMake is installed and is in the PATH

ROOT_DIR="`pwd`/../.."
PROJECT_DIR="$ROOT_DIR/Build"
CMAKELISTS_DIR="$ROOT_DIR"

CODEBLOCK_DEBUG_PROJECT_DIR="$PROJECT_DIR/Linux/CodeBlocks/Debug"
CODEBLOCK_RELEASE_PROJECT_DIR="$PROJECT_DIR/Linux/CodeBlocks/Release"

SUBLIME_DEBUG_PROJECT_DIR="$PROJECT_DIR/Linux/SublimeText/Debug"
SUBLIME_RELEASE_PROJECT_DIR="$PROJECT_DIR/Linux/SublimeText/Release"

MAKEFILE_DEBUG_PROJECT_DIR="$PROJECT_DIR/Linux/Makefile/Debug"
MAKEFILE_RELEASE_PROJECT_DIR="$PROJECT_DIR/Linux/Makefile/Release"

# Create the directories (if they don't exist)
mkdir --parents ${CODEBLOCK_DEBUG_PROJECT_DIR}
mkdir --parents ${CODEBLOCK_RELEASE_PROJECT_DIR}

mkdir --parents ${SUBLIME_DEBUG_PROJECT_DIR}
mkdir --parents ${SUBLIME_RELEASE_PROJECT_DIR}

mkdir --parents ${MAKEFILE_DEBUG_PROJECT_DIR}
mkdir --parents ${MAKEFILE_RELEASE_PROJECT_DIR}

# Run the cmake command in each directory
cd ${CODEBLOCK_DEBUG_PROJECT_DIR}
cmake -G "CodeBlocks - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ${CMAKELISTS_DIR}
echo ""

cd ${CODEBLOCK_RELEASE_PROJECT_DIR}
cmake -G "CodeBlocks - Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ${CMAKELISTS_DIR}
echo ""

cd ${SUBLIME_DEBUG_PROJECT_DIR}
cmake -G "Sublime Text 2 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ${CMAKELISTS_DIR}
echo ""

cd ${SUBLIME_RELEASE_PROJECT_DIR}
cmake -G "Sublime Text 2 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ${CMAKELISTS_DIR}
echo ""

cd ${MAKEFILE_DEBUG_PROJECT_DIR}
cmake -DCMAKE_BUILD_TYPE=Debug ${CMAKELISTS_DIR}
echo ""

cd ${MAKEFILE_RELEASE_PROJECT_DIR}
cmake -DCMAKE_BUILD_TYPE=Release ${CMAKELISTS_DIR}
echo ""
