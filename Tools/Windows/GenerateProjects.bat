@echo off

:: This script will autogenerate Visual Studio and Code::Blocks IDE projects to build the executable (both Debug and Release modes)
:: It is only intended to work on Windows
:: It is intended to be run from the Tools directory
:: It assumes that CMake is installed and is in the PATH

set START_DIR=%CD%
set ROOT_DIR=%START_DIR%\..\..

set PROJECT_DIR=%ROOT_DIR%\Build
set CMAKELISTS_DIR=%ROOT_DIR%

set CODEBLOCK_DEBUG_PROJECT_DIR=%PROJECT_DIR%\Windows\CodeBlocks\Debug
set CODEBLOCK_RELEASE_PROJECT_DIR=%PROJECT_DIR%\Windows\CodeBlocks\Release

set SUBLIME_DEBUG_PROJECT_DIR=%PROJECT_DIR%\Windows\SublimeText\Debug
set SUBLIME_RELEASE_PROJECT_DIR=%PROJECT_DIR%\Windows\SublimeText\Release

:: CMake can generate proper Visual Studio projects, no need for separate Debug and Release directories
set VISUAL_STUDIO_PROJECT_DIR=%PROJECT_DIR%\Windows\VisualStudio\

:: Create the directories (if they don't exist)
mkdir %CODEBLOCK_DEBUG_PROJECT_DIR%
mkdir %CODEBLOCK_RELEASE_PROJECT_DIR%

mkdir %SUBLIME_DEBUG_PROJECT_DIR%
mkdir %SUBLIME_RELEASE_PROJECT_DIR%

mkdir %VISUAL_STUDIO_PROJECT_DIR%

::  Run the cmake command in each directory
cd %CODEBLOCK_DEBUG_PROJECT_DIR%
cmake -G "CodeBlocks - MinGW Makefiles" -DCMAKE_BUILD_TYPE=Debug %CMAKELISTS_DIR%
echo.

cd %CODEBLOCK_RELEASE_PROJECT_DIR%
cmake -G "CodeBlocks - MinGW Makefiles" -DCMAKE_BUILD_TYPE=Release %CMAKELISTS_DIR%
echo.

cd %SUBLIME_DEBUG_PROJECT_DIR%
cmake -G "Sublime Text 2 - MinGW Makefiles" -DCMAKE_BUILD_TYPE=Debug %CMAKELISTS_DIR%
echo.

cd %SUBLIME_RELEASE_PROJECT_DIR%
cmake -G "Sublime Text 2 - MinGW Makefiles" -DCMAKE_BUILD_TYPE=Release %CMAKELISTS_DIR%
echo.

cd %VISUAL_STUDIO_PROJECT_DIR%
cmake -G "Visual Studio 12" -T "CTP_Nov2013" %CMAKELISTS_DIR%
echo.

:: Go back to the Tools directory, because Windows doesn't run the script in a seperate shell
cd %START_DIR%
