@echo OFF
setlocal

:: Put cl.exe (if it exists) on the PATH
call "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" x86
:: call %VS100COMNTOOLS%vsvars32.cat"


:: Move to the Testing directory and run the entire suite.
cd "..\..\Testing\UnitTests"
python UnitTestDriver.py %*

endlocal

:: Don't exit until a key is pressed. This will keep the cmd window open if the script was run by double-clicking on it.
pause
