#
# The driver file for unit testing.
#
# Author:      Stuart Rudderham
# Created:     September 30, 2013
# Last Edited: June 3, 2014
#

import concurrent.futures       # ThreadPoolExecutor
import datetime                 # now
import distutils.core           # copy_tree
import multiprocessing          # cpu_count
import os                       # getcwd, walk, path.join
import re
import sys                      # path, exit, argv

# Add the parent directory to the search path so we can include
# TestingCommon. This feels like a gross solution, but is apparently
# the idiomatic way to include a file located in a parent directory.
sys.path.append( os.pardir )
from TestingCommon import *

#
# A class that holds the results of running a single test with
# a single compiler.
#
class TestResult:
    def __init__( self ):
        self.timer          = None      # [WallTimer]
        self.state          = None      # [string] {Pass, Fail, Skip}
        self.expectedResult = None      # [string] {CompilePass, CompileFail, TestPass, TestFail}
        self.actualResult   = None      # [string] {CompilePass, CompileFail, TestPass, TestFail, RegexMatchFail, RegexNonMatchFail, Timeout}
        self.compiler       = None      # [string]
        self.name           = None      # [string]

    def SetResult( self, actualResult ):
        self.actualResult = actualResult
        self.state = "Pass" if self.expectedResult == self.actualResult else "Fail"

    def SetSkipped( self, skipReason ):
        self.skipReason = skipReason
        self.state = "Skip"

    def __str__( self ):
        # Generate the part of the result string that has the same format regardless of the test result.
        # Is of the format "<TestResult> [<CompilerName>] <TestName>".
        resultString = "{} [{:<7}] [{:>4}ms] {}".format(
            self.state,
            self.compiler,
            self.timer.elapsed_ms,
            self.name
        )

        # If the test failed then add the expected and actual result.
        if self.state == "Fail":
            resultString += " (Expected: {}, Actual: {})".format(
                self.expectedResult,
                self.actualResult
            )
        # If the test was skipped then add the reason for the skip.
        elif self.state == "Skip":
            resultString += " ({})".format(
                self.skipReason
            )

        return resultString

#
# A class that holds the results of running an entire testsuite
# with all installed compilers
#
class TestsuiteResult:
    def __init__( self ):
        self.passingTests = []
        self.failingTests = []
        self.skippedTests = []

    def AddTestResult( self, testResult ):
        if testResult.state == "Pass":
            self.passingTests.append( testResult )

        elif testResult.state == "Fail":
            self.failingTests.append( testResult )

        elif testResult.state == "Skip":
            self.skippedTests.append( testResult )

    def __str__( self ):
        # Calculate stats on test results
        numPass = len( self.passingTests )
        numFail = len( self.failingTests )
        numSkip = len( self.skippedTests )
        numTests = numPass + numFail + numSkip

        # They are the first line in the string. Return early if we didn't actually run anything.
        resultString = "Ran {} tests".format( numTests )

        if numTests == 0:
            return resultString + "\n"

        resultString += ", {} passed ({}%), {} failed ({}%), {} skipped ({}%)\n".format(
            numPass, int( float(numPass) / numTests * 100 ),
            numFail, int( float(numFail) / numTests * 100 ),
            numSkip, int( float(numSkip) / numTests * 100 )
        )

        # Add any failing tests
        if self.failingTests:
            resultString += "\nFailing Tests\n"
            for test in self.failingTests:
                resultString += "    {!s}\n".format( test )

        # Add any skipped tests
        if self.skippedTests:
            resultString += "\nSkipped Tests\n"
            for test in self.skippedTests:
                resultString += "    {!s}\n".format( test )

        # Return
        return "\n" + resultString

#
# Global variables.
#
CurrentTimeString = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

RootDir        = os.getcwd();
BaseTestDir    = os.path.join( RootDir, "Tests" )
BaseResultsDir = os.path.join( RootDir, "Results" )
ResultsDir     = os.path.join( BaseResultsDir, CurrentTimeString )

LogFilePath = os.path.join( ResultsDir, "results.log" )
LogFile     = None

UnitTestMain = os.path.join( RootDir, "UnitTestMain.cpp" )
SourceDir    = os.path.join( RootDir, os.pardir, os.pardir, "Source" )

def GetUnitTestCommandLine( compilerName, fileName, exeName ):
    """
    Purpose:
        This function returns, for the given compiler, the command line that is used for the unit tests.
        If the given compiler is unrecognized then an attempt is made to guess at a suitable command line.

    Parameters:
        [string] compilerName
            The name of the compiler executable (e.g. g++, cl.exe)

        [string] fileName
            The name of the test file to be force-included into UnitTestMain.cpp

        [string] exeName
            The name of the resulting executable

    Return:
        [list [string]]
            The command line, suitable for use with subprocess.call()
    """
    if compilerName == "g++" or compilerName == "clang++":
        return [
                 compilerName,                  # Compiler
                 UnitTestMain,                  # File with main()
                 "-Wall",                       # All warnings
                 "-Wextra",
                 "-Wundef",
                 "-Werror=undef",
                 "-Werror",                     # Warnings are errors
                 "-std=c++11",                  # Latest C++ standard
                 "-I", SourceDir,               # Add the Source directory to include path
                 "-o", exeName,                 # Set the executable name
                 "-include", fileName           # Force #include the test file
               ]

    elif compilerName == "cl.exe":
        return [
                 compilerName,                  # Compiler
                 UnitTestMain,                  # File with main()
                 "/W4",                         # All warnings
                 "/WX",                         # Warnings are errors
                 "/wd4127",                     # Disable warning "conditional expression is constant"
                 "/wd4702",                     # Disable warning "unreachable code"
                 "/wd4307",                     # Disable warning "integral constant overflow"
                 "/EHsc",                       # Define exception handling model to suppress warning
                 "/I" + SourceDir,              # Add the Source directory to include path
                 "/Fe" + exeName,               # Set the executable name
                 "/Fo" + exeName + ".obj",      # Set the object file name
                 "/FI" + fileName               # Force #include the test file
               ]

    else:
        return [ compilerName, fileName ]

#
# Functions to actually run the tests and testsuites.
#
def RunTestsuite( testsuiteDirectory, compilers ):
    """
    Purpose:
        This function runs all the tests found in the given testsuite directories
        with each of the given compilers.

    Parameters:
        [string] testsuiteDirectory
            The absolute path to the directory of the testsuite that should be run.

        [list [string]] compilers
            The compiler executables to run the tests with.

    Return:
        [TestsuiteResult]
            The results of running all the tests with each compiler.
    """
    tests = []

    # For each C++ file found add it to the list of tests to be run.
    for root, dirs, filenames in os.walk( testsuiteDirectory ):
        for f in filenames:
            if f.endswith('.cpp') == False:
                continue
            else:
                absoluteTestName = os.path.join( root, f )

                for compiler in compilers:
                    tests.append( (absoluteTestName, testsuiteDirectory, compiler) )

    # Make a folder in the Results directory that mirrors the structure in the Tests directory.
    # Use distutils rather than shutil because shutil.copytree fails if the destination folder
    # already exists.
    distutils.dir_util.copy_tree( testsuiteDirectory, ResultsDir )

    # Run each test on its own thread.
    numCores = multiprocessing.cpu_count()

    LogFile.info( "Running testsuite: {}".format(testsuiteDirectory) )
    LogFile.info( "" )
    LogFile.info( "Running with {} threads.".format(numCores) )
    LogFile.info( "" )

    testsuiteResult = TestsuiteResult()

    with concurrent.futures.ThreadPoolExecutor( max_workers = numCores ) as executor:
        testFutures = { executor.submit(RunSingleTest, test[0], test[1], test[2]) : test for test in tests }

        for future in concurrent.futures.as_completed( testFutures ):
            testResult = future.result()

            testsuiteResult.AddTestResult( testResult )
            LogFile.info( testResult )

    # Return the results.
    return testsuiteResult

def RunSingleTest( absoluteTestPath, testsuiteDirectory, compiler ):
    """
    Purpose:
        This function runs a single test with the given compiler.

    Parameters:
        [string] absoluteTestPath
            The absolute path to the test to be run.

        [string] testsuiteDirectory
            The testsuite directory the test is located in.

        [string] compiler
            The compiler to run the test with.

    Return:
        [TestResult]
            The result of running the test with the given compiler.
    """
    with WallTimer() as timer:
        # The path the the .cpp file relative to the testsuite directory
        # (e.g. Test-Foo-1/Test-Foo-1.cpp)
        relativeTestPath = os.path.relpath( absoluteTestPath, testsuiteDirectory )

        # The absolute path to the directory where the .cpp file is located.
        # (e.g. /home/stuart/Testing/Tests/Test-Foo-1)
        absoluteTestDirectoryPath = os.path.dirname( absoluteTestPath )

        # The name of the folder where the .cpp file is located.
        # (e.g. Test-Foo-1)
        relativeTestDirectoryPath = os.path.dirname( relativeTestPath );

        # An absolute path to the Results directory similar in structure to the Tests directory
        # (e.g. /home/stuart/Testing/Results/Test-Foo-1)
        absoluteResultsDirectoryPath = os.path.join( ResultsDir, relativeTestDirectoryPath )

        # Test name
        # (e.g. Test-Foo-1)
        testName = os.path.splitext(os.path.basename(absoluteTestPath))[0]

        # Absolute file paths for logging
        compileOutputAbsolutePath = os.path.join( absoluteResultsDirectoryPath, "_".join([testName, compiler, "compile_output"]) + ".log")
        programOutputAbsolutePath = os.path.join( absoluteResultsDirectoryPath, "_".join([testName, compiler, "program_output"]) + ".log")

        # Absolute file paths for the generated executable
        executableName =  "_".join([testName, compiler]) + ".exe"
        absoluteExecutablePath = os.path.join( absoluteResultsDirectoryPath, executableName )

        # Regex matching
        matchingRegexAbsolutePath    = os.path.join( absoluteTestDirectoryPath, "MatchingRegex"    )
        nonMatchingRegexAbsolutePath = os.path.join( absoluteTestDirectoryPath, "NonMatchingRegex" )

        # Setup the test result
        result = TestResult()

        result.compiler = compiler
        result.timer = timer
        result.name = relativeTestPath

        try:
            # Find out what the expected test result should be.
            #
            # It detects this by searching for the existence of a file with one of the following names:
            #     CompileFail
            #         - the test passes if compiling the test file fails
            #
            #     CompilePass
            #         - the test passes if compiling the test file succeeds. The resulting executable is not run.
            #
            #     TestPass
            #         - the test passes if compiling succeeds and running the resulting executable has an exit code of zero
            #
            #     TestFail
            #         - the test passes if compiling succeeds and running the resulting executable has a non-zero exit code
            #
            # If a test folder doesn't have a file with one of those four names or has multiple files with one of those names an
            # error message is printed and the testsuite exits.
            expectedResult = ""

            for resultFile in [ "CompileFail", "CompilePass", "TestFail", "TestPass" ]:
                if os.path.exists( os.path.join(absoluteTestDirectoryPath, resultFile) ):
                    if expectedResult == "":
                        expectedResult = resultFile
                    else:
                        result.SetSkipped( "Multiple result files found (" + expectedResult + ", " + resultFile + ")" )
                        return result

            if expectedResult == "":
                result.SetSkipped( "No result file found" )
                return result

            result.expectedResult = expectedResult

            # Compile the test in the Results directory
            commandLine = GetUnitTestCommandLine( compiler, absoluteTestPath, executableName )
            compileOutput, compileReturnCode, compileTime = RunProcess( commandLine, absoluteResultsDirectoryPath )

            with open( compileOutputAbsolutePath, "w") as logFile:
                logFile.write( " ".join(commandLine) + "\n" )
                logFile.write( "-------------------\n" )
                logFile.write( compileOutput )

            # If the compile failed then we have to stop.
            if compileReturnCode != 0:
                if compileTime == -1.0:
                    result.SetResult( "CompileTimeout" )
                else:
                    result.SetResult( "CompileFail" )

                return result
            # And if it passed and we only cared about compiling then we're done.
            elif expectedResult == "CompilePass" or expectedResult == "CompileFail":
                result.SetResult( "CompilePass" )
                return result

            # Otherwise run the resulting executable.
            programOutput, programReturnCode, programTime = RunProcess( [absoluteExecutablePath], absoluteResultsDirectoryPath )

            with open( programOutputAbsolutePath, "w") as logFile:
                logFile.write( absoluteExecutablePath + "\n" )
                logFile.write( "-------------------\n" )
                logFile.write( programOutput )

            # If the executable took too long to run we kill it.
            if programTime == -1.0:
                result.SetResult( "ProgramTimeout" )
                return result

            # If the executable return code wasn't as expected then no need to check program output.
            if programReturnCode == 0 and expectedResult != "TestPass":
                result.SetResult( "TestPass" )
                return result

            if programReturnCode != 0 and expectedResult != "TestFail":
                result.SetResult( "TestFail" )
                return result

            # Check if at least one line in the output matches a given regex (if applicable)
            matchingRegexResult = True
            if os.path.exists( matchingRegexAbsolutePath ):
                with open( matchingRegexAbsolutePath, "r" ) as file:
                    data = file.read().strip()
                    regex = re.compile( data, re.DOTALL )

                    if not regex.search( programOutput ):
                        matchingRegexResult = False

            # Check that no lines in the output match a given regex (if applicable)
            nonMatchingRegexResult = True
            if os.path.exists( nonMatchingRegexAbsolutePath ):
                with open( nonMatchingRegexAbsolutePath, "r" ) as file:
                    data = file.read().strip()
                    regex = re.compile( data, re.DOTALL )

                    if regex.search( programOutput ):
                        nonMatchingRegexResult = False

            # Return result if regex checks failed.
            if matchingRegexResult == False:
                result.SetResult( "RegexMatchFail" )
                return result

            if nonMatchingRegexResult == False:
                result.SetResult( "RegexNonMatchFail" )
                return result

            # Return result based on executable return code.
            if programReturnCode == 0:
                result.SetResult( "TestPass" )
                return result
            else:
                result.SetResult( "TestFail" )
                return result
        except Exception as e:
            result.SetSkipped( str(e) )
            return result

#
# The main function.
#
def main():
    # Create the folders where all the output files will be placed.
    try:
        os.makedirs( BaseResultsDir )
    except:
        pass

    if os.path.exists( ResultsDir ):
        sys.exit( "ERROR: {} already exists! Maybe increase timestamp precision?".format(ResultsDir) )

    os.makedirs( ResultsDir )

    # Open the global log file handle for writing.
    # We haven't done anything worth logging before this point, so it doesn't matter
    # if we missed some output.
    global LogFile
    LogFile = InitializeGlobalLogger( LogFilePath )

    # Make sure all the expected paths can be found
    for path in [BaseTestDir, ResultsDir, LogFilePath, UnitTestMain, SourceDir]:
        if os.path.exists( path ) == False:
            sys.exit( "ERROR: {} does not exist!".format(path) )

    # Parse command line arguments to determine the testsuite to run.
    # If no command line arguments are given then we run all the tests.
    testSuite = BaseTestDir

    if len( sys.argv ) > 2:
        LogFile.info( "Too many arguments, can only accept one testsuite directory." )
        return
    elif len( sys.argv ) == 2:
        testSuite = os.path.join( BaseTestDir, sys.argv[1] )

    # Make sure the testsuite directory actually exists.
    if not os.path.exists( testSuite ):
        LogFile.info( "Testsuite \"{}\" does not exist!".format(testSuite) )
        return

    # Discover what compilers are installed.
    compilersFound = GetInstalledCompilers()

    if compilersFound:
        LogFile.info( "Compilers found: {!s}".format(compilersFound) )
        LogFile.info( "" )
    else:
        LogFile.info( "No compilers passed the sanity test!" )
        return

    # Get the current git revision
    gitRevisionHEAD = GetGitRevisionNumberHEAD()

    if gitRevisionHEAD:
        LogFile.info( "Using commit {}".format(gitRevisionHEAD) )
    else:
        LogFile.info( "Unable to get HEAD revision number" )

    LogFile.info( "" )

    # Run the testsuite with all the installed compilers.
    testsuiteResults = RunTestsuite( testSuite, compilersFound )
    LogFile.info( testsuiteResults )

#
# Call the main() function, timing how long it takes.
#
if __name__ == "__main__":
    with WallTimer() as timer:
        main()

    LogFile.info( "Took {:.2f} seconds.".format(timer.elapsed) )
