Preprocessor
    Preprocessor-Test1  -> Confirm that the appropriate compiler macros are defined.
    Preprocessor-Test2  -> Confirm that Debug mode is set properly.
    Preprocessor-Test3  -> Confirm that Release mode is set properly.
    Preprocessor-Test4  -> Confirm that Debug mode and Release mode are mutually exclusive.
    Preprocessor-Test5  -> Confirm that if nothing is specified then we default to Debug mode.
    Preprocessor-Test6  -> Confirm that the NO_COPY macro works correctly for classes.
    Preprocessor-Test7  -> Confirm that the NO_COPY macro works correctly for template classes.
    Preprocessor-Test8  -> Confirm that the NO_ASSIGN macro works correctly for classes.
    Preprocessor-Test9  -> Confirm that the NO_ASSIGN macro works correctly for template classes.
    Preprocessor-Test10 -> Confirm that the STRINGIFY macro works correctly.

IO
    Print
        IO-Print-Test1  -> Basic sanity test for ioPrint.
        IO-Print-Test2  -> Confirm that arguments to ioPrint are forwarded perfectly.
        IO-Print-Test3  -> Confirm that ioPrint can accept non-copyable arguments.
        IO-Print-Test4  -> Confirm that ioPrint can accept *lots* of arguments.

        IO-Print-Test5  -> Basic sanity test for ioPrintln.
        IO-Print-Test6  -> Confirm that arguments to ioPrintln are forwarded perfectly.
        IO-Print-Test7  -> Confirm that ioPrintln can accept non-copyable arguments.
        IO-Print-Test8  -> Confirm that ioPrintln can accept *lots* of arguments.

        IO-Print-Test9  -> Basic sanity test for ioCout.
        IO-Print-Test10 -> Basic sanity test for ioCerr.

        IO-Print-Test11 -> Confirm that ioVerbose prints when enabled.
        IO-Print-Test12 -> Confirm that ioVerbose does not print when disabled and doesn't evaluate its arguments.

    Printf

    PrettyPrint
        IO-PrettyPrint-Test1  -> Confirm that an std::array can be pretty-printed.
        IO-PrettyPrint-Test2  -> Confirm that an std::deque can be pretty-printed.
        IO-PrettyPrint-Test3  -> Confirm that an std::forward_list can be pretty-printed.
        IO-PrettyPrint-Test4  -> Confirm that an std::initializer_list can be pretty-printed.
        IO-PrettyPrint-Test5  -> Confirm that an std::list can be pretty-printed.
        IO-PrettyPrint-Test6  -> Confirm that an std::map can be pretty-printed.
        IO-PrettyPrint-Test7  -> Confirm that an std::pair can be pretty-printed.
        IO-PrettyPrint-Test8  -> Confirm that an std::set can be pretty-printed.
        IO-PrettyPrint-Test9  -> Confirm that an std::tuple can be pretty-printed.
        IO-PrettyPrint-Test10 -> Confirm that an std::unordered_map can be pretty-printed.
        IO-PrettyPrint-Test11 -> Confirm that an std::unordered_set can be pretty-printed.
        IO-PrettyPrint-Test12 -> Confirm that an std::vector can be pretty-printed.

Debug
    Debug-Break-Test1 -> Confirm that dbBreak() correctly inserts a breakpoint.
    Debug-Break-Test2 -> Confirm that dbBreak() can be disabled.
    Debug-Break-Test3 -> Confirm that a disabled dbBreak() does not evaluate its arguments.
    Debug-Break-Test4 -> Confirm that an enabled dbBreak() correctly forwards its arguments.

    Debug-Assert-Test1 -> Confirm that a triggered dbAssert() will crash the program.
    Debug-Assert-Test2 -> Confirm that an untriggered dbAssert() will not crash the program.
    Debug-Assert-Test3 -> Confirm that dbAssert() can be disabled.
    Debug-Assert-Test4 -> Confirm that a disabled dbAssert() doesn't evaluate its arguments.
    Debug-Assert-Test5 -> Confirm that an enabled dbAssert() correctly forwards its arguments.

    Debug-Warn-Test1 -> Confirm that a triggered dbWarn() will print out a message and not crash the program.
    Debug-Warn-Test2 -> Confirm that an untriggered dbWarn will not print out a message or crash the program.
    Debug-Warn-Test3 -> Confirm that dbWarn() can be disabled.
    Debug-Warn-Test4 -> Confirm that a disabled dbWarn() doesn't evaluate its arguments.
    Debug-Warn-Test5 -> Confirm that an enabled dbWarn() correctly forwards its arguments.

    Debug-ErrorCheck-Test1 -> Confirm that a failed dbErrorCheck() will crash the program.
    Debug-ErrorCheck-Test2 -> Confirm that a passed dbErrorCheck() will not crash the program.
    Debug-ErrorCheck-Test3 -> Confirm that dbErrorCheck still runs the condition when disabled.
    Debug-ErrorCheck-Test4 -> Confirm that an enabled dbErrorCheck() correctly forwards its arguments.

    Debug-Crash-Test1 -> Confirm that dbCrash() will crash the program.
    Debug-Crash-Test2 -> Confirm that dbCrash() still crashes the program when breakpoints are disabled.
    Debug-Crash-Test3 -> Confirm that dbCrash() correctly forwards its arguments.

    Debug-Location-Test1 -> Confirm that dbLocation() gets the correct data.

    Debug-LocationMap-Test1 -> Confirm that Debug::LocationMap works properly.
    Debug-LocationMap-Test2 -> Confirm that Debug::LocationMap can be disabled.
    Debug-LocationMap-Test3 -> Confirm that double add to a Debug::LocationMap is caught.
    Debug-LocationMap-Test4 -> Confirm that double delete from a Debug::LocationMap is caught.
    Debug-LocationMap-Test5 -> Confirm that double add to a Debug::LocationMap is ignored if disabled.
    Debug-LocationMap-Test6 -> Confirm that double delete from a Debug::LocationMap is ignored if disabled.

    Debug-ExistenceSet-Test1 -> Confirm that Debug::ExistenceSet works properly.
    Debug-ExistenceSet-Test2 -> Confirm that Debug::ExistenceSet can be disabled.
    Debug-ExistenceSet-Test3 -> Confirm that double add to a Debug::ExistenceSet is caught.
    Debug-ExistenceSet-Test4 -> Confirm that double delete from a Debug::ExistenceSet is caught.
    Debug-ExistenceSet-Test5 -> Confirm that double add to a Debug::ExistenceSet is ignored if disabled.
    Debug-ExistenceSet-Test6 -> Confirm that double delete from a Debug::ExistenceSet is ignored if disabled.

    Debug-Cast-Test1 -> Confirm that an enabled poly_cast will catch an invalid cast.
    Debug-Cast-Test2 -> Confirm that an enabled poly_cast will be fine with a valid cast.
    Debug-Cast-Test3 -> Confirm that a disabled poly_cast will ignored an invalid cast.

TypeNames
    Typename-Test1 -> Confirm that Typename<T>() returns the correct string for builtin types.
    Typename-Test2 -> Confirm that Typename<T>() returns the default string when given a type without an associated string.
    Typename-Test3 -> Confirm that Typename<T>() returns the correct string for user-created classes and structs.
    Typename-Test4 -> Confirm that Typename<T>() returns the correct string for enum and enum classes.
    Typename-Test5 -> Confirm that Typename<T>() returns the correct string for classes with a single template type parameter.
    Typename-Test6 -> Confirm that Typename<T>() returns the correct string for classes with a single unsigned int template parameter.
    Typename-Test7 -> Confirm that Typename<T>() returns the correct string when working with STL containers.
    Typename-Test8 -> Confirm that Typename<T>() returns the correct string when given a derivation (e.g. const, pointer, array, etc..) of a known type.
    Typename-Test9 -> Confirm that Typename<T>() returns the correct string for classes with a template type parameter and an unsigned int template parameter.
    Typename-Test10 -> Confirm that Typename<T>() returns the correct string for classes with a template type parameter and a template template type parameter.

Singleton
    Singleton-Test1 -> Confirm that inheriting from the Singleton<T> template class creates a global Singleton of type T.
    Singleton-Test2 -> Confirm that the Singleton is destructed at the end of the program.
    Singleton-Test3 -> Confirm that the Singleton is never created if it isn't used.
    Singleton-Test4 -> Confirm that the Singleton can only be created with Get(), not on the stack.
    Singleton-Test5 -> Confirm that there can only be one Singleton instance.
    Singleton-Test6 -> Confirm that the Singleton can only be created with Get(), not on the heap.
    Singleton-Test7 -> Confirm that use after destruction is caught.
    Singleton-Test8 -> Confirm that you cannot copy-construct a Singleton.
    Singleton-Test9 -> Confirm that you cannot copy-assign a Singleton.
    Singleton-Test10 -> Confirm that you must inherit from the Singleton<T> base class
    Singleton-Test11 -> Confirm that the Singleton is created lazily the first time you access it.
    Singleton-Test12 -> Confirm that Singletons are destroyed in a Last-In-First-Out order.
    Singleton-Test13 -> Confirm that two Singletons recursively accessing each other in their constructors is caught.
    Singleton-Test14 -> Confirm that you can create a Singleton<Singleton<T>>.
    Singleton-Test15 -> Confirm that you can't create just a Singleton<T> object.
    Singleton-Test16 -> Confirm can access Singleton in constructor and destructor.

        Add a static_assert to make sure that the Singleton instance is default-constructable???

Allocator
    IAllocator
        IAllocator-Test1 -> Confirm that a class that uses the IAllocator cannot be created using the throwing operator new.
        IAllocator-Test2 -> Confirm that a class that uses the IAllocator cannot be created using the non-throwing operator new.
        IAllocator-Test3 -> Confirm that a class that uses the IAllocator cannot be created using the placement operator new.
        IAllocator-Test4 -> Confirm that a class that uses the IAllocator cannot be created using the throwing operator new[].
        IAllocator-Test5 -> Confirm that a class that uses the IAllocator cannot be created using the non-throwing operator new[].
        IAllocator-Test6 -> Confirm that a class that uses the IAllocator cannot be created using the placement operator new[].

        IAllocator-Test7 -> Confirm that a class that uses the IAllocator can be created using the IAllocator placement operator new.
        IAllocator-Test8 -> Confirm that the IAllocator placement operator new syntax is still valid if the IAllocator is disabled.

        IAllocator-Test9 -> Confirm a simple new/delete cycle works on a class that uses the IAllocator.
        IAllocator-Test10 -> Stress test random allocations and deallocations.

        IAllocator-Test11 -> Confirm that memory leaks are caught and have the the correct file locations.
        IAllocator-Test12 -> Confirm that memory leaks are caught and don't display file locations when disabled.

        IAllocator-Test13 -> Confirm that object memory is cleared when an object is deleted.
        IAllocator-Test14 -> Confirm that memory clearing can be disabled.

        IAllocator-Test15 -> Confirm that there are no memory leaks if a constructor throws an exception.

        IAllocator-Test16 -> Confirm that the IAllocator works correctly with virtual delete.

        IAllocator-Test17 -> Confirm that the IAllocator can be used with arbitrary types by using a wrapper struct.

        IAllocator-Test18 -> Confirm that inherited operator new fails if child class size is different from the base class.
        IAllocator-Test19 -> Confirm that inherited operator new works properly if child class is correct size.
        IAllocator-Test20 -> Confirm that a derived class can override the base class allocator.

        IAllocator-Test21 -> Confirm that using the IAllocator macro with the wrong type fails to compile.

        IAllocator-Test22 -> Confirm that deletion of a nullptr is fine.

        IAllocator-Test23 -> Confirm that double deletion is caught.
        IAllocator-Test24 -> Confirm that deletion of a stack allocated object is caught.

        IAllocator-Test25 -> Confirm that the Create and Destroy wrapper functions work properly.
            Add a test for typos, like PoolAllocator-Common-Test21???

        IAllocator-Test26 -> Confirm works properly with std::unique_ptr.
        IAllocator-Test27 -> Confirm works properly with std::shared_ptr.

        IAllocator-Test28 -> Confirm that the IAllocator can be disabled.
        IAllocator-Test29 -> Confirm that the disabled IAllocator implementation handles out-of-memory errors correctly.

    PoolAllocator
        PoolAllocator-Test1 -> Confirm that objects created right after each other have adjacent addresses.

        PoolAllocator-Test2 -> Confirm that it is possible to create N simultaneous objects.
        PoolAllocator-Test3 -> Confirm that creating N + 1 simultaneous objects fails.

        PoolAllocator-Test4 -> Confirm that the PoolAllocator can handle not being able to create the memory pool.

        Confirm that there is a compile failure when you don't have pool size

    BasicAllocator
            Confirm that there is a compile failure if you have the pool size


Factory
    FactorySetImpl
        FactorySetImpl-Test1 -> Basic create and destroy test.
        FactorySetImpl-Test2 -> Confirm that memory leaks are caught and have the correct file location.
        FactorySetImpl-Test3 -> Confirm that all objects can be destroyed at once.
        FactorySetImpl-Test4 -> Confirm that all objects that match a predicate can be destroyed at once.
        FactorySetImpl-Test5 -> Confirm that deletion of an object not created by the factory is caught.
        FactorySetImpl-Test6 -> Confirm that double deletion is caught.
        FactorySetImpl-Test7 -> Confirm can create any object in the type hierarchy.
        FactorySetImpl-Test8 -> Confirm can create any object with many different constructors.
        FactorySetImpl-Test9 -> Confirm creating an object not in the type hierachy fails.
        FactorySetImpl-Test10 -> Stress test with random creations and deletions.
        FactorySetImpl-Test11 -> Confirm that line tracking can be disabled.

Metaprogramming
    IndexSeqeuence-Test1 -> Confirm that IndexSequence works correctly.

Tests
    ExceptionHandler
        Check for new running out of memory
        Check for throwing an unexpected exception
        Check for uncaught exception
        Check for exception thrown during stack unwinding

    Debug
        Check what happens with nested asserts???
        Check what happens with a warning nested inside an assert???
        Confirm that everything still works with console printing disabled

    Macros
        Confirm STRINGIFY works properly

    Typenames
        Confirm works with STL containers
        Confirm works with std::string

    MemoryAllocationMacros
        Confirm that macros work properly, and can be turned on/off

    Factory
        Confirm variadic creation works
        Confirm Destroy works
            Especially with virtual dtors
        Created() function work
        Confirm ForEach works
        DestroyAllObjectsThat
        DestroyAllObjects
        Confirm that MaxAllocated, CurAllocated, LeakedMemory are correct
            No reason they can't be public, either
        Confirm can use with an arbitrary type
        confirm that memory leaks are caught
        Make sure memory clearing works

    Allocator+Factory
        Confirm that they work correctly together

