/*
 * Confirm that IndexSequence works correctly.
 *
 * Author:      Stuart Rudderham
 * Created:     June 8, 2014
 * Last Edited: June 8, 2014
 */

#include "Metaprogramming/IndexSequence.hpp"    // MakeIndexSequence, IndexSequence
#include <type_traits>                          // std::is_same
int Test()
{
    static_assert( std::is_same< MakeIndexSequence<0>::Sequence, IndexSequence<       > >::value, "MakeIndexSequence unit test failed!" );
    static_assert( std::is_same< MakeIndexSequence<1>::Sequence, IndexSequence<0      > >::value, "MakeIndexSequence unit test failed!" );
    static_assert( std::is_same< MakeIndexSequence<2>::Sequence, IndexSequence<0, 1   > >::value, "MakeIndexSequence unit test failed!" );
    static_assert( std::is_same< MakeIndexSequence<3>::Sequence, IndexSequence<0, 1, 2> >::value, "MakeIndexSequence unit test failed!" );

    return 0;
}
