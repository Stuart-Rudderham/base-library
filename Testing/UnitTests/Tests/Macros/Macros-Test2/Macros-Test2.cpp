/*
 * Confirm that if nothing is specified then we default to Debug mode.
 *
 * Author:      Stuart Rudderham
 * Created:     November 13, 2013
 * Last Edited: November 20, 2013
 */

#if defined( DEBUG ) || defined( RELEASE )
    #error "Debug or Release mode specified!"
#endif

#include "Macros/Macros.hpp"

#if !DEBUG_MODE
    #error "DEBUG_MODE != 1"
#endif

int Test()
{
    return 0;
}



