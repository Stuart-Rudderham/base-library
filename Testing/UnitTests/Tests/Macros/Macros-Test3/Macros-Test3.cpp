/*
 * Confirm that the NO_COPY macro works correctly for classes.
 *
 * Author:      Stuart Rudderham
 * Created:     November 13, 2013
 * Last Edited: November 20, 2013
 */

#include "Macros/Macros.hpp"

class Foo
{
    public:
        NO_COPY( Foo );    
};

int Test()
{
    Foo x( ( Foo() ) );

    return 0;
}



