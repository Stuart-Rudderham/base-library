/*
 * Confirm that the NO_ASSIGN macro works correctly for template classes.
 *
 * Author:      Stuart Rudderham
 * Created:     November 13, 2013
 * Last Edited: November 20, 2013
 */

#include "Macros/Macros.hpp"

template<typename T>
class Foo
{
    public:
        NO_ASSIGN( Foo );    
};

int Test()
{
    Foo<int> x;
    x = Foo<int>();

    return 0;
}



