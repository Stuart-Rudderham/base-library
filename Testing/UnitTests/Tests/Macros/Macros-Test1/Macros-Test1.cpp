/*
 * Confirm that Debug mode and Release mode are mutually exclusive.
 *
 * Author:      Stuart Rudderham
 * Created:     November 13, 2013
 * Last Edited: November 20, 2013
 */

#define DEBUG   1
#define RELEASE 1

#include "Macros/Macros.hpp"

int Test()
{
    return 0;
}



