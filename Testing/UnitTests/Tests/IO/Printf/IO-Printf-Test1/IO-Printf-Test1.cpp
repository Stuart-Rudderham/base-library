/*
 * Basic sanity test for ioPrintf.
 *
 * Author:      Stuart Rudderham
 * Created:     July 28, 2014
 * Last Edited: July 28, 2014
 */

#include "IO/Print.hpp"     // ioCout
#include "IO/Printf.hpp"    // ioPrintf, ioPrintlnf, ioCoutf, ioCerrf, ioVerbosef
#include <iostream>         // std::cout, std::cerr

struct Foo
{
    Foo()
    {
        ioCout( "Foo::Foo" );
    }

    ~Foo()
    {
        ioCout( "Foo::~Foo" );
    }

    Foo( const Foo& ) = delete;

    Foo( Foo&& ) = delete;

    Foo& operator=( const Foo& ) = delete;

    Foo& operator=( Foo&& ) = delete;
};

// Different overloads for various cv-qualifiers and rvalue vs lvalue references.
std::ostream& operator<<( std::ostream& stream, Foo& )
{
    stream << "Foo&";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, const Foo& )
{
    stream << "const Foo&";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, volatile Foo& )
{
    stream << "volatile Foo&";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, const volatile Foo& )
{
    stream << "const volatile Foo&";
    return stream;
}


std::ostream& operator<<( std::ostream& stream, Foo&& )
{
    stream << "Foo&&";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, const Foo&& )
{
    stream << "const Foo&&";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, volatile Foo&& )
{
    stream << "volatile Foo&&";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, const volatile Foo&& )
{
    stream << "const volatile Foo&&";
    return stream;
}


int Test()
{
    Foo f;

    ioPrintf  ( std::cout, "1 ~ ~\n", f, Foo() );
    ioPrintlnf( std::cerr, "2 ~ ~"  , f, Foo() );
    ioCoutf   (            "3 ~ ~"  , f, Foo() );
    ioCerrf   (            "4 ~ ~"  , f, Foo() );
    ioVerbosef(            "5 ~ ~"  , f, Foo() );

    return 0;
}
