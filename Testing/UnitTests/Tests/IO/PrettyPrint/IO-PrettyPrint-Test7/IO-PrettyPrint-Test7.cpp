/*
 * Confirm that an std::pair can be pretty-printed.
 *
 * Author:      Stuart Rudderham
 * Created:     May 14, 2014
 * Last Edited: May 14, 2014
 */

#include "IO/Print.hpp"                 // ioCout
#include "IO/PrettyPrint/Pair.hpp"      // operator<<(std::ostream&, const std::pair<T, U>&)
#include <utility>                      // std::pair, std::make_pair

int Test()
{
    auto p1 = std::make_pair(1, 2);
    ioCout( p1 );

    ioCout( std::make_pair(3.5, 4.5) );

    ioCout( std::make_pair( std::make_pair('a', "b"), std::make_pair("c", 'd') ) );

    return 0;
}
