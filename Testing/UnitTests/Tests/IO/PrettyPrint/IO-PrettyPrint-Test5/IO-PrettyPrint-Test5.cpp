/*
 * Confirm that an std::list can be pretty-printed.
 *
 * Author:      Stuart Rudderham
 * Created:     May 14, 2014
 * Last Edited: May 14, 2014
 */

#include "IO/Print.hpp"                 // ioCout
#include "IO/PrettyPrint/List.hpp"      // operator<<(std::ostream&, const std::list<T, A>&)
#include <list>                         // std::list
#include <string>                       // std::string

int Test()
{
    // Empty.
    {
        std::list<int> a1;
        ioCout( a1 );

        std::list<std::string> a2;
        ioCout( a2 );

        ioCout( std::list<double>() );
    }

    // With 1 element.
    {
        std::list<int> a1{ 1 };
        ioCout( a1 );

        std::list<std::string> a2{ "1" };
        ioCout( a2 );

        ioCout( std::list<double>{ 1.5 } );
    }

    // With 2 elements.
    {
        std::list<int> a1{ 1, 2 };
        ioCout( a1 );

        std::list<std::string> a2{ "1", "2" };
        ioCout( a2 );

        ioCout( std::list<double>{ 1.5, 2.5 } );
    }

    // With 3 elements.
    {
        std::list<int> a1{ 1, 2, 3 };
        ioCout( a1 );

        std::list<std::string> a2{ "1", "2", "3" };
        ioCout( a2 );

        ioCout( std::list<double>{ 1.5, 2.5, 3.5 } );
    }

    // With 5 elements.
    {
        std::list<int> a1{ 1, 2, 3, 4, 5 };
        ioCout( a1 );

        std::list<std::string> a2{ "1", "2", "3", "4", "5" };
        ioCout( a2 );

        ioCout( std::list<double>{ 1.5, 2.5, 3.5, 4.5, 5.5 } );
    }

    // Nested.
    {
        std::list<std::list<int>> a1;
        ioCout( a1 );

        std::list<std::list<int>> a2{ std::list<int>{1, 2},
                                      std::list<int>{3, 4},
                                      std::list<int>{5, 6} };
        ioCout( a2 );
    }

    return 0;
}
