/*
 * Confirm that an std::array can be pretty-printed.
 *
 * Author:      Stuart Rudderham
 * Created:     May 14, 2014
 * Last Edited: May 14, 2014
 */

#include "IO/Print.hpp"                 // ioCout
#include "IO/PrettyPrint/Array.hpp"     // operator<<(std::ostream&, const std::array<T, N>&)
#include <array>                        // std::array
#include <string>                       // std::string

int Test()
{
    // Empty array.
    {
        std::array<int, 0> a1;
        ioCout( a1 );

        std::array<std::string, 0> a2;
        ioCout( a2 );

        ioCout( std::array<double, 0>() );
    }

    // Array with 1 element.
    {
        std::array<int, 1> a1{{ 1 }};
        ioCout( a1 );

        std::array<std::string, 1> a2{{ "1" }};
        ioCout( a2 );

        ioCout( std::array<double, 1>{{ 1.5 }} );
    }

    // Array with 2 elements.
    {
        std::array<int, 2> a1{{ 1, 2 }};
        ioCout( a1 );

        std::array<std::string, 2> a2{{ "1", "2" }};
        ioCout( a2 );

        ioCout( std::array<double, 2>{{ 1.5, 2.5 }} );
    }

    // Array with 3 elements.
    {
        std::array<int, 3> a1{{ 1, 2, 3 }};
        ioCout( a1 );

        std::array<std::string, 3> a2{{ "1", "2", "3" }};
        ioCout( a2 );

        ioCout( std::array<double, 3>{{ 1.5, 2.5, 3.5 }} );
    }

    // Array with 5 elements.
    {
        std::array<int, 5> a1{{ 1, 2, 3, 4, 5 }};
        ioCout( a1 );

        std::array<std::string, 5> a2{{ "1", "2", "3", "4", "5" }};
        ioCout( a2 );

        ioCout( std::array<double, 5>{{ 1.5, 2.5, 3.5, 4.5, 5.5 }} );
    }

    // Nested arrays.
    {
        std::array<std::array<int, 0>, 0> a1;
        ioCout( a1 );

        std::array<std::array<int, 0>, 1> a2;
        ioCout( a2 );

        std::array<std::array<int, 1>, 0> a3;
        ioCout( a3 );

        std::array<std::array<int, 2>, 3> a4{{ std::array<int, 2>{{1, 2}},
                                               std::array<int, 2>{{3, 4}},
                                               std::array<int, 2>{{5, 6}} }};
        ioCout( a4 );
    }

    return 0;
}
