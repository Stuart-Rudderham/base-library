/*
 * Confirm that an std::tuple can be pretty-printed.
 *
 * Author:      Stuart Rudderham
 * Created:     May 14, 2014
 * Last Edited: May 14, 2014
 */

#include "IO/Print.hpp"                 // ioCout
#include "IO/PrettyPrint/Tuple.hpp"     // operator<<(std::ostream&, const std::tuple<T...>&)
#include <tuple>                        // std::tuple, std::make_tuple
#include <string>                       // std::string

int Test()
{
    // Empty.
    auto emptyTuple = std::make_tuple();
    ioCout( emptyTuple );

    // Double empty.
    // Nice corner case -> http://stackoverflow.com/q/5481658
    ioCout( std::make_tuple(emptyTuple) );

    // One element.
    ioCout( std::make_tuple(1) );
    ioCout( std::make_tuple(std::make_tuple(1)) );

    // Two elements.
    ioCout( std::make_tuple(1, 2) );
    ioCout( std::make_tuple(1, std::make_tuple(2, 3)) );

    // Three elements.
    ioCout( std::make_tuple(1, 2, 3) );

    // Five elements.
    ioCout( std::make_tuple(1, 2.1, '3', "4", "Five") );

    // Nested.
    ioCout( std::make_tuple(std::make_tuple(1), std::make_tuple(2.1, '3'), std::make_tuple("4", "Five")) );

    return 0;
}
