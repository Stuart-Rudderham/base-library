/*
 * Confirm that an std::initializer_list can be pretty-printed.
 *
 * Author:      Stuart Rudderham
 * Created:     May 14, 2014
 * Last Edited: May 14, 2014
 */

#include "IO/Print.hpp"                         // ioCout
#include "IO/PrettyPrint/InitializerList.hpp"   // operator<<(std::ostream&, const std::initializer_list<T>&)
#include <initializer_list>                     // std::initializer_list
#include <string>                               // std::string

int Test()
{
    // Empty.
    {
        std::initializer_list<int> a1;
        ioCout( a1 );

        std::initializer_list<std::string> a2;
        ioCout( a2 );

        ioCout( std::initializer_list<double>() );
    }

    // With 1 element.
    {
        std::initializer_list<int> a1{ 1 };
        ioCout( a1 );

        std::initializer_list<std::string> a2{ "1" };
        ioCout( a2 );

        ioCout( std::initializer_list<double>{ 1.5 } );
    }

    // With 2 elements.
    {
        std::initializer_list<int> a1{ 1, 2 };
        ioCout( a1 );

        std::initializer_list<std::string> a2{ "1", "2" };
        ioCout( a2 );

        ioCout( std::initializer_list<double>{ 1.5, 2.5 } );
    }

    // With 3 elements.
    {
        std::initializer_list<int> a1{ 1, 2, 3 };
        ioCout( a1 );

        std::initializer_list<std::string> a2{ "1", "2", "3" };
        ioCout( a2 );

        ioCout( std::initializer_list<double>{ 1.5, 2.5, 3.5 } );
    }

    // With 5 elements.
    {
        std::initializer_list<int> a1{ 1, 2, 3, 4, 5 };
        ioCout( a1 );

        std::initializer_list<std::string> a2{ "1", "2", "3", "4", "5" };
        ioCout( a2 );

        ioCout( std::initializer_list<double>{ 1.5, 2.5, 3.5, 4.5, 5.5 } );
    }

    // Nested.
    {
        std::initializer_list<std::initializer_list<int>> a1;
        ioCout( a1 );

        std::initializer_list<std::initializer_list<int>> a2{ std::initializer_list<int>{1, 2},
                                         std::initializer_list<int>{3, 4},
                                         std::initializer_list<int>{5, 6} };
        ioCout( a2 );
    }

    return 0;
}
