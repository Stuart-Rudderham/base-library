/*
 * Confirm that an std::map and std::multimap can be pretty-printed.
 *
 * Author:      Stuart Rudderham
 * Created:     May 14, 2014
 * Last Edited: May 14, 2014
 */

#include "IO/Print.hpp"                 // ioCout
#include "IO/PrettyPrint/Map.hpp"       // operator<<(std::ostream&, const std::map<K, V, C, A>&)
                                        // operator<<(std::ostream&, const std::multimap<K, V, C, A>&)
#include <map>                          // std::map, std::multimap
#include <string>                       // std::string
#include <functional>                   // std::greater

int Test()
{
    // Empty.
    {
        std::map<int, std::string> a1;
        ioCout( a1 );

        std::multimap<int, std::string> a2;
        ioCout( a2 );

        ioCout( std::map<int, std::string, std::less<int>>() );

        ioCout( std::multimap<int, std::string, std::greater<int>>() );
    }

    // With 1 element.
    {
        std::map<int, std::string> a1{ {1, "1"} };
        ioCout( a1 );

        std::multimap<int, std::string> a2{ {1, "1"} };
        ioCout( a2 );

        std::map<int, std::string, std::less<int>> a3{ {1, "1"}, {1, "2"} };
        ioCout( a3 );

        std::multimap<int, std::string, std::greater<int>> a4{ {1, "1"} };
        ioCout( a4 );
    }

    // With 2 elements.
    {
        std::map<int, std::string> a1{ {1, "1"},
                                       {2, "2"} };
        ioCout( a1 );

        std::multimap<int, std::string> a2{ {1, "1"},
                                            {1, "1"} };
        ioCout( a2 );

        std::map<int, std::string, std::less<int>> a3{ {1, "1"},
                                                       {2, "2"} };
        ioCout( a3 );

        std::multimap<int, std::string, std::greater<int>> a4{ {1, "1"},
                                                               {1, "1"} };
        ioCout( a4 );
    }

    // With 3 elements.
    {
        std::map<int, std::string> a1{ {1, "1"},
                                       {2, "2"},
                                       {3, "3"} };
        ioCout( a1 );

        std::multimap<int, std::string> a2{ {1, "1"},
                                            {1, "1"},
                                            {2, "2"} };
        ioCout( a2 );

        std::map<int, std::string, std::less<int>> a3{ {1, "1"},
                                                       {2, "2"},
                                                       {3, "3"} };
        ioCout( a3 );

        std::multimap<int, std::string, std::greater<int>> a4{ {1, "1"},
                                                               {1, "1"},
                                                               {2, "2"} };
        ioCout( a4 );
    }

    // With 5 elements.
    {
        std::map<int, std::string> a1{ {1, "1"},
                                       {2, "2"},
                                       {3, "3"},
                                       {4, "4"},
                                       {5, "5"} };
        ioCout( a1 );

        std::multimap<int, std::string> a2{ {1, "1"},
                                            {1, "1"},
                                            {2, "2"},
                                            {2, "2"},
                                            {3, "3"} };
        ioCout( a2 );

        std::map<int, std::string, std::less<int>> a3{ {1, "1"},
                                                       {2, "2"},
                                                       {3, "3"},
                                                       {4, "4"},
                                                       {5, "5"} };
        ioCout( a3 );

        std::multimap<int, std::string, std::greater<int>> a4{ {1, "1"},
                                                               {1, "1"},
                                                               {2, "2"},
                                                               {2, "2"},
                                                               {3, "3"} };
        ioCout( a4 );
    }

    // Nested.
    {
        std::map<char, std::string> innerM1{ {'a', "a"}, {'b', "b"} };
        std::map<char, std::string> innerM2{ {'c', "c"}, {'d', "d"} };
        std::map<int, std::map<char, std::string>> a1{ {1, innerM1}, {2, innerM2} };
        ioCout( a1 );

        std::multimap<char, std::string> innerMM1{ {'a', "a"}, {'a', "b"} };
        std::multimap<char, std::string> innerMM2{ {'c', "c"}, {'c', "d"} };
        std::multimap<int, std::multimap<char, std::string>> a2{ {1, innerMM1}, {1, innerMM2} };
        ioCout( a2 );
    }

    return 0;
}
