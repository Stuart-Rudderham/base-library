/*
 * Confirm that an std::set and std::multiset can be pretty-printed.
 *
 * Author:      Stuart Rudderham
 * Created:     May 14, 2014
 * Last Edited: May 14, 2014
 */

#include "IO/Print.hpp"                 // ioCout
#include "IO/PrettyPrint/Set.hpp"       // operator<<(std::ostream&, const std::set<K, C, A>&)
                                        // operator<<(std::ostream&, const std::multiset<K, C, A>&)
#include <set>                          // std::set, std::multiset
#include <string>                       // std::string
#include <functional>                   // std::greater

int Test()
{
    // Empty.
    {
        std::set<int> a1;
        ioCout( a1 );

        std::multiset<int> a2;
        ioCout( a2 );

        ioCout( std::set<int, std::less<int>>() );

        ioCout( std::multiset<int, std::greater<int>>() );
    }

    // With 1 element.
    {
        std::set<int> a1{ 1 };
        ioCout( a1 );

        std::multiset<int> a2{ 1 };
        ioCout( a2 );

        std::set<int, std::less<int>> a3{ 1, 1 };
        ioCout( a3 );

        std::multiset<int, std::greater<int>> a4{ 1 };
        ioCout( a4 );
    }

    // With 2 elements.
    {
        std::set<int> a1{ 1, 2 };
        ioCout( a1 );

        std::multiset<int> a2{ 1, 1 };
        ioCout( a2 );

        std::set<int, std::less<int>> a3{ 1, 2 };
        ioCout( a3 );

        std::multiset<int, std::greater<int>> a4{ 1, 1 };
        ioCout( a4 );
    }

    // With 3 elements.
    {
        std::set<int> a1{ 1, 2, 3 };
        ioCout( a1 );

        std::multiset<int> a2{ 1, 1, 2 };
        ioCout( a2 );

        std::set<int, std::less<int>> a3{ 1, 2, 3 };
        ioCout( a3 );

        std::multiset<int, std::greater<int>> a4{ 1, 1, 2 };
        ioCout( a4 );
    }

    // With 5 elements.
    {
        std::set<int> a1{ 1, 2, 3, 4, 5 };
        ioCout( a1 );

        std::multiset<int> a2{ 1, 1, 2, 2, 3 };
        ioCout( a2 );

        std::set<int, std::less<int>> a3{ 1, 2, 3, 4, 5 };
        ioCout( a3 );

        std::multiset<int, std::greater<int>> a4{ 1, 1, 2, 2, 3 };
        ioCout( a4 );
    }

    // Nested.
    {
        std::set<char> innerS1{ 'a', 'b' };
        std::set<char> innerS2{ 'c', 'd' };
        std::set<std::set<char>> a1{ innerS1, innerS2 };
        ioCout( a1 );

        std::multiset<char> innerMS1{ 'a', 'a', 'b' };
        std::multiset<std::multiset<char>> a2{ innerMS1, innerMS1 };
        ioCout( a2 );
    }

    return 0;
}
