/*
 * Confirm that an std::unordered_map and std::unordered_multimap can be pretty-printed.
 *
 * Author:      Stuart Rudderham
 * Created:     May 14, 2014
 * Last Edited: May 14, 2014
 */

#include "IO/Print.hpp"                     // ioCout
#include "IO/PrettyPrint/UnorderedMap.hpp"  // operator<<(std::ostream&, const std::unordered_map<K, V, H, P, A>&)
                                            // operator<<(std::ostream&, const std::unordered_multimap<K, V, H, P, A>&)
#include <unordered_map>                    // std::unordered_map, std::unordered_multimap
#include <string>                           // std::string

// Custom hashing function.
struct CustomHash
{
    std::size_t operator()( const int& key ) const
    {
        return key;
    }
};

int Test()
{
    // Empty.
    {
        std::unordered_map<int, std::string> a1;
        ioCout( a1 );

        std::unordered_multimap<int, std::string> a2;
        ioCout( a2 );

        ioCout( std::unordered_map<int, std::string, CustomHash>() );

        ioCout( std::unordered_multimap<int, std::string, CustomHash>() );
    }

    // With 1 element.
    {
        std::unordered_map<int, std::string> a1{ {1, "1"} };
        ioCout( a1 );

        std::unordered_multimap<int, std::string> a2{ {1, "1"} };
        ioCout( a2 );

        std::unordered_map<int, std::string, CustomHash> a3{ {1, "1"}, {1, "2"} };
        ioCout( a3 );

        std::unordered_multimap<int, std::string, CustomHash> a4{ {1, "1"} };
        ioCout( a4 );
    }

    // With 2 elements.
    {
        std::unordered_map<int, std::string> a1{ {1, "1"},
                                                 {2, "2"} };
        ioCout( a1 );

        std::unordered_multimap<int, std::string> a2{ {1, "1"},
                                                      {1, "1"} };
        ioCout( a2 );

        std::unordered_map<int, std::string, CustomHash> a3{ {1, "1"},
                                                             {2, "2"} };
        ioCout( a3 );

        std::unordered_multimap<int, std::string, CustomHash> a4{ {1, "1"},
                                                                  {1, "1"} };
        ioCout( a4 );
    }

    // With 3 elements.
    {
        std::unordered_map<int, std::string> a1{ {1, "1"},
                                                 {2, "2"},
                                                 {3, "3"} };
        ioCout( a1 );

        std::unordered_multimap<int, std::string> a2{ {1, "1"},
                                                      {1, "1"},
                                                      {2, "2"} };
        ioCout( a2 );

        std::unordered_map<int, std::string, CustomHash> a3{ {1, "1"},
                                                             {2, "2"},
                                                             {3, "3"} };
        ioCout( a3 );

        std::unordered_multimap<int, std::string, CustomHash> a4{ {1, "1"},
                                                                  {1, "1"},
                                                                  {2, "2"} };
        ioCout( a4 );
    }

    // With 5 elements.
    {
        std::unordered_map<int, std::string> a1{ {1, "1"},
                                                 {2, "2"},
                                                 {3, "3"},
                                                 {4, "4"},
                                                 {5, "5"} };
        ioCout( a1 );

        std::unordered_multimap<int, std::string> a2{ {1, "1"},
                                                      {1, "1"},
                                                      {2, "2"},
                                                      {2, "2"},
                                                      {3, "3"} };
        ioCout( a2 );

        std::unordered_map<int, std::string, CustomHash> a3{ {1, "1"},
                                                             {2, "2"},
                                                             {3, "3"},
                                                             {4, "4"},
                                                             {5, "5"} };
        ioCout( a3 );

        std::unordered_multimap<int, std::string, CustomHash> a4{ {1, "1"},
                                                                  {1, "1"},
                                                                  {2, "2"},
                                                                  {2, "2"},
                                                                  {3, "3"} };
        ioCout( a4 );
    }

    // Nested.
    {
        std::unordered_map<char, std::string> innerM1{ {'a', "a"}, {'b', "b"} };
        std::unordered_map<char, std::string> innerM2{ {'c', "c"}, {'d', "d"} };
        std::unordered_map<int, std::unordered_map<char, std::string>> a1{ {1, innerM1}, {2, innerM2} };
        ioCout( a1 );

        std::unordered_multimap<char, std::string> innerMM1{ {'a', "a"}, {'a', "b"} };
        std::unordered_multimap<char, std::string> innerMM2{ {'c', "c"}, {'c', "d"} };
        std::unordered_multimap<int, std::unordered_multimap<char, std::string>> a2{ {1, innerMM1}, {1, innerMM2} };
        ioCout( a2 );
    }

    return 0;
}
