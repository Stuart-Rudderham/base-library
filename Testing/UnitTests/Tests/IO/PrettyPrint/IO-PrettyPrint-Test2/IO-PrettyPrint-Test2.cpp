/*
 * Confirm that an std::deque can be pretty-printed.
 *
 * Author:      Stuart Rudderham
 * Created:     May 14, 2014
 * Last Edited: May 14, 2014
 */

#include "IO/Print.hpp"                 // ioCout
#include "IO/PrettyPrint/Deque.hpp"     // operator<<(std::ostream&, const std::deque<T, A>&)
#include <deque>                        // std::deque
#include <string>                       // std::string

int Test()
{
    // Empty.
    {
        std::deque<int> a1;
        ioCout( a1 );

        std::deque<std::string> a2;
        ioCout( a2 );

        ioCout( std::deque<double>() );
    }

    // With 1 element.
    {
        std::deque<int> a1{ 1 };
        ioCout( a1 );

        std::deque<std::string> a2{ "1" };
        ioCout( a2 );

        ioCout( std::deque<double>{ 1.5 } );
    }

    // With 2 elements.
    {
        std::deque<int> a1{ 1, 2 };
        ioCout( a1 );

        std::deque<std::string> a2{ "1", "2" };
        ioCout( a2 );

        ioCout( std::deque<double>{ 1.5, 2.5 } );
    }

    // With 3 elements.
    {
        std::deque<int> a1{ 1, 2, 3 };
        ioCout( a1 );

        std::deque<std::string> a2{ "1", "2", "3" };
        ioCout( a2 );

        ioCout( std::deque<double>{ 1.5, 2.5, 3.5 } );
    }

    // With 5 elements.
    {
        std::deque<int> a1{ 1, 2, 3, 4, 5 };
        ioCout( a1 );

        std::deque<std::string> a2{ "1", "2", "3", "4", "5" };
        ioCout( a2 );

        ioCout( std::deque<double>{ 1.5, 2.5, 3.5, 4.5, 5.5 } );
    }

    // Nested.
    {
        std::deque<std::deque<int>> a1;
        ioCout( a1 );

        std::deque<std::deque<int>> a2{ std::deque<int>{1, 2},
                                         std::deque<int>{3, 4},
                                         std::deque<int>{5, 6} };
        ioCout( a2 );
    }

    return 0;
}
