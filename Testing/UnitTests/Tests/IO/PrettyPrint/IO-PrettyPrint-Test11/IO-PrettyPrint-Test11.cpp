/*
 * Confirm that an std::unordered_set and std::unordered_multiset can be pretty-printed.
 *
 * Author:      Stuart Rudderham
 * Created:     May 14, 2014
 * Last Edited: May 14, 2014
 */

#include "IO/Print.hpp"                     // ioCout
#include "IO/PrettyPrint/UnorderedSet.hpp"  // operator<<(std::ostream&, const std::unordered_set<K, H, P, A>&)
                                            // operator<<(std::ostream&, const std::unordered_multiset<K, H, P, A>&)
#include <unordered_set>                    // std::unordered_set, std::unordered_multiset

// Custom hashing function.
struct CustomHash
{
    std::size_t operator()( const int& key ) const
    {
        return key;
    }
};

int Test()
{
    // Empty.
    {
        std::unordered_set<int> a1;
        ioCout( a1 );

        std::unordered_multiset<int> a2;
        ioCout( a2 );

        ioCout( std::unordered_set<int, CustomHash>() );

        ioCout( std::unordered_multiset<int, CustomHash>() );
    }

    // With 1 element.
    {
        std::unordered_set<int> a1{ 1 };
        ioCout( a1 );

        std::unordered_multiset<int> a2{ 1 };
        ioCout( a2 );

        std::unordered_set<int, CustomHash> a3{ 1 };
        ioCout( a3 );

        std::unordered_multiset<int, CustomHash> a4{ 1 };
        ioCout( a4 );
    }

    // With 2 elements.
    {
        std::unordered_set<int> a1{ 1, 2 };
        ioCout( a1 );

        std::unordered_multiset<int> a2{ 1, 1 };
        ioCout( a2 );

        std::unordered_set<int, CustomHash> a3{ 1, 2 };
        ioCout( a3 );

        std::unordered_multiset<int, CustomHash> a4{ 1, 1 };
        ioCout( a4 );
    }

    // With 3 elements.
    {
        std::unordered_set<int> a1{ 1, 2, 3 };
        ioCout( a1 );

        std::unordered_multiset<int> a2{ 1, 1, 2 };
        ioCout( a2 );

        std::unordered_set<int, CustomHash> a3{ 1, 2, 3 };
        ioCout( a3 );

        std::unordered_multiset<int, CustomHash> a4{ 1, 1, 2 };
        ioCout( a4 );
    }

    // With 5 elements.
    {
        std::unordered_set<int> a1{ 1, 2, 3, 4, 5 };
        ioCout( a1 );

        std::unordered_multiset<int> a2{ 1, 1, 2, 2, 3 };
        ioCout( a2 );

        std::unordered_set<int, CustomHash> a3{ 1, 2, 3, 4, 5 };
        ioCout( a3 );

        std::unordered_multiset<int, CustomHash> a4{ 1, 1, 2, 2, 3 };
        ioCout( a4 );
    }

    return 0;
}
