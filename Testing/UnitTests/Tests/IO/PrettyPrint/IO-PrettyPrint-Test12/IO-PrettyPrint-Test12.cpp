/*
 * Confirm that an std::vector can be pretty-printed.
 *
 * Author:      Stuart Rudderham
 * Created:     May 14, 2014
 * Last Edited: May 14, 2014
 */

#include "IO/Print.hpp"                 // ioCout
#include "IO/PrettyPrint/Vector.hpp"    // operator<<(std::ostream&, const std::vector<T, A>&)
#include <vector>                       // std::vector
#include <string>                       // std::string

int Test()
{
    // Empty.
    {
        std::vector<int> a1;
        ioCout( a1 );

        std::vector<std::string> a2;
        ioCout( a2 );

        ioCout( std::vector<double>() );
    }

    // With 1 element.
    {
        std::vector<int> a1{ 1 };
        ioCout( a1 );

        std::vector<std::string> a2{ "1" };
        ioCout( a2 );

        ioCout( std::vector<double>{ 1.5 } );
    }

    // With 2 elements.
    {
        std::vector<int> a1{ 1, 2 };
        ioCout( a1 );

        std::vector<std::string> a2{ "1", "2" };
        ioCout( a2 );

        ioCout( std::vector<double>{ 1.5, 2.5 } );
    }

    // With 3 elements.
    {
        std::vector<int> a1{ 1, 2, 3 };
        ioCout( a1 );

        std::vector<std::string> a2{ "1", "2", "3" };
        ioCout( a2 );

        ioCout( std::vector<double>{ 1.5, 2.5, 3.5 } );
    }

    // With 5 elements.
    {
        std::vector<int> a1{ 1, 2, 3, 4, 5 };
        ioCout( a1 );

        std::vector<std::string> a2{ "1", "2", "3", "4", "5" };
        ioCout( a2 );

        ioCout( std::vector<double>{ 1.5, 2.5, 3.5, 4.5, 5.5 } );
    }

    // Nested.
    {
        std::vector<std::vector<int>> a1;
        ioCout( a1 );

        std::vector<std::vector<int>> a2{ std::vector<int>{1, 2},
                                          std::vector<int>{3, 4},
                                          std::vector<int>{5, 6} };
        ioCout( a2 );
    }

    return 0;
}
