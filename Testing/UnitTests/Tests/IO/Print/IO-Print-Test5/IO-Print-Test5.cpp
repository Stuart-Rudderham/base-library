/*
 * Basic sanity test for ioPrintln.
 *
 * Author:      Stuart Rudderham
 * Created:     October 2, 2013
 * Last Edited: May 13, 2014
 */

#include "IO/Print.hpp"     // ioPrintln
#include <iostream>         // std::cout
#include <sstream>          // std::stringstream
#include <ios>              // std::hex, std::showbase

struct Point
{
    float x;
    float y;

    Point( float x, float y ) : x(x), y(y) {}
};

std::ostream& operator<<( std::ostream& out, const Point& p )
{
    out << "(" << p.x << ", " << p.y << ")";
    return out;
}

int Test()
{
    std::stringstream stream;

    int i = 10;
    int h = 0xFF;
    float f = 11.5f;
    int* iPtr = nullptr;
    const char* s = "Test String";
    char c = 'a';
    Point p( 11.0f, 12.0f );

    // Test printing to an std::stringstream.
    ioPrintln( stream );                                            // Test with no arguments.
    ioPrintln( stream, "" );                                        // Test with empty string.
    ioPrintln( stream, "int i = ", i    );                          // Test with some numbers.
    ioPrintln( stream, "float f = ", f    );
    ioPrintln( stream, "int h = ", std::hex, std::showbase, h );    // Test with format flags.
    ioPrintln( stream, "int* iPtr = ", iPtr );                      // Test with a pointer.
    ioPrintln( stream, "const char* s = ", s );                     // Test with some letters.
    ioPrintln( stream, "char c = ", c );
    ioPrintln( stream, "Point p = ", p );                           // Test with a user defined type.

    // Print to the console.
    ioPrintln( std::cout, stream.str() );

    return 0;
}
