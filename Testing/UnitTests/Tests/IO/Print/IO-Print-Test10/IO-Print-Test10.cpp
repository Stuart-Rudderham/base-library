/*
 * Basic sanity test for ioCerr.
 *
 * Author:      Stuart Rudderham
 * Created:     October 2, 2013
 * Last Edited: May 13, 2014
 */

#include "IO/Print.hpp"     // ioCerr
#include <ios>              // std::hex, std::showbase

struct Point
{
    float x;
    float y;

    Point( float x, float y ) : x(x), y(y) {}
};

std::ostream& operator<<( std::ostream& out, const Point& p )
{
    out << "(" << p.x << ", " << p.y << ")";
    return out;
}

int Test()
{
    int i = 10;
    int h = 0xFF;
    float f = 11.5f;
    int* iPtr = nullptr;
    const char* s = "Test String";
    char c = 'a';
    Point p( 11.0f, 12.0f );

    ioCerr( "" );                                           // Test with empty string.
    ioCerr( "int i = ", i    );                             // Test with some numbers.
    ioCerr( "float f = ", f    );
    ioCerr( "int h = ", std::hex, std::showbase, h );       // Test with format flags.
    ioCerr( "int* iPtr = ", iPtr );                         // Test with a pointer.
    ioCerr( "const char* s = ", s );                        // Test with some letters.
    ioCerr( "char c = ", c );
    ioCerr( "Point p = ", p );                              // Test with a user defined type.

    return 0;
}
