/*
 * Confirm that arguments to ioPrintln are forwarded perfectly.
 *
 * Author:      Stuart Rudderham
 * Created:     December 17, 2013
 * Last Edited: May 20, 2014
 */

#include "IO/Print.hpp"     // ioPrintln
#include <iostream>         // std::cout

struct Foo
{

};

// Different overloads for various cv-qualifiers and rvalue vs lvalue references.
std::ostream& operator<<( std::ostream& stream, Foo& )
{
    stream << "Foo&";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, const Foo& )
{
    stream << "const Foo&";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, volatile Foo& )
{
    stream << "volatile Foo&";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, const volatile Foo& )
{
    stream << "const volatile Foo&";
    return stream;
}


std::ostream& operator<<( std::ostream& stream, Foo&& )
{
    stream << "Foo&&";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, const Foo&& )
{
    stream << "const Foo&&";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, volatile Foo&& )
{
    stream << "volatile Foo&&";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, const volatile Foo&& )
{
    stream << "const volatile Foo&&";
    return stream;
}

// Creation functions for getting rvalues with cv-qualifiers.
Foo F1()
{
    return Foo();
}

const Foo F2()
{
    return Foo();
}

volatile Foo F3()
{
    return Foo();
}

const volatile Foo F4()
{
    return Foo();
}

int Test()
{
    // Create lvalues.
    Foo f1;
    const Foo f2;
    volatile Foo f3;
    const volatile Foo f4;

    // Printing lvalues.
    ioPrintln( std::cout, f1 );
    ioPrintln( std::cout, f2 );
    ioPrintln( std::cout, f3 );
    ioPrintln( std::cout, f4 );

    // Printing rvalues.
    ioPrintln( std::cout, F1() );
    ioPrintln( std::cout, F2() );
    ioPrintln( std::cout, F3() );
    ioPrintln( std::cout, F4() );

    ioPrintln( std::cout, Foo() );

    // Printing all of them at once.
    ioPrintln( std::cout, f1, ", ", f2, ", ", f3, ", ", f4, ", ", F1(), ", ", F2(), ", ", F3(), ", ", F4(), ", ", Foo() );

    return 0;
}

