/*
 * Confirm that ioPrint can accept non-copyable arguments.
 *
 * Author:      Stuart Rudderham
 * Created:     November 15, 2013
 * Last Edited: May 13, 2014
 */

#include "IO/Print.hpp"     // ioPrint
#include <iostream>         // std::cout

struct Foo
{
    Foo() = default;

    // No copy.
    Foo( const Foo& )            = delete;
    Foo& operator=( const Foo& ) = delete;

    // No move.
    Foo( Foo&& )                 = delete;
    Foo& operator=( Foo&& )      = delete;
};

std::ostream& operator<<( std::ostream& out, const Foo& )
{
    out << "Foo";
    return out;
}

int Test()
{
    Foo x;

    ioPrint( std::cout, x     );    // Print lvalue.
    ioPrint( std::cout, Foo() );    // Print rvalue.

    return 0;
}


