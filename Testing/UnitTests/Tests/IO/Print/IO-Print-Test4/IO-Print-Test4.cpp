/*
 * Confirm that ioPrint can accept *lots* of arguments.
 *
 * Author:      Stuart Rudderham
 * Created:     October 22, 2013
 * Last Edited: May 14, 2014
 */

#include "IO/Print.hpp"     // ioPrint
#include <iostream>         // std::cout

// Recursive macros to easily produce an exponential number of arguments.
#define Level1(arg) arg         , arg
#define Level2(arg) Level1(arg) , Level1(arg)
#define Level3(arg) Level2(arg) , Level2(arg)
#define Level4(arg) Level3(arg) , Level3(arg)
#define Level5(arg) Level4(arg) , Level4(arg)
#define Level6(arg) Level5(arg) , Level5(arg)

int Test()
{
    // Pass 64 arguments.
    ioPrint( std::cout, Level6("foo") );

    return 0;
}


