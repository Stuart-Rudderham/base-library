/*
 * Basic sanity test for ioPrint.
 *
 * Author:      Stuart Rudderham
 * Created:     October 2, 2013
 * Last Edited: May 12, 2014
 */

#include "IO/Print.hpp"     // ioPrint
#include <iostream>         // std::cout
#include <sstream>          // std::stringstream
#include <ios>              // std::hex, std::showbase

struct Point
{
    float x;
    float y;

    Point( float x, float y ) : x(x), y(y) {}
};

std::ostream& operator<<( std::ostream& out, const Point& p )
{
    out << "(" << p.x << ", " << p.y << ")";
    return out;
}

int Test()
{
    std::stringstream stream;

    int i = 10;
    int h = 0xFF;
    float f = 11.5f;
    int* iPtr = nullptr;
    const char* s = "Test String";
    char c = 'a';
    Point p( 11.0f, 12.0f );

    // Test printing to an std::stringstream.
    ioPrint( stream );                                              // Test with no arguments.
    ioPrint( stream, "" );                                          // Test with empty string.
    ioPrint( stream, "int i = ", i    );                            // Test with some numbers.
    ioPrint( stream, "float f = ", f    );
    ioPrint( stream, "int h = ", std::hex, std::showbase, h );      // Test with format flags.
    ioPrint( stream, "int* iPtr = ", iPtr );                        // Test with a pointer.
    ioPrint( stream, "const char* s = ", s );                       // Test with some letters.
    ioPrint( stream, "char c = ", c );
    ioPrint( stream, "Point p = ", p );                             // Test with a user defined type.

    // Print to the console.
    ioPrint( std::cout, stream.str() );

    return 0;
}
