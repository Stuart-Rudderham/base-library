/*
 * Confirm that ioVerbose prints when enabled.
 *
 * Author:      Stuart Rudderham
 * Created:     May 15, 2014
 * Last Edited: May 15, 2014
 */

#define ENABLE_VERBOSE_OUTPUT 1

#include "IO/Print.hpp"     // ioVerbose, ioCout

int Bar()
{
    ioCout( "Calling Bar" );
    return 10;
}

int Foo( int i )
{
    ioCout( "Calling Foo" );
    return i;
}

int Test()
{
    ioVerbose( Foo( Bar() ) );
    return 0;
}
