/*
 * Confirm that ioPrintln can accept non-copyable arguments.
 *
 * Author:      Stuart Rudderham
 * Created:     November 15, 2013
 * Last Edited: May 13, 2014
 */

#include "IO/Print.hpp"     // ioPrintln
#include <iostream>         // std::cout

struct Foo
{
    Foo() = default;

    // No copy.
    Foo( const Foo& )            = delete;
    Foo& operator=( const Foo& ) = delete;

    // No move.
    Foo( Foo&& )                 = delete;
    Foo& operator=( Foo&& )      = delete;
};

std::ostream& operator<<( std::ostream& out, const Foo& )
{
    out << "Foo";
    return out;
}

int Test()
{
    Foo x;

    ioPrintln( std::cout, x     );  // Print lvalue.
    ioPrintln( std::cout, Foo() );  // Print rvalue.

    return 0;
}


