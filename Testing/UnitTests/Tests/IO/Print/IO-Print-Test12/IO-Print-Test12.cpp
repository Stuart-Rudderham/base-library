/*
 * Confirm that ioVerbose does not print when disabled and doesn't evaluate its arguments.
 *
 * Author:      Stuart Rudderham
 * Created:     May 15, 2014
 * Last Edited: June 5, 2014
 */

#define ENABLE_VERBOSE_OUTPUT 0

#include "IO/Print.hpp"     // ioVerbose, ioCout

int Bar()
{
    ioCout( "Calling Bar" );
    return 10;
}

int Foo( int i )
{
    ioCout( "Calling Foo" );
    return i;
}

int Test()
{
    ioVerbose( Foo( Bar() ) );
    return 0;
}
