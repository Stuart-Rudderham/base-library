/*
 * Basic sanity test for ioCout.
 *
 * Author:      Stuart Rudderham
 * Created:     October 2, 2013
 * Last Edited: May 13, 2014
 */

#include "IO/Print.hpp"     // ioCout
#include <ios>              // std::hex, std::showbase

struct Point
{
    float x;
    float y;

    Point( float x, float y ) : x(x), y(y) {}
};

std::ostream& operator<<( std::ostream& out, const Point& p )
{
    out << "(" << p.x << ", " << p.y << ")";
    return out;
}

int Test()
{
    int i = 10;
    int h = 0xFF;
    float f = 11.5f;
    int* iPtr = nullptr;
    const char* s = "Test String";
    char c = 'a';
    Point p( 11.0f, 12.0f );

    ioCout( "" );                                           // Test with empty string.
    ioCout( "int i = ", i    );                             // Test with some numbers.
    ioCout( "float f = ", f    );
    ioCout( "int h = ", std::hex, std::showbase, h );       // Test with format flags.
    ioCout( "int* iPtr = ", iPtr );                         // Test with a pointer.
    ioCout( "const char* s = ", s );                        // Test with some letters.
    ioCout( "char c = ", c );
    ioCout( "Point p = ", p );                              // Test with a user defined type.

    return 0;
}
