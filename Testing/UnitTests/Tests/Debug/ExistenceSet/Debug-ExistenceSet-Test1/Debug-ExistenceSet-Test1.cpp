/*
 * Confirm that Debug::ExistenceSet works properly.
 *
 * Author:      Stuart Rudderham
 * Created:     November 19, 2013
 * Last Edited: June 7, 2014
 */

#include "Debug/Test.hpp"               // dbAssert
#include "Debug/ExistenceSet.hpp"
#include <vector>
#include <algorithm>
#include <tuple>

int Test()
{
    Debug::ExistenceSet<int> existenceSet;
    unsigned int setSize = 0;

    // Create a bunch of objects.
    int* i1 = new int;
    existenceSet.Add( i1 );

    setSize = existenceSet.GetSize();
    dbAssert( setSize == 1, "Expected set size = 1, got set size = ", setSize );

    int* i2 = new int;
    existenceSet.Add( i2 );

    setSize = existenceSet.GetSize();
    dbAssert( setSize == 2, "Expected set size = 2, got set size = ", setSize );

    int* i3 = new int;
    existenceSet.Add( i3 );

    setSize = existenceSet.GetSize();
    dbAssert( setSize == 3, "Expected set size = 3, got set size = ", setSize );

    // Erase things from the set.
    existenceSet.Remove( i2 );

    setSize = existenceSet.GetSize();
    dbAssert( setSize == 2, "Expected set size = 2, got set size = ", setSize );

    existenceSet.Remove( i3 );

    setSize = existenceSet.GetSize();
    dbAssert( setSize == 1, "Expected set size = 1, got set size = ", setSize );

    existenceSet.Remove( i1 );

    setSize = existenceSet.GetSize();
    dbAssert( setSize == 0, "Expected set size = 0, got set size = ", setSize );

    return 0;
}
