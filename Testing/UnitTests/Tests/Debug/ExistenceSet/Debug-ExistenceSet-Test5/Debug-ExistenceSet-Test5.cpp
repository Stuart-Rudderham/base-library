/*
 * Confirm that double add to a Debug::ExistenceSet is ignored if disabled.
 *
 * Author:      Stuart Rudderham
 * Created:     November 19, 2013
 * Last Edited: June 7, 2014
 */

#define ENABLE_EXISTENCE_TRACKING 0

#include "Debug/ExistenceSet.hpp"
#include <vector>
#include <algorithm>
#include <tuple>

int Test()
{
    Debug::ExistenceSet<int> existenceSet;

    // Create an object and add it to the set twice. This should be fine.
    int* i1 = new int;
    existenceSet.Add( i1 );
    existenceSet.Add( i1 );

    return 0;
}


