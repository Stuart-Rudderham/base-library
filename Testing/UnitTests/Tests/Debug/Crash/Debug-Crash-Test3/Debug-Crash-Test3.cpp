/*
 * Confirm that dbCrash() correctly forwards its arguments.
 *
 * Author:      Stuart Rudderham
 * Created:     June 3, 2014
 * Last Edited: June 3, 2014
 */

#define ENABLE_BREAKPOINTS 1

#include "Debug/Crash.hpp"      // dbCrash

// A class that prints different strings based on if it's an rvalue or lvalue.
class Foo
{

};

std::ostream& operator<<( std::ostream& stream, Foo& )
{
    stream << "lvalue ";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, Foo&& )
{
    stream << "rvalue ";
    return stream;
}

int Test()
{
    // Crash the program.
    Foo f;
    dbCrash( "Die ", f, Foo() );

    return 0;
}


