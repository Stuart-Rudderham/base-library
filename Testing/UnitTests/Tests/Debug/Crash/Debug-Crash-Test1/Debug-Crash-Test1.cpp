/*
 * Confirm that dbCrash() will crash the program.
 *
 * Author:      Stuart Rudderham
 * Created:     October 2, 2013
 * Last Edited: June 3, 2014
 */

#define ENABLE_BREAKPOINTS 1

#include "Debug/Crash.hpp"      // dbCrash
#include "IO/Print.hpp"         // ioCerr

// Class that prints on construction and destruction
struct Foo
{
    Foo()
    {
        ioCerr( "Constructor" );
    }

    ~Foo()
    {
        ioCerr( "Destructor" );
    }
};

// Static global Foo. Its constructor should run but its destructor shouldn't.
static Foo foo;

int Test()
{
    // Crash the program.
    dbCrash( "Die", 1 );

    return 0;
}


