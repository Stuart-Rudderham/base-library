/*
 * Confirm that an enabled dbErrorCheck() correctly forwards its arguments.
 *
 * Author:      Stuart Rudderham
 * Created:     June 5, 2014
 * Last Edited: June 5, 2014
 */

#define ENABLE_ERROR_CHECKING 1

#include "Debug/Test.hpp"           // dbErrorCheck
#include "IO/Print.hpp"             // ioCout
#include <iostream>                 // std::ostream

// A class that prints different strings based on if it's an rvalue or lvalue.
class Foo
{

};

std::ostream& operator<<( std::ostream& stream, Foo& )
{
    stream << "lvalue ";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, Foo&& )
{
    stream << "rvalue ";
    return stream;
}

int Test()
{
    // Assert.
    Foo f;
    dbErrorCheck( false, "Foo ", f, Foo() );

    // This line should not be reached.
    ioCout( "Bar" );

    return 0;
}
