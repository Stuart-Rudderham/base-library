/*
 * Confirm that dbErrorCheck still runs the condition when disabled.
 *
 * Author:      Stuart Rudderham
 * Created:     June 5, 2014
 * Last Edited: June 5, 2014
 */

#define ENABLE_ERROR_CHECKING 0
#include "Debug/Test.hpp"   // dbErrorCheck
#include "IO/Print.hpp"     // ioCout

int Check()
{
    ioCout( "Foo" );
    return 2;
}

int Test()
{
    // Check should not be triggered.
    dbErrorCheck( Check() == 0, "ErrorCheck triggered!" );

    return 0;
}


