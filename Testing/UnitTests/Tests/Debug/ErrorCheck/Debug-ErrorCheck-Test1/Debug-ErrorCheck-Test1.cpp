/*
 * Confirm that a failed dbErrorCheck() will crash the program.
 *
 * Author:      Stuart Rudderham
 * Created:     June 5, 2014
 * Last Edited: June 5, 2014
 */

#define ENABLE_ERROR_CHECKING 1
#include "Debug/Test.hpp"   // dbErrorCheck
#include "IO/Print.hpp"     // ioCout

int Check()
{
    ioCout( "Check" );
    return 2;
}

int Test()
{
    // Trigger the check, crash the program.
    dbErrorCheck( Check() == 0, "ErrorCheck triggered!" );

    return 0;
}


