/*
 * Confirm that a triggered dbWarn() will print out a message and not crash the program.
 *
 * Author:      Stuart Rudderham
 * Created:     October 1, 2013
 * Last Edited: June 5, 2014
 */

#define ENABLE_WARNINGS 1
#include "Debug/Test.hpp"   // dbWarn

int Test()
{
    // Trigger the warning, program should exit normally.
    dbWarn( false, "Warning triggered!" );

    return 0;
}


