/*
 * Confirm that an enabled dbWarn() correctly forwards its arguments.
 *
 * Author:      Stuart Rudderham
 * Created:     June 5, 2014
 * Last Edited: June 5, 2014
 */

#define ENABLE_WARNINGS 1

#include "Debug/Test.hpp"           // dbWarn
#include <iostream>                 // std::ostream

// A class that prints different strings based on if it's an rvalue or lvalue.
class Foo
{

};

std::ostream& operator<<( std::ostream& stream, Foo& )
{
    stream << "lvalue ";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, Foo&& )
{
    stream << "rvalue ";
    return stream;
}

int Test()
{
    // Warn.
    Foo f;
    dbWarn( false, "Foo ", f, Foo() );

    return 0;
}
