/*
 * Confirm that dbWarn() can be disabled.
 *
 * Author:      Stuart Rudderham
 * Created:     October 1, 2013
 * Last Edited: June 7, 2014
 */

// Disable warnings.
#define ENABLE_WARNINGS 0
#include "Debug/Test.hpp"   // dbWarn

int Test()
{
    // Try and trigger it, nothing should be printed.
    dbWarn( false, "Shouldn't be triggered!" );

    return 0;
}


