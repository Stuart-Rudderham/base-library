/*
 * Confirm that an untriggered dbWarn will not print out a message or crash the program.
 *
 * Author:      Stuart Rudderham
 * Created:     October 1, 2013
 * Last Edited: June 5, 2014
 */

#define ENABLE_WARNINGS 1
#include "Debug/Test.hpp"   // dbWarn

int Test()
{
    // Don't trigger the warning, nothing should be printed.
    dbWarn( true, "Should not be hit!" );

    return 0;
}


