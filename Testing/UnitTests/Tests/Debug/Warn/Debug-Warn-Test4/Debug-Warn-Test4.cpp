/*
 * Confirm that a disabled dbWarn() doesn't exaluate its arguments.
 *
 * Author:      Stuart Rudderham
 * Created:     October 1, 2013
 * Last Edited: June 5, 2014
 */

// Disable warnings.
#define ENABLE_WARNINGS 0
#include "Debug/Test.hpp"   // dbWarn

// A function that returns true, so it won't trigger the warning. But it will crash
// the program if it is ever called.
bool F()
{
    dbCrash( "Shouldn't be evaluated!" );
    return true;
}

int Test()
{
    // Since the warning is disabled F() shouldn't be called, and the program should exit normally.
    dbWarn( F(), "Shouldn't trigger!" );

    return 0;
}

