/*
 * Confirm that an enabled dbBreak() correctly forwards its arguments.
 *
 * Author:      Stuart Rudderham
 * Created:     June 3, 2014
 * Last Edited: June 3, 2014
 */

#define ENABLE_BREAKPOINTS 1

#include "Debug/Breakpoint.hpp"     // dbBreak
#include "IO/Print.hpp"             // ioCout
#include <iostream>                 // std::ostream

// A class that prints different strings based on if it's an rvalue or lvalue.
class Foo
{

};

std::ostream& operator<<( std::ostream& stream, Foo& )
{
    stream << "lvalue ";
    return stream;
}

std::ostream& operator<<( std::ostream& stream, Foo&& )
{
    stream << "rvalue ";
    return stream;
}

int Test()
{
    // Insert a breakpoint.
    Foo f;
    dbBreak( "Foo ", f, Foo() );

    // This line should not be reached.
    ioCout( "Bar" );

    return 0;
}
