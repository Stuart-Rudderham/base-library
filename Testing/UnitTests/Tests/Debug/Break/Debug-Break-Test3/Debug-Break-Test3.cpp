/*
 * Confirm that a disabled dbBreak() does not evaluate its arguments.
 *
 * Author:      Stuart Rudderham
 * Created:     June 3, 2014
 * Last Edited: June 3, 2014
 */

#define ENABLE_BREAKPOINTS 0

#include "Debug/Breakpoint.hpp"     // dbBreak
#include "IO/Print.hpp"             // ioCout

// A function that prints output when evaluated.
int Foo()
{
    ioCout( "Foo" );
    return 10;
}

int Test()
{
    // Insert a breakpoint. The function Foo shouldn't be called.
    dbBreak( "Output message: ", Foo() );

    // This line should be reached.
    ioCout( "Bar" );

    return 0;
}


