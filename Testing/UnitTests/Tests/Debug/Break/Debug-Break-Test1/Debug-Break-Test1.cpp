/*
 * Confirm that dbBreak() correctly inserts a breakpoint.
 *
 * Author:      Stuart Rudderham
 * Created:     June 3, 2014
 * Last Edited: June 3, 2014
 */

#define ENABLE_BREAKPOINTS 1

#include "Debug/Breakpoint.hpp"     // dbBreak
#include "IO/Print.hpp"             // ioCout

int Test()
{
    // Insert a breakpoint.
    dbBreak( "Foo", 2 );

    // This line should not be reached.
    ioCout( "Bar" );

    return 0;
}
