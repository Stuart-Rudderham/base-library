/*
 * Confirm that dbBreak() can be disabled.
 *
 * Author:      Stuart Rudderham
 * Created:     June 3, 2014
 * Last Edited: June 3, 2014
 */

#define ENABLE_BREAKPOINTS 0

#include "Debug/Breakpoint.hpp"     // dbBreak
#include "IO/Print.hpp"             // ioCout

int Test()
{
    // Insert a breakpoint. This shouldn't do anything.
    dbBreak( "Foo" );

    // This line should be reached.
    ioCout( "Bar" );

    return 0;
}


