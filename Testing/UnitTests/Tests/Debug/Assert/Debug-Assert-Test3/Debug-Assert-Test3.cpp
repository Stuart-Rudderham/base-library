/*
 * Confirm that dbAssert() can be disabled.
 *
 * Author:      Stuart Rudderham
 * Created:     October 1, 2013
 * Last Edited: June 5, 2014
 */

// Disable asserts.
#define ENABLE_ASSERTS 0
#include "Debug/Test.hpp"   // dbAssert

int Test()
{
    // Try and trigger it, program should still exit normally.
    dbAssert( false, "Shouldn't be triggered!" );

    return 0;
}


