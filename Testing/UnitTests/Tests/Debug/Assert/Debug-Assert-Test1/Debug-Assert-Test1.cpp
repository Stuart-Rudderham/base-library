/*
 * Confirm that a triggered dbAssert() will crash the program.
 *
 * Author:      Stuart Rudderham
 * Created:     October 1, 2013
 * Last Edited: June 5, 2014
 */

#define ENABLE_ASSERTS 1
#include "Debug/Test.hpp"   // dbAssert

int Test()
{
    // Trigger the assert, crash the program.
    dbAssert( false, "Assert triggered!" );

    return 0;
}


