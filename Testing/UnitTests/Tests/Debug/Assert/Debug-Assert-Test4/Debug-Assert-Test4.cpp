/*
 * Confirm that a disabled dbAssert() doesn't evaluate its arguments.
 *
 * Author:      Stuart Rudderham
 * Created:     October 1, 2013
 * Last Edited: June 5, 2014
 */

// Disable asserts.
#define ENABLE_ASSERTS 0
#include "Debug/Test.hpp"   // dbAssert

// A function that returns true, so it won't trigger the assert. But it will crash
// the program if it is ever called.
bool F()
{
    dbCrash( "Shouldn't be evaluated!" );
    return true;
}

int Test()
{
    // Since the assert is disabled F() shouldn't be called, and the program should exit normally.
    dbAssert( F(), "Shouldn't trigger!" );

    return 0;
}

