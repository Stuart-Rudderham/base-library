/*
 * Confirm that an untriggered dbAssert() will not crash the program.
 *
 * Author:      Stuart Rudderham
 * Created:     October 1, 2013
 * Last Edited: June 5, 2014
 */

#define ENABLE_ASSERTS 1
#include "Debug/Test.hpp"   // dbAssert

int Test()
{
    // Don't trigger the assert, program should exit normally.
    dbAssert( true, "Should not be hit!" );

    return 0;
}


