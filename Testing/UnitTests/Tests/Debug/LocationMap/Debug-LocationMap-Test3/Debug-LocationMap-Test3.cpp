/*
 * Confirm that double add to a Debug::LocationMap is caught.
 *
 * Author:      Stuart Rudderham
 * Created:     November 13, 2013
 * Last Edited: June 7, 2014
 */

#include "Debug/Location.hpp"       // dbLocation
#include "Debug/LocationMap.hpp"
#include <vector>
#include <algorithm>
#include <tuple>

int Test()
{
    Debug::LocationMap<int> locationMap;

    // Create an object and add it to the map twice. This should fail.
    int* i1 = new int;
    locationMap.Add( i1, dbLocation() );
    locationMap.Add( i1, dbLocation() );

    return 0;
}


