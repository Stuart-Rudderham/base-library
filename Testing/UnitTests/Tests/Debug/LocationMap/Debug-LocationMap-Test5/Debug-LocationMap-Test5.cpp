/*
 * Confirm that double add to a Debug::LocationMap is ignored if disabled.
 *
 * Author:      Stuart Rudderham
 * Created:     November 19, 2013
 * Last Edited: June 7, 2014
 */

#define ENABLE_FILE_LOCATION_TRACKING 0

#include "Debug/Location.hpp"       // dbLocation
#include "Debug/LocationMap.hpp"
#include <vector>
#include <algorithm>
#include <tuple>

int Test()
{
    Debug::LocationMap<int> locationMap;

    // Create an object and add it to the map twice. This should be fine.
    int* i1 = new int;
    locationMap.Add( i1, dbLocation() );
    locationMap.Add( i1, dbLocation() );

    return 0;
}


