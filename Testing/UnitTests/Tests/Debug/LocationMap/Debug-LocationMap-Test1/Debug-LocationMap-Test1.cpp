/*
 * Confirm that Debug::LocationMap works properly.
 *
 * Author:      Stuart Rudderham
 * Created:     November 12, 2013
 * Last Edited: June 7, 2014
 */

#include "Debug/Test.hpp"           // dbAssert
#include "Debug/Location.hpp"       // dbLocation
#include "Debug/LocationMap.hpp"
#include <vector>
#include <algorithm>
#include <tuple>
#include "IO/Print.hpp"         // ioCout

int Test()
{
    Debug::LocationMap<int> locationMap;
    unsigned int mapSize = 0;

    // Create a bunch of objects and record their file location.
    int* i1 = new int;
    locationMap.Add( i1, dbLocation() );

    mapSize = locationMap.GetSize();
    dbAssert( mapSize == 1, "Expected map size = 1, got map size = ", mapSize );

    int* i2 = new int;
    locationMap.Add( i2, dbLocation() );

    mapSize = locationMap.GetSize();
    dbAssert( mapSize == 2, "Expected map size = 2, got map size = ", mapSize );

    int* i3 = new int;
    locationMap.Add( i3, dbLocation() );

    mapSize = locationMap.GetSize();
    dbAssert( mapSize == 3, "Expected map size = 3, got map size = ", mapSize );

    // Print out everything.
    for( const auto& i : locationMap.GetContents() ) {
        ioCout( i.first, " - ", i.second );
        ioCout( "" );
    }

    // Erase things from the map.
    locationMap.Remove( i2 );

    mapSize = locationMap.GetSize();
    dbAssert( mapSize == 2, "Expected map size = 2, got map size = ", mapSize );

    locationMap.Remove( i3 );

    mapSize = locationMap.GetSize();
    dbAssert( mapSize == 1, "Expected map size = 1, got map size = ", mapSize );

    locationMap.Remove( i1 );

    mapSize = locationMap.GetSize();
    dbAssert( mapSize == 0, "Expected map size = 0, got map size = ", mapSize );

    return 0;
}


