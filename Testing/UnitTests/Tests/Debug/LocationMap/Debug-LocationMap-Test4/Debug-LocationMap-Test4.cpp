/*
 * Confirm that double delete from a Debug::LocationMap is caught.
 *
 * Author:      Stuart Rudderham
 * Created:     November 13, 2013
 * Last Edited: June 7, 2014
 */

#include "Debug/Location.hpp"       // dbLocation
#include "Debug/LocationMap.hpp"
#include <vector>
#include <algorithm>
#include <tuple>

int Test()
{
    Debug::LocationMap<int> locationMap;

    // Create an object and add it to the map.
    int* i1 = new int;
    locationMap.Add( i1, dbLocation() );

    // Remove it twice. This should fail.
    locationMap.Remove( i1 );
    locationMap.Remove( i1 );

    return 0;
}


