/*
 * Confirm that an enabled poly_cast will catch an invalid cast.
 *
 * Author:      Stuart Rudderham
 * Created:     October 2, 2013
 * Last Edited: June 7, 2014
 */

#define ENABLE_POLY_CAST 1
#include "Debug/Cast.hpp"       // poly_cast

struct Base
{
    virtual ~Base()
    {

    }
};

struct Derived1 : public Base
{

};

struct Derived2 : public Base
{

};

int Test()
{
    // Invalid cast is caught.
    Base* b = new Derived1;
    Derived2* d2 = poly_cast<Derived2*>( b );

    (void)b;
    (void)d2;

    return 0;
}
