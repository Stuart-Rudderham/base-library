/*
 * Confirm that an enabled poly_cast will be fine with a valid cast.
 *
 * Author:      Stuart Rudderham
 * Created:     June 7, 2014
 * Last Edited: June 7, 2014
 */

#define ENABLE_POLY_CAST 1
#include "Debug/Cast.hpp"       // poly_cast

struct Base
{
    virtual ~Base()
    {

    }
};

struct Derived1 : public Base
{

};

struct Derived2 : public Base
{

};

int Test()
{
    // Valid cast is fine.
    Base* b = new Derived1;
    Derived1* d1 = poly_cast<Derived1*>( b );

    (void)b;
    (void)d1;

    return 0;
}
