/*
 * Confirm that a disabled poly_cast will ignored an invalid cast.
 *
 * Author:      Stuart Rudderham
 * Created:     June 7, 2014
 * Last Edited: June 7, 2014
 */

#define ENABLE_POLY_CAST 0
#include "Debug/Cast.hpp"       // poly_cast

struct Base
{
    virtual ~Base()
    {

    }
};

struct Derived1 : public Base
{

};

struct Derived2 : public Base
{

};

int Test()
{
    // Invalid cast is ignored.
    Base* b = new Derived1;
    Derived2* d2 = poly_cast<Derived2*>( b );

    (void)b;
    (void)d2;

    return 0;
}
