/*
 * Confirm that a derived class can override the base class allocator.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Debug/Location.hpp"   // dbLocation
#include "Typename.hpp"
#include "IO/Print.hpp"         // ioCout

struct Base
{
    // This is defined by the test for each specific implementation.
    SET_BASE_IALLOCATOR_IMPL();
    int y;

    virtual ~Base()
    {

    }
};

SetTypenameString( Base );

struct Derived : public Base
{
    // This is defined by the test for each specific implementation.
    SET_DERIVED_IALLOCATOR_IMPL();
    int x;

    virtual ~Derived()
    {
        ioCout( "Derived::~Derived()" );
    }
};

SetTypenameString( Derived );

void SanityCheck( int expectedCur, int expectedMax )
{
    int curAllocated = Derived::GetAllocator().CurAllocated();
    int maxAllocated = Derived::GetAllocator().MaxAllocated();

    dbAssert( curAllocated == expectedCur, "Expected ", expectedCur, " got ", curAllocated );
    dbAssert( maxAllocated == expectedMax, "Expected ", expectedMax, " got ", maxAllocated );
}

int Test()
{
    // Create a Derived. This should use the Derived allocator
    // rather than the Base allocator.
    Base* b = new ( dbLocation() ) Derived;
    SanityCheck(1, 1);

    // Delete through the Base pointer. It should use the Derived allocator.
    delete b;
    SanityCheck(0, 1);

    return 0;
}



