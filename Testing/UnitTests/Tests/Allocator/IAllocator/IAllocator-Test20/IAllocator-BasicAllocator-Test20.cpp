/*
 * Confirm that IAllocator-Test20 passes when using the BasicAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#include "Allocator/BasicAllocator.hpp"

#define SET_BASE_IALLOCATOR_IMPL() USE_BASIC_ALLOCATOR(Base);
#define SET_DERIVED_IALLOCATOR_IMPL() USE_BASIC_ALLOCATOR(Derived);

#include "IAllocator-Test20.hpp"
