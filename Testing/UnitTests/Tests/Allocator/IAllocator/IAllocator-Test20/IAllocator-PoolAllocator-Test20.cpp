/*
 * Confirm that IAllocator-Test20 passes when using the PoolAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#include "Allocator/PoolAllocator.hpp"

#define SET_BASE_IALLOCATOR_IMPL() USE_POOL_ALLOCATOR(Base, 10);
#define SET_DERIVED_IALLOCATOR_IMPL() USE_POOL_ALLOCATOR(Derived, 10);

#include "IAllocator-Test20.hpp"
