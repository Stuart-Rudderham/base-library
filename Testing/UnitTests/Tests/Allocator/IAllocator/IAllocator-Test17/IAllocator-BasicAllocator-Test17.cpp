/*
 * Confirm that IAllocator-Test17 passes when using the BasicAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#include "Allocator/BasicAllocator.hpp"

#define SET_LIBRARY_CLASS_WRAPPER_IALLOCATOR_IMPL() USE_BASIC_ALLOCATOR(LibraryClassWrapper);

#include "IAllocator-Test17.hpp"
