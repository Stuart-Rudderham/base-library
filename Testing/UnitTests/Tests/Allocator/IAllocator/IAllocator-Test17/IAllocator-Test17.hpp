/*
 * Confirm that the IAllocator can be used with arbitrary types
 * by using a wrapper struct.
 *
 * Author:      Stuart Rudderham
 * Created:     October 29, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Debug/Location.hpp"   // dbLocation
#include <string>

// The IAllocator works by overriding the operator new for a class.
// This isn't possible for builtin types like int, or for classes that
// we can't modify the source code for. It is still possible to use the
// IAllocator with these types though, if we use a wrapper class.
// The only requirement is that the base class has a virtual destructor.

// Pretend we cannot modify the source code for this class
struct LibraryClass
{
    // There can be anything in this class.
    LibraryClass( int i )
    {
        (void)i;
    }

    LibraryClass( std::string s )
    {
        (void)s;
    }

    int F()
    {
        return 10;
    }

    // It has to have a a virtual dtor, however.
    virtual ~LibraryClass()
    {

    }
};

struct LibraryClassWrapper : public LibraryClass
{
    // Inherit ctors from base class.
    using LibraryClass::LibraryClass;

    LibraryClassWrapper( int i )
        : LibraryClass( i )
    {

    }

    LibraryClassWrapper( std::string s )
        : LibraryClass( s )
    {

    }

    // Do not add anything except for the IAllocator
    // functions.
    SET_LIBRARY_CLASS_WRAPPER_IALLOCATOR_IMPL();
};

// A function that has the LibraryClass as a parameter.
void Foo( LibraryClass* c )
{
    dbAssert( c->F() == 10, "Did not equal 10!" );
}

int Test()
{
    // Create the LibraryClass using the wrapper class.
    LibraryClass* c1 = new ( dbLocation() ) LibraryClassWrapper( 0 );
    LibraryClassWrapper* c2 = new ( dbLocation() ) LibraryClassWrapper( "string" );

    // Use the wrapper class like you would the normal one.
    Foo( c1 );
    Foo( c2 );

    // Can delete through base class pointer since it has a virtual dtor.
    delete c1;
    delete c2;

    return 0;
}



