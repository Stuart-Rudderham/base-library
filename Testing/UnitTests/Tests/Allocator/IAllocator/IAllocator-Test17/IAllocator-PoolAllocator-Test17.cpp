/*
 * Confirm that IAllocator-Test17 passes when using the PoolAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#include "Allocator/PoolAllocator.hpp"

#define SET_LIBRARY_CLASS_WRAPPER_IALLOCATOR_IMPL() USE_POOL_ALLOCATOR(LibraryClassWrapper, 10);

#include "IAllocator-Test17.hpp"
