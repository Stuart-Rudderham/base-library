/*
 * Confirm works properly with std::shared_ptr.
 *
 * Author:      Stuart Rudderham
 * Created:     November 21, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Location.hpp"   // dbLocation
#include <memory>
#include <array>

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();
};

int Test()
{
    std::array< std::shared_ptr<Foo>, MAX_CREATE> arr;

    // Create a bunch of Foos.
    for( int i = 0; i < MAX_CREATE; ++i ) {
        arr[i].reset( new( dbLocation() ) Foo );
    }

    // Cleanup should happen automatically.

    return 0;
}



