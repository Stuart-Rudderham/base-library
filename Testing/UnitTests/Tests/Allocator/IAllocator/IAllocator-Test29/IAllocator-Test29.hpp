/*
 * Confirm that the disabled IAllocator implementation handles
 * out-of-memory errors correctly.
 *
 * Author:      Stuart Rudderham
 * Created:     November 4, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Crash.hpp"      // dbCrash
#include "Debug/Location.hpp"   // dbLocation
#include <new>                  // std::set_new_handler
#include <cstddef>              // SIZE_MAX

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();

    // Make sizeof(Foo) really big.
    //     32-bit systems
    //         SIZE_MAX     = 2^32 - 1 (~4GB)
    //         SIZE_MAX / 8 = 2^29 - 1 (~512MB)
    //     64-bit systems
    //         SIZE_MAX     = 2^64 - 1 (~16EB)
    //         SIZE_MAX / 8 = 2^61 - 1 (~2EB)
    //
    // For some reason Clang 3.4 on Linux 64-bit will fail if
    // we have a char array of size > 2^61. I would have though
    // the limit would be SIZE_MAX / 2 = 2^63, but whatever...
    char padding[ SIZE_MAX / 8 ];
};

int Test()
{
    // Set the new handler to crash.
    std::set_new_handler(
        []() {
            dbCrash("new_handler called");
        }
    );

    // Keep creating Foos until we run out of memory. The new handler
    // set above should be called when that happens.
    while( true ) {
        new ( dbLocation() ) Foo;
    }

    return 0;
}

