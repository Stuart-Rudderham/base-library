/*
 * Confirm that IAllocator-Test29 passes when using the BasicAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#define ENABLE_BASIC_ALLOCATOR 0

#include "Allocator/BasicAllocator.hpp"

#define SET_FOO_IALLOCATOR_IMPL() USE_BASIC_ALLOCATOR(Foo);

#include "IAllocator-Test29.hpp"
