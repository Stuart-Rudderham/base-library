/*
 * Confirm that IAllocator-Test29 passes when using the PoolAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#define ENABLE_POOL_ALLOCATOR 0

#include "Allocator/PoolAllocator.hpp"

#define SET_FOO_IALLOCATOR_IMPL() USE_POOL_ALLOCATOR(Foo, 10);

#include "IAllocator-Test29.hpp"
