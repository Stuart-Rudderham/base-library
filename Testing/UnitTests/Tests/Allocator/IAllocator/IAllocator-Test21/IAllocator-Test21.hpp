/*
 * Confirm that using the IAllocator macros with the wrong
 * type fails to compile.
 *
 * Author:      Stuart Rudderham
 * Created:     October 30, 2013
 * Last Edited: June 8, 2014
 */

struct Bar
{
    SET_BAR_IALLOCATOR_IMPL();
};

struct Foo
{
    // Uh oh, copy & paste error. Shouldn't compile.
    SET_FOO_IALLOCATOR_IMPL();
};

int Test()
{
    return 0;
}



