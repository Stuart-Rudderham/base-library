/*
 * Confirm that deletion of a nullptr is fine.
 *
 * Author:      Stuart Rudderham
 * Created:     October 30, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Location.hpp"   // dbLocation

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();
};

void SanityCheck( int expectedCur, int expectedMax )
{
    int curAllocated = Foo::GetAllocator().CurAllocated();
    int maxAllocated = Foo::GetAllocator().MaxAllocated();

    dbAssert( curAllocated == expectedCur, "Expected ", expectedCur, " got ", curAllocated );
    dbAssert( maxAllocated == expectedMax, "Expected ", expectedMax, " got ", maxAllocated );
}

int Test()
{
    // Create a NULL Foo.
    Foo* foo = nullptr;

    // Delete it. This should be fine.
    delete foo;
    SanityCheck(0, 0);

    return 0;
}



