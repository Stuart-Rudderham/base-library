/*
 * Confirm that a class that uses the IAllocator cannot be created using
 * the non-throwing operator new.
 *
 * Author:      Stuart Rudderham
 * Created:     October 28, 2013
 * Last Edited: November 26, 2013
 */

#include <new>

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();
};

int Test()
{
    // Create a Foo on the heap using the non-throwing operator new.
    // Shouldn't compile, since we've deleted that function.
    Foo* foo = new (std::nothrow) Foo;

    return 0;
}



