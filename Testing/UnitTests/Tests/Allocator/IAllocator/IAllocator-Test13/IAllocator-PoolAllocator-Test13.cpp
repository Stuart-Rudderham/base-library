/*
 * Confirm that IAllocator-Test13 passes when using the PoolAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#define ENABLE_MEMORY_CLEARING 1

#include "Allocator/PoolAllocator.hpp"

#define SET_DERIVED_IALLOCATOR_IMPL() USE_POOL_ALLOCATOR(Derived, 10);

#include "IAllocator-Test13.hpp"
