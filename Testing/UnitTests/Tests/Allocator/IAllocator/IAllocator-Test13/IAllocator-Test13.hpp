/*
 * Confirm that object memory is cleared when an object is deleted.
 *
 * Author:      Stuart Rudderham
 * Created:     October 30, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Debug/Location.hpp"   // dbLocation
#include <string>
#include <iostream>
#include <iomanip>

struct Base
{
    virtual int F()
    {
        return 1;
    }

    // Need virtual dtor.
    virtual ~Base()
    {

    }

    int z;
};

struct Derived : public Base
{
    // Ctor.
    Derived( int n )
        : x( n )
    {

    }

    // Override the virtual function in the base class.
    virtual int F() override
    {
        return 2;
    }

    // Add member variable.
    int x;
    int y;

    // This is defined by the test for each specific implementation.
    SET_DERIVED_IALLOCATOR_IMPL();
};

int Test()
{
    // Create a Derived.
    Derived* d = new ( dbLocation() ) Derived( 6 );
    Base* b = d;

    std::cout << "sizeof(Base) = " << sizeof(Base) << std::endl;
    std::cout << "sizeof(Derived) = " << sizeof(Derived) << std::endl;

    // Print out its memory
    std::cout << "Memory layout before [ ";
    for( unsigned int i = 0; i < sizeof(Derived); ++i ) {
        std::cout << std::hex
                  << std::setw(2)
                  << std::setfill('0')
                  << ( reinterpret_cast<unsigned char*>(d)[i] & 0xFF ) << " ";
    }
    std::cout << "]" << std::endl;

    // Make sure it's valid.
    dbAssert( d->x == 6 && b->F() == 2, "Not valid!" );

    // Delete it.
    delete b;

    // Print out its memory
    std::cout << "Memory layout after  [ ";
    for( unsigned int i = 0; i < sizeof(Derived); ++i ) {
        std::cout << std::hex
                  << std::setw(2)
                  << std::setfill('0')
                  << ( reinterpret_cast<unsigned char*>(d)[i] & 0xFF ) << " ";
    }
    std::cout << "]" << std::endl;

    // Check if memory was cleared. This assert should fail.
    dbAssert( d->x == 6, "Memory cleared, d->x = ", d->x );

    return 0;
}



