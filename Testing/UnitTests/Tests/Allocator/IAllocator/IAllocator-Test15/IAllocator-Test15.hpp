/*
 * Confirm that there are no memory leaks if a constructor throws
 * an exception.
 *
 * Author:      Stuart Rudderham
 * Created:     October 29, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Debug/Location.hpp"   // dbLocation
#include "IO/Print.hpp"         // ioCout

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();

    Foo()
    {
        throw 1;
    }
};

void SanityCheck( int expectedCur, int expectedMax )
{
    int curAllocated = Foo::GetAllocator().CurAllocated();
    int maxAllocated = Foo::GetAllocator().MaxAllocated();

    dbAssert( curAllocated == expectedCur, "Expected ", expectedCur, " got ", curAllocated );
    dbAssert( maxAllocated == expectedMax, "Expected ", expectedMax, " got ", maxAllocated );
}

int Test()
{
    // Try and create a Foo, which throws during construction.
    try {
        Foo* foo = new ( dbLocation() ) Foo;
        (void)foo;
    }
    catch(int e) {
        ioCout( "Exception caught!" );
        (void)e;
    }

    // The Foo should automatically be deleted as a result of throwing the exception.
    SanityCheck(0, 1);

    return 0;
}



