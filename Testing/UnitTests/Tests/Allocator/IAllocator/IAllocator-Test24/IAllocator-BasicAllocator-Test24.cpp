/*
 * Confirm that IAllocator-Test24 passes when using the BasicAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#define ENABLE_EXISTENCE_TRACKING 1
#define ENABLE_FILE_LOCATION_TRACKING 0

#include "Allocator/BasicAllocator.hpp"

#define SET_FOO_IALLOCATOR_IMPL() USE_BASIC_ALLOCATOR(Foo);

#include "IAllocator-Test24.hpp"
