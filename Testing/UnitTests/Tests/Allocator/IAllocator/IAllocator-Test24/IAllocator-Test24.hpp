/*
 * Confirm that deletion of a stack allocated object is caught.
 *
 * Author:      Stuart Rudderham
 * Created:     October 30, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Debug/Location.hpp"   // dbLocation

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();
};

void SanityCheck( int expectedCur, int expectedMax )
{
    int curAllocated = Foo::GetAllocator().CurAllocated();
    int maxAllocated = Foo::GetAllocator().MaxAllocated();

    dbAssert( curAllocated == expectedCur, "Expected ", expectedCur, " got ", curAllocated );
    dbAssert( maxAllocated == expectedMax, "Expected ", expectedMax, " got ", maxAllocated );
}

int Test()
{
    // Create a Foo on the stack and one on the heap.
    Foo foo1;
    SanityCheck(0, 0);

    Foo* foo2 = new ( dbLocation() ) Foo;
    SanityCheck(1, 1);

    // Delete the one on the stack. This should fail.
    delete &foo1;

    (void)foo2;

    return 0;
}



