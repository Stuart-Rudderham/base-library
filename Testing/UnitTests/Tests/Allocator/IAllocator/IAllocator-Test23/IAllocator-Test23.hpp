/*
 * Confirm that double deletion is caught.
 *
 * Author:      Stuart Rudderham
 * Created:     October 30, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Location.hpp"   // dbLocation

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();
};

int Test()
{
    // Create two Foos.
    Foo* foo1 = new ( dbLocation() ) Foo;
    Foo* foo2 = new ( dbLocation() ) Foo;

    // Delete one twice. This should fail.
    delete foo1;
    delete foo1;

    // Don't delete the other one.
    (void)foo2;

    return 0;
}



