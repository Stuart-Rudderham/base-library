/*
 * Confirm that the IAllocator works correctly with virtual delete.
 *
 * Author:      Stuart Rudderham
 * Created:     October 29, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Debug/Location.hpp"   // dbLocation

struct Base
{
    virtual ~Base()
    {

    }
};

struct Derived : public Base
{
    // This is defined by the test for each specific implementation.
    SET_DERIVED_IALLOCATOR_IMPL();
};

void SanityCheck( int expectedCur, int expectedMax )
{
    int curAllocated = Derived::GetAllocator().CurAllocated();
    int maxAllocated = Derived::GetAllocator().MaxAllocated();

    dbAssert( curAllocated == expectedCur, "Expected ", expectedCur, " got ", curAllocated );
    dbAssert( maxAllocated == expectedMax, "Expected ", expectedMax, " got ", maxAllocated );
}

int Test()
{
    // Create a Derived but store it in a Base pointer.
    Base* b = new ( dbLocation() ) Derived;
    SanityCheck(1, 1);

    // Delete through the base class pointer.
    delete b;
    SanityCheck(0, 1);

    return 0;
}



