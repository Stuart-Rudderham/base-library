/*
 * Confirm that inherited operator new fails if child class size
 * is different from the base class.
 *
 * Author:      Stuart Rudderham
 * Created:     October 30, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Debug/Location.hpp"   // dbLocation

struct Base
{
    // This is defined by the test for each specific implementation.
    SET_BASE_IALLOCATOR_IMPL();

    int y;

    virtual ~Base()
    {

    }
};

struct Derived : public Base
{
    int x;
};

int Test()
{
    // Derived and Base are different sizes.
    dbAssert( sizeof(Base) != sizeof(Derived), "Base and Derived are the same size!" );

    // Create a Derived object, which uses the Base allocator. This should fail since
    // a Derived object is the wrong size.
    Derived* d = new ( dbLocation() ) Derived;

    // Clean up. We should fail before here though.
    delete d;

    return 0;
}



