/*
 * Confirm that IAllocator-Test14 passes when using the BasicAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#define ENABLE_MEMORY_CLEARING 0

#include "Allocator/BasicAllocator.hpp"

#define SET_DERIVED_IALLOCATOR_IMPL() USE_BASIC_ALLOCATOR(Derived);

#include "IAllocator-Test14.hpp"
