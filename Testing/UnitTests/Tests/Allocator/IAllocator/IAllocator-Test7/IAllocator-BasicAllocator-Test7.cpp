/*
 * Confirm that IAllocator-Test7 passes when using the BasicAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#include "Allocator/BasicAllocator.hpp"

#define SET_FOO_IALLOCATOR_IMPL() USE_BASIC_ALLOCATOR(Foo);

#include "IAllocator-Test7.hpp"
