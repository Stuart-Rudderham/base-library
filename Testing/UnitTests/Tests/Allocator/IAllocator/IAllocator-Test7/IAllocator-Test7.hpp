/*
 * Confirm that a class that uses the IAllocator can be created using the
 * IAllocator placement operator new.
 *
 * Author:      Stuart Rudderham
 * Created:     October 28, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Location.hpp"   // dbLocation

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();
};

int Test()
{
    // Create a Foo on the heap using our version of the placment operator new.
    Foo* foo = new ( dbLocation() ) Foo;
    (void)foo;

    return 0;
}



