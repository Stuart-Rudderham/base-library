/*
 * Confirm that IAllocator-Test1 passes when using the PoolAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#include "Allocator/PoolAllocator.hpp"

#define SET_FOO_IALLOCATOR_IMPL() USE_POOL_ALLOCATOR(Foo, 10);

#include "IAllocator-Test1.hpp"

