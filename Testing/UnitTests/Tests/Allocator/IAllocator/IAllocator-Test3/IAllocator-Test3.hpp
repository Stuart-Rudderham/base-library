/*
 * Confirm that a class that uses the IAllocator cannot be created using
 * the placement operator new.
 *
 * Author:      Stuart Rudderham
 * Created:     October 28, 2013
 * Last Edited: November 26, 2013
 */

#include <new>

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();
};

int Test()
{
    // Create a Foo on the stack using the placment operator new.
    // Shouldn't compile, since we've deleted that function.
    char buffer[ sizeof(Foo) ];
    Foo* foo = new (&buffer) Foo;

    return 0;
}



