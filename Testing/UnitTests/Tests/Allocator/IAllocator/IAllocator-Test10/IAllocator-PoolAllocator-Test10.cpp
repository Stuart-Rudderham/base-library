/*
 * Confirm that IAllocator-Test10 passes when using the PoolAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#include "Allocator/PoolAllocator.hpp"

#define MAX_CREATED 1000
#define SET_FOO_IALLOCATOR_IMPL() USE_POOL_ALLOCATOR(Foo, MAX_CREATED);

#include "IAllocator-Test10.hpp"
