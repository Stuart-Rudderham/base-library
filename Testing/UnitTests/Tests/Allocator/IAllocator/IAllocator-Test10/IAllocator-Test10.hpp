/*
 * Stress test random allocations and deallocations.
 *
 * Author:      Stuart Rudderham
 * Created:     November 4, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include <cstdlib>
#include <array>

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();

    char space[1024];
};

void SanityCheck( int expectedCur, int expectedMax )
{
    int curAllocated = Foo::GetAllocator().CurAllocated();
    int maxAllocated = Foo::GetAllocator().MaxAllocated();

    dbAssert( curAllocated == expectedCur, "Expected ", expectedCur, " got ", curAllocated );
    dbAssert( maxAllocated == expectedMax, "Expected ", expectedMax, " got ", maxAllocated );
}

int Test()
{
    // Want deterministic results. 10 is an arbitrary value.
    srand(10);

    std::array<Foo*, MAX_CREATED> arr;

    unsigned int numFoo = 0;
    unsigned int maxFoo = 0;

    // Fill the array completely.
    for( unsigned int i = 0; i < MAX_CREATED; ++i ) {
        arr[i] = new ( dbLocation() ) Foo;

        ++numFoo;
        ++maxFoo;
        SanityCheck(numFoo, maxFoo);
    }

    // Clear it completely.
    for( unsigned int i = 0; i < MAX_CREATED; ++i ) {
        delete arr[i];
        arr[i] = nullptr;

        --numFoo;
        SanityCheck(numFoo, maxFoo);
    }

    // Random new and deletes.
    for( unsigned int i = 0; i < 10000; ++i ) {
        int index = rand() % MAX_CREATED;

        if( arr[index] == nullptr ) {
            arr[index] = new ( dbLocation() ) Foo;

            ++numFoo;
            SanityCheck(numFoo, maxFoo);
        }
        else {
            delete arr[index];
            arr[index] = nullptr;

            --numFoo;
            SanityCheck(numFoo, maxFoo);
        }
    }

    // Clean up everything.
    for( unsigned int i = 0; i < MAX_CREATED; ++i ) {
        if( arr[i] != nullptr ) {
            delete arr[i];
            arr[i] = nullptr;

            --numFoo;
            SanityCheck(numFoo, maxFoo);

        }
    }

    dbAssert( numFoo == 0, "Still ", numFoo, " objects left" );

    return 0;
}

