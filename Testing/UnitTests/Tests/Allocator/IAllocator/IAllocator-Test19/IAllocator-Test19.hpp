/*
 * Confirm that inherited operator new works properly if child
 * class is correct size.
 *
 * Author:      Stuart Rudderham
 * Created:     October 30, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Debug/Location.hpp"   // dbLocation
#include "Typename.hpp"
#include "IO/Print.hpp"         // ioCout

struct Base
{
    // This is defined by the test for each specific implementation.
    SET_BASE_IALLOCATOR_IMPL();
    int y;

    virtual ~Base()
    {

    }
};

SetTypenameString( Base );

struct Derived : public Base
{
    virtual ~Derived()
    {
        ioCout( "Derived::~Derived()" );
    }
};

SetTypenameString( Derived );

void SanityCheck( int expectedCur, int expectedMax )
{
    int curAllocated = Base::GetAllocator().CurAllocated();
    int maxAllocated = Base::GetAllocator().MaxAllocated();

    dbAssert( curAllocated == expectedCur, "Expected ", expectedCur, " got ", curAllocated );
    dbAssert( maxAllocated == expectedMax, "Expected ", expectedMax, " got ", maxAllocated );
}

int Test()
{
    // Derived and Base are the same size.
    dbAssert( sizeof(Base) == sizeof(Derived), "Base and Derived aren't the same size!" );

    // Create a Derived in the Base memory pool. This is ok because Derived
    // and Base objects are the same size.
    Base* b = new ( dbLocation() ) Derived;
    SanityCheck(1, 1);

    // Clean up. The Derived destructor should be called.
    delete b;
    SanityCheck(0, 1);

    return 0;
}



