/*
 * Confirm that IAllocator-Test19 passes when using the BasicAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#include "Allocator/BasicAllocator.hpp"

#define SET_BASE_IALLOCATOR_IMPL() USE_BASIC_ALLOCATOR(Base);

#include "IAllocator-Test19.hpp"
