/*
 * Confirm that the IAllocator placement operator new syntax is still
 * valid if the IAllocator is disabled.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Location.hpp"   // dbLocation

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();
};

int Test()
{
    // Create a Foo on the heap using our version of the placment operator new.
    // This syntax should still be valid even though the IAllocator is disabled.
    Foo* foo = new ( dbLocation() ) Foo;
    (void)foo;

    return 0;
}



