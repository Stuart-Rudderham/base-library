/*
 * Confirm that the Create and Destroy wrapper functions work properly.
 *
 * Author:      Stuart Rudderham
 * Created:     November 1, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Debug/Location.hpp"   // dbLocation
#include <string>

struct Bar
{
    int x;
    WRAPPED_OPERATOR_DELETE(Bar);

    virtual ~Bar()
    {

    }
};

struct Foo : public Bar
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();

    WRAPPED_NEW_AND_DELETE(Foo);

    Foo( int )
    {

    }

    Foo( std::string )
    {

    }
};

void SanityCheck( int expectedCur, int expectedMax )
{
    int curAllocated = Foo::GetAllocator().CurAllocated();
    int maxAllocated = Foo::GetAllocator().MaxAllocated();

    dbAssert( curAllocated == expectedCur, "Expected ", expectedCur, " got ", curAllocated );
    dbAssert( maxAllocated == expectedMax, "Expected ", expectedMax, " got ", maxAllocated );
}

int Test()
{
    // Create a bunch of Foo in different ways. These should all succeed.
    Foo f1( 100 );
    SanityCheck(0, 0);

    Foo* f2 = Foo::Create( dbLocation(), 1 );
    SanityCheck(1, 1);

    Foo* f3 = Foo::Create( dbLocation(), "test" );
    SanityCheck(2, 2);

    Foo* f4 = Foo::Create( dbLocation(), std::string("") );
    SanityCheck(3, 3);

    Foo* f5 = Foo::Create( dbLocation(), f1 );
    SanityCheck(4, 4);

    // Cleanup.
    Foo::Destroy( f2 );
    SanityCheck(3, 4);

    Bar::Destroy( f3 );
    SanityCheck(2, 4);

    Bar::Destroy( f4 );
    SanityCheck(1, 4);

    Foo::Destroy( f5 );
    SanityCheck(0, 4);

    return 0;
}



