/*
 * Confirm that IAllocator-Test26 passes when using the PoolAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#include "Allocator/PoolAllocator.hpp"

#define MAX_CREATE 10000
#define SET_FOO_IALLOCATOR_IMPL() USE_POOL_ALLOCATOR(Foo, MAX_CREATE);

#include "IAllocator-Test26.hpp"
