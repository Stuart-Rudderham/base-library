/*
 * Confirm that a class that uses the IAllocator cannot be created using
 * the placement operator new[].
 *
 * Author:      Stuart Rudderham
 * Created:     October 28, 2013
 * Last Edited: November 26, 2013
 */

#include <new>

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();
};

int Test()
{
    // Create an array of Foos on the stack using the placment operator new[].
    // Shouldn't compile, since we've deleted that function.
    char buffer[ 10 * sizeof(Foo) ];
    Foo* foo = new (&buffer) Foo[10];

    return 0;
}



