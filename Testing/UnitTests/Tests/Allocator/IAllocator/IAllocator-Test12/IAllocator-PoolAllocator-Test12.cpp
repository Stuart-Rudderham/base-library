/*
 * Confirm that IAllocator-Test12 passes when using the PoolAllocator
 * as the internal implementation.
 *
 * Author:      Stuart Rudderham
 * Created:     November 26, 2013
 * Last Edited: November 26, 2013
 */

#define ENABLE_FILE_LOCATION_TRACKING 0
#define ENABLE_EXISTENCE_TRACKING 0

#include "Allocator/PoolAllocator.hpp"

#define SET_FOO_IALLOCATOR_IMPL() USE_POOL_ALLOCATOR(Foo, 10);

#include "IAllocator-Test12.hpp"
