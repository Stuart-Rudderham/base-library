/*
 * Confirm that memory leaks are caught and don't display file locations when disabled.
 *
 * Author:      Stuart Rudderham
 * Created:     November 21, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Location.hpp"   // dbLocation

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();
};

int Test()
{
    // Create a bunch of Foos and don't destroy them.
    new ( dbLocation() ) Foo;


    Foo* f = new ( dbLocation() ) Foo;


    new ( dbLocation() ) Foo;
    new ( dbLocation() ) Foo; new ( dbLocation() ) Foo;


    delete f;

    return 0;
}
