/*
 * Confirm a simple new/delete cycle works on a class that uses the IAllocator.
 *
 * Author:      Stuart Rudderham
 * Created:     October 29, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Debug/Location.hpp"   // dbLocation

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();
};

void SanityCheck( int expectedCur, int expectedMax )
{
    int curAllocated = Foo::GetAllocator().CurAllocated();
    int maxAllocated = Foo::GetAllocator().MaxAllocated();

    dbAssert( curAllocated == expectedCur, "Expected ", expectedCur, " got ", curAllocated );
    dbAssert( maxAllocated == expectedMax, "Expected ", expectedMax, " got ", maxAllocated );
}

int Test()
{
    // Create a Foo.
    Foo* foo = new ( dbLocation() ) Foo;
    SanityCheck(1, 1);

    // Then destroy it.
    delete foo;
    SanityCheck(0, 1);

    return 0;
}



