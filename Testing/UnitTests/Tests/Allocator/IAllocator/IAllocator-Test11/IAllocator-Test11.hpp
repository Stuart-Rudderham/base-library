/*
 * Confirm that memory leaks are caught and have the correct file location.
 *
 * Author:      Stuart Rudderham
 * Created:     October 29, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Location.hpp"   // dbLocation

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();
};

int Test()
{
    // Create a bunch of Foos and don't destroy them.
    // Space out the creations to make sure the line numbers work correctly.
    new ( dbLocation() ) Foo;


    Foo* f = new ( dbLocation() ) Foo;


    new ( dbLocation() ) Foo;
    new ( dbLocation() ) Foo; new ( dbLocation() ) Foo;


    delete f;

    return 0;
}
