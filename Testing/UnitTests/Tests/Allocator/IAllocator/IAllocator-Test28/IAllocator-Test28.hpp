/*
 * Confirm that the IAllocator can be disabled.
 *
 * Author:      Stuart Rudderham
 * Created:     November 4, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Location.hpp"   // dbLocation

struct Foo
{
    // This is defined by the test for each specific implementation.
    SET_FOO_IALLOCATOR_IMPL();
};

int Test()
{
    // IAllocator operator new should still be syntactically
    // valid, but not actually use the IAllocator.
    Foo* foo1 = new( dbLocation() ) Foo;

    // Don't cleanup. We shouldn't catch this memory leak.
    (void)foo1;

    return 0;
}

