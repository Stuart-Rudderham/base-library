/*
 * Confirm that creating N + 1 simultaneous objects fails.
 *
 * Author:      Stuart Rudderham
 * Created:     November 4, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Location.hpp"   // dbLocation
#include "Allocator/PoolAllocator.hpp"
#include <array>

struct Foo
{
    USE_POOL_ALLOCATOR(Foo, 10000);
};

int Test()
{
    std::array<Foo*, 10001> arr;

    // Create more than the maximum number of Foos. This should fail.
    for( int i = 0; i < 10001; ++i ) {
        arr[i] = new( dbLocation() ) Foo;
    }

    // Cleanup. Shouldn't reach here.
    for( int i = 0; i < 10001; ++i ) {
        delete arr[i];
    }

    return 0;
}



