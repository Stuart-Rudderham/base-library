/*
 * Confirm that the PoolAllocator can handle not being able to create the memory pool.
 *
 * Author:      Stuart Rudderham
 * Created:     November 4, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Location.hpp"   // dbLocation
#include "Allocator/PoolAllocator.hpp"
#include <cstddef>              // SIZE_MAX

struct Foo
{
    USE_POOL_ALLOCATOR(Foo, 1024);

    // Make sizeof(Foo) really big.
    //     32-bit systems
    //         SIZE_MAX     = 2^32 - 1 (~4GB)
    //         SIZE_MAX / 8 = 2^29 - 1 (~512MB)
    //     64-bit systems
    //         SIZE_MAX     = 2^64 - 1 (~16EB)
    //         SIZE_MAX / 8 = 2^61 - 1 (~2EB)
    //
    // For some reason Clang 3.4 on Linux 64-bit will fail if
    // we have a char array of size > 2^61. I would have though
    // the limit would be SIZE_MAX / 2 = 2^63, but whatever...
    char padding[ SIZE_MAX / 8 ];
};

int Test()
{
    // The PoolAllocator<Foo, 1024> will try and allocate the memory pool
    // upon first use. Since we don't have that much memory it will fail.
    Foo* f = new ( dbLocation() ) Foo;
    (void)f;

    return 0;
}

