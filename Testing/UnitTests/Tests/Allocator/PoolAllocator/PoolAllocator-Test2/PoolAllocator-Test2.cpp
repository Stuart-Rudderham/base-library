/*
 * Confirm that it is possible to create N simultaneous objects.
 *
 * Author:      Stuart Rudderham
 * Created:     November 4, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Debug/Location.hpp"   // dbLocation
#include "Allocator/PoolAllocator.hpp"
#include <array>

struct Foo
{
    USE_POOL_ALLOCATOR(Foo, 10000);
};

void SanityCheck( int expectedCur, int expectedMax )
{
    int curAllocated = Foo::GetAllocator().CurAllocated();
    int maxAllocated = Foo::GetAllocator().MaxAllocated();

    dbAssert( curAllocated == expectedCur, "Expected ", expectedCur, " got ", curAllocated );
    dbAssert( maxAllocated == expectedMax, "Expected ", expectedMax, " got ", maxAllocated );
}

int Test()
{
    std::array<Foo*, 10000> arr;

    // Create the maximum number of Foos. This should be fine.
    for( int i = 0; i < 10000; ++i ) {
        arr[i] = new( dbLocation() ) Foo;
    }

    SanityCheck(10000, 10000);

    // Cleanup.
    for( int i = 0; i < 10000; ++i ) {
        delete arr[i];
    }

    SanityCheck(0, 10000);

    return 0;
}



