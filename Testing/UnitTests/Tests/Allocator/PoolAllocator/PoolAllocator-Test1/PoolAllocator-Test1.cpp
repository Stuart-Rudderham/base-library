/*
 * Confirm that objects created right after each other have adjacent addresses.
 *
 * Author:      Stuart Rudderham
 * Created:     October 29, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"               // dbAssert
#include "Debug/Location.hpp"           // dbLocation
#include "Allocator/PoolAllocator.hpp"
#include <cstddef>
#include <array>

struct Foo
{
    USE_POOL_ALLOCATOR(Foo, 10);
};

void CheckDistance( Foo* foo1, Foo* foo2, std::ptrdiff_t expectedDistance )
{
    std::ptrdiff_t distance = foo2 - foo1;

    dbAssert(
        distance == expectedDistance,
        "Objects are ", distance, " away from each other, should be ", expectedDistance
    );
}

int Test()
{
    std::array<Foo*, 10> arr;

    // Create a bunch of Foos.
    for( int i = 0; i < 10; ++i ) {
        arr[i] = new ( dbLocation() ) Foo;
    }

    // Make sure they are all the expected distance from each other.
    for( int i = 0; i < 10; ++i ) {
        for( int j = i + 1; j < 10; ++j ) {
            CheckDistance( arr[i], arr[j], j - i );
        }
    }

    // Cleanup.
    for( int i = 0; i < 10; ++i ) {
        delete arr[i];
    }

    return 0;
}



