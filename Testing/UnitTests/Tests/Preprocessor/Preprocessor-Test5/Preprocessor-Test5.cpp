/*
 * Confirm that if nothing is specified then we default to Debug mode.
 *
 * Author:      Stuart Rudderham
 * Created:     November 13, 2013
 * Last Edited: May 16, 2014
 */

#if defined( DEBUG ) || defined( RELEASE )
    #error "Debug or Release mode specified!"
#endif

#include "Preprocessor/BuildType.hpp"

// Make sure the macros are defined.
#ifndef DEBUG_MODE
    #error "DEBUG_MODE not defined!"
#endif

#ifndef RELEASE_MODE
    #error "RELEASE_MODE not defined!"
#endif

// Make sure the macros have the correct value.
#if DEBUG_MODE

#else
    #error "DEBUG_MODE has incorrect value!"
#endif

#if RELEASE_MODE
    #error "RELEASE_MODE has incorrect value!"
#else

#endif

int Test()
{
    return 0;
}
