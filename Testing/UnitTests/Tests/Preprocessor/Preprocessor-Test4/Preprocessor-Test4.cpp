/*
 * Confirm that Debug mode and Release mode are mutually exclusive.
 *
 * Author:      Stuart Rudderham
 * Created:     November 13, 2013
 * Last Edited: May 16, 2014
 */

#define DEBUG   1
#define RELEASE 1

#include "Preprocessor/BuildType.hpp"

int Test()
{
    return 0;
}
