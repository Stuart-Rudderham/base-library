/*
 * Confirm that the appropriate compiler macros are defined.
 *
 * Author:      Stuart Rudderham
 * Created:     May 16, 2014
 * Last Edited: May 16, 2014
 */

#include "Preprocessor/CompilerInfo.hpp"

#ifndef USING_MSVC
    #error "USING_MSVC not defined!"
#endif

#ifndef USING_CLANG
    #error "USING_CLANG not defined!"
#endif

#ifndef USING_GCC
    #error "USING_GCC not defined!"
#endif

#if (USING_MSVC + USING_CLANG + USING_GCC) != 1
    #error "More than one compiler define is true!"
#endif

#ifndef COMPILER_NAME
    #error "COMPILER_VERSION not defined!"
#endif

#ifndef COMPILER_VERSION
    #error "COMPILER_VERSION not defined!"
#endif

#ifndef FUNCTION_NAME
    #error "COMPILE_TIME not defined!"
#endif

#ifndef COMPILER_INFO
    #error "COMPILE_TIME not defined!"
#endif

#ifndef COMPILE_TIME
    #error "COMPILE_TIME not defined!"
#endif

int Test()
{
    return 0;
}
