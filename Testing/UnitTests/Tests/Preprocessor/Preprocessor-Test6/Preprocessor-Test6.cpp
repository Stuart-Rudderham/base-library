/*
 * Confirm that the NO_COPY macro works correctly for classes.
 *
 * Author:      Stuart Rudderham
 * Created:     November 13, 2013
 * Last Edited: May 16, 2014
 */

#include "Preprocessor/Misc.hpp"

class Foo
{
    public:
        NO_COPY( Foo );
};

int Test()
{
    Foo x( ( Foo() ) );

    return 0;
}
