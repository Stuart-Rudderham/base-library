/*
 * Confirm that the STRINGIFY macro works correctly.
 *
 * Author:      Stuart Rudderham
 * Created:     May 16, 2014
 * Last Edited: May 20, 2014
 */

#include "Preprocessor/Misc.hpp"
#include "IO/Print.hpp"

#define ONE 1

int Test()
{
    const char* str = STRINGIFY(ONE);
    (void)str;

    return 0;
}
