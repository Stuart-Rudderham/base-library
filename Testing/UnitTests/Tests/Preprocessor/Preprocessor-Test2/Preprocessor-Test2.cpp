/*
 * Confirm that Debug mode is set properly.
 *
 * Author:      Stuart Rudderham
 * Created:     May 16, 2014
 * Last Edited: May 16, 2014
 */

#define DEBUG

#include "Preprocessor/BuildType.hpp"

// Make sure the macros are defined.
#ifndef DEBUG_MODE
    #error "DEBUG_MODE not defined!"
#endif

#ifndef RELEASE_MODE
    #error "RELEASE_MODE not defined!"
#endif

// Make sure the macros have the correct value.
#if DEBUG_MODE

#else
    #error "DEBUG_MODE has incorrect value!"
#endif

#if RELEASE_MODE
    #error "RELEASE_MODE has incorrect value!"
#else

#endif

int Test()
{
    return 0;
}
