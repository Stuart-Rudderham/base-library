/*
 * Confirm that the NO_ASSIGN macro works correctly for classes.
 *
 * Author:      Stuart Rudderham
 * Created:     November 13, 2013
 * Last Edited: May 16, 2014
 */

#include "Preprocessor/Misc.hpp"

class Foo
{
    public:
        NO_ASSIGN( Foo );
};

int Test()
{
    Foo x;
    x = Foo();

    return 0;
}

