/*
 * Confirm that Release mode is set properly.
 *
 * Author:      Stuart Rudderham
 * Created:     May 16, 2014
 * Last Edited: May 16, 2014
 */

#define RELEASE

#include "Preprocessor/BuildType.hpp"

// Make sure the macros are defined.
#ifndef DEBUG_MODE
    #error "DEBUG_MODE not defined!"
#endif

#ifndef RELEASE_MODE
    #error "RELEASE_MODE not defined!"
#endif

// Make sure the macros have the correct value.
#if DEBUG_MODE
    #error "DEBUG_MODE has incorrect value!"
#else

#endif

#if RELEASE_MODE

#else
    #error "RELEASE_MODE has incorrect value!"
#endif

int Test()
{
    return 0;
}
