/*
 * Confirm that Typename<T>() returns the correct string for classes with a
 * single unsigned int template parameter.
 *
 * Author:      Stuart Rudderham
 * Created:     October 10, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Typename.hpp"

// Some macros to reduce copy/paste
#define TestTypename( Type )                            \
    dbAssert(                                           \
        Typename<Type>() == std::string( #Type ),       \
        "TypeName<" #Type ">() was not correct!"        \
    );

template<unsigned int N>
class Foo
{

};

SetTypenameString_TemplateUInt( Foo )

template<typename T>
struct Bar
{

};

SetTypenameString_TemplateT( Bar )

int Test()
{
    TestTypename( Foo<5> );
    TestTypename( Bar<Foo<10>> );

    return 0;
}
