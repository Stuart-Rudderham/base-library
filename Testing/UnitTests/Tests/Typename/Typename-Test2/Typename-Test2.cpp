/*
 * Confirm that Typename<T>() returns the default string when given a type without an associated string.
 *
 * Author:      Stuart Rudderham
 * Created:     October 3, 2013
 * Last Edited: June 8, 2014
 */

#include "Typename.hpp"
#include "IO/Print.hpp"         // ioCout

class Foo
{

};

int Test()
{
    // Foo is defined but doesn't have a string associated with it.
    ioCout( Typename<Foo>() );

    return 0;
}


