/*
 * Confirm that Typename<T>() returns the correct string for builtin types.
 *
 * Author:      Stuart Rudderham
 * Created:     October 3, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Typename.hpp"
#include <string>

#define TestTypename( Type )                                \
    dbAssert(                                               \
        Typename<Type>() == std::string( #Type ),           \
        "TypeName<"#Type">() was not correct!"              \
    );

int Test()
{
    TestTypename( bool );

    TestTypename( char );
    TestTypename( unsigned char );
    TestTypename( signed char );

    TestTypename( short );
    TestTypename( unsigned short );

    TestTypename( int );
    TestTypename( unsigned int );

    TestTypename( long );
    TestTypename( unsigned long );

    TestTypename( long long );
    TestTypename( unsigned long long );

    TestTypename( float );
    TestTypename( double );
    TestTypename( long double );

    TestTypename( wchar_t );

    return 0;
}
