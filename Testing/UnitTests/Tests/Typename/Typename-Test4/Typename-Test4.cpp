/*
 * Confirm that Typename<T>() returns the correct string for enum and enum classes.
 *
 * Author:      Stuart Rudderham
 * Created:     October 10, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Typename.hpp"

// Some macros to reduce copy/paste
#define TestTypename( Type )                            \
    dbAssert(                                           \
        Typename<Type>() == std::string( #Type ),       \
        "TypeName<" #Type ">() was not correct!"        \
    );

enum E1
{
    A, B, C
};

SetTypenameString( E1 );

enum class E2
{
    D, E, F
};

SetTypenameString( E2 );

int Test()
{
    TestTypename( E1 );
    TestTypename( E2 );

    return 0;
}
