/*
 * Confirm that Typename<T>() returns the correct string for classes with
 * a template type parameter and an unsigned int template parameter.
 *
 * Author:      Stuart Rudderham
 * Created:     October 23, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Typename.hpp"

template<typename T, unsigned int N>
class Foo
{

};

SetTypenameString_TemplateTAndUInt( Foo )

template<typename T>
struct Bar
{

};

SetTypenameString_TemplateT( Bar )

int Test()
{
    dbAssert(
        ( Typename<Foo<int, 5>>() == "Foo<int, 5>" ),
        "TypeName<Foo<int, 5>>() was not correct!"
    );

    dbAssert(
        ( Typename<Bar<Foo<Bar<Bar<int>>, 10>>>() == "Bar<Foo<Bar<Bar<int>>, 10>>" ),
        "TypeName<Bar<Foo<Bar<Bar<int>>, 10>>>() was not correct!"
    );

    return 0;
}
