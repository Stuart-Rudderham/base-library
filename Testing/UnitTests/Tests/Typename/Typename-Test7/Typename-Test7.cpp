/*
 * Confirm that Typename<T>() returns the correct string
 * when working with STL containers.
 *
 * Author:      Stuart Rudderham
 * Created:     October 10, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Typename.hpp"

// Some macros to reduce copy/paste
#define TestTypename( Type )                            \
    dbAssert(                                           \
        Typename<Type>() == std::string( #Type ),       \
        "TypeName<" #Type ">() was not correct!"        \
    );

int Test()
{
    TestTypename( std::array<int, 5> );
    TestTypename( std::vector<int> );
    TestTypename( std::deque<int> );

    TestTypename( std::forward_list<int> );
    TestTypename( std::list<int> );

    TestTypename( std::stack<int> );
    TestTypename( std::queue<int> );
    TestTypename( std::priority_queue<int> );

    TestTypename( std::set<int> );
    TestTypename( std::multiset<int> );

    TestTypename( std::map<int, double> );
    TestTypename( std::multimap<int, double> );

    TestTypename( std::unordered_set<int> );
    TestTypename( std::unordered_multiset<int> );

    TestTypename( std::unordered_map<int, double> );
    TestTypename( std::unordered_multimap<int, double> );

    return 0;
}
