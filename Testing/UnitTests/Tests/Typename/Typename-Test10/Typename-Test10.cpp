/*
 * Confirm that Typename<T>() returns the correct string for classes with
 * a template type parameter and a template template type parameter.
 *
 * Author:      Stuart Rudderham
 * Created:     November 20, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Typename.hpp"

template<typename T, template<typename> class U>
class Foo
{

};

SetTypenameString_TemplateTAndTemplateTemplateT( Foo )

template<typename T>
struct Bar
{

};

SetTypenameString_TemplateT( Bar )

int Test()
{
    dbAssert(
        ( Typename<Foo<int, Bar>>() == "Foo<int, Bar<int>>" ),
        "Typename<Foo<int, Bar>>() was not correct!"
    );

    return 0;
}
