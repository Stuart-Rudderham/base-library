/*
 * Confirm that Typename<T>() returns the correct string for user-created classes and structs.
 *
 * Author:      Stuart Rudderham
 * Created:     October 10, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Typename.hpp"

// Some macros to reduce copy/paste
#define TestTypename( Type )                            \
    dbAssert(                                           \
        Typename<Type>() == std::string( #Type ),       \
        "TypeName<" #Type ">() was not correct!"        \
    );

class Foo
{

};

SetTypenameString( Foo );

struct Bar
{

};

SetTypenameString( Bar );

int Test()
{
    TestTypename( Foo );
    TestTypename( Bar );

    return 0;
}
