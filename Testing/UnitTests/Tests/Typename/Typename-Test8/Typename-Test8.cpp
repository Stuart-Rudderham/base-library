/*
 * Typename-Test8 -> Confirm that Typename<T>() returns the correct string
 * when given a derivation (e.g. const, pointer, array, etc..) of a known type.
 *
 * Author:      Stuart Rudderham
 * Created:     October 3, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Typename.hpp"
#include <type_traits>
#include "IO/Print.hpp"         // ioCout

// Some macros to reduce copy/paste
#define TestTypename( Type )                                                   \
    ioCout( #Type" - '", Typename<Type>(), "'" );                              \
    dbAssert(                                                                  \
        Typename<Type>() == std::string( #Type ),                              \
        "TypeName<"#Type">() was not correct!, is ", Typename<Type>()          \
    );

#define TestSameTypes( Type1, Type2 )                                          \
    dbAssert(                                                                  \
        (std::is_same<Type1, Type2>::value),                                   \
        "Types are not the same!"                                              \
    );                                                                         \
                                                                               \
    dbAssert(                                                                  \
        Typename<Type1>() == Typename<Type2>(),                                \
        "TypeName<T>() did not return the same string for both types!"         \
    );

#define TestDerivations(Type)                                                  \
    TestTypename(Type);                                                        \
                                                                               \
    TestTypename( const Type );                                                \
    TestSameTypes( const Type, Type const );                                   \
                                                                               \
    TestTypename( volatile Type );                                             \
    TestSameTypes( volatile Type, Type volatile );                             \
                                                                               \
    TestTypename( const volatile Type );                                       \
    TestSameTypes( const volatile Type, volatile const Type );                 \
                                                                               \
    TestTypename(Type*);                                                       \
    TestTypename(Type**);                                                      \
                                                                               \
    TestTypename(const Type*);                                                 \
    TestSameTypes( const Type*, Type const* );                                 \
                                                                               \
    TestTypename(Type&);                                                       \
    TestTypename(Type&&);                                                      \
                                                                               \
    TestTypename(const Type&);                                                 \
    TestSameTypes( const Type&, Type const& );                                 \
                                                                               \
    TestTypename(const Type&&);                                                \
    TestSameTypes( const Type&&, Type const&& );                               \
                                                                               \
    TestTypename(Type* const);                                                 \
                                                                               \
    TestTypename(const Type* const)                                            \
    TestSameTypes( const Type * const, Type const * const );                   \
                                                                               \
    TestTypename( Type[4] );                                                   \
    TestTypename( Type[1][2][3] );

class Foo
{

};

SetTypenameString( Foo );

struct Bar
{

};

SetTypenameString( Bar );

template<typename T>
class Baz
{

};

SetTypenameString_TemplateT( Baz );

template<unsigned int N>
struct FooBar
{

};

SetTypenameString_TemplateUInt( FooBar );

int Test()
{
    TestDerivations( int );
    TestDerivations( float );

    TestDerivations( Foo );
    TestDerivations( Bar );

    TestDerivations( Baz<int> );
    TestDerivations( FooBar<5> );

    TestDerivations( Baz<FooBar<5>> );

    return 0;
}
