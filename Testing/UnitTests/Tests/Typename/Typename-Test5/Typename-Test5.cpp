/*
 * Confirm that Typename<T>() returns the correct string for classes with a
 * single template type parameter.
 *
 * Author:      Stuart Rudderham
 * Created:     October 10, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Typename.hpp"

// Some macros to reduce copy/paste
#define TestTypename( Type )                            \
    dbAssert(                                           \
        Typename<Type>() == std::string( #Type ),       \
        "TypeName<" #Type ">() was not correct!"        \
    );

template<typename T>
class Foo
{

};

SetTypenameString_TemplateT( Foo )

template<typename T>
struct Bar
{

};

SetTypenameString_TemplateT( Bar )

int Test()
{
    TestTypename( Foo<int> );
    TestTypename( Bar<float> );
    TestTypename( Foo<Bar<Foo<Bar<int>>>> )

    return 0;
}
