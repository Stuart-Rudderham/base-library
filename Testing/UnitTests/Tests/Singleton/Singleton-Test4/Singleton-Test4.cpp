/*
 * Confirm that the Singleton can only be created with Get(), not on the stack.
 *
 * Author:      Stuart Rudderham
 * Created:     October 16, 2013
 * Last Edited: October 16, 2013
 */

#include "Singleton.hpp"

struct Foo : public Singleton<Foo>
{

};

int Test()
{
    // Try and create on stack, should fail at runtime.
    Foo x;
    
    return 0;
}
