/*
 * Confirm that you can't create just a Singleton<T> object
 *
 * Author:      Stuart Rudderham
 * Created:     October 21, 2013
 * Last Edited: October 21, 2013
 */

#include "Singleton.hpp"

struct Foo : public Singleton<Foo>
{

};

int Test()
{
    Singleton<Foo> foo;

    return 0;
}
