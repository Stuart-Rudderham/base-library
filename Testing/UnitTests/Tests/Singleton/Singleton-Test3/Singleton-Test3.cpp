/*
 * Confirm that the Singleton is never created if it isn't used.
 *
 * Author:      Stuart Rudderham
 * Created:     October 11, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Crash.hpp"      // dbCrash
#include "Singleton.hpp"

struct Foo : public Singleton<Foo>
{
    Foo()
    {
        dbCrash( "Singleton constructed!" );
    }
};

int Test()
{
    // Don't access the Singleton, so it shouldn't be constructed.
    
    return 0;
}
