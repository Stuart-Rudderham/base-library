/*
 * Confirm that there can only be one Singleton instance.
 *
 * Author:      Stuart Rudderham
 * Created:     October 16, 2013
 * Last Edited: October 16, 2013
 */

#include "Singleton.hpp"

struct Foo : public Singleton<Foo>
{

};

int Test()
{
    // Create the singleton.
    Foo::Get();

    // Try and create another instance, should fail at runtime.
    Foo x;
    
    return 0;
}
