/*
 * Confirm that you can create a Singleton<Singleton<T>>.
 *
 * Author:      Stuart Rudderham
 * Created:     October 21, 2013
 * Last Edited: October 21, 2013
 */

#include "Singleton.hpp"

struct Foo
{

};

// Specialize the Singleton class to be a Singleton<Singleton<T>>.
//
// Not sure why anyone would do this, but we can't really prevent it
// so it's good to have a test to see what happens.
template<>
class Singleton<Foo> : public Singleton<Singleton<Foo>>
{

};

int Test()
{
    Singleton<Foo>::Get();

    return 0;
}
