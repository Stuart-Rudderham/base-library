/*
 * Confirm that the Singleton is destructed at the end of the program.
 *
 * Author:      Stuart Rudderham
 * Created:     October 11, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Crash.hpp"      // dbCrash
#include "Singleton.hpp"

struct Foo : public Singleton<Foo>
{
    ~Foo()
    {
        dbCrash( "Singleton destructed!" );
    }
};

int Test()
{
    // Create the Singleton. It should be destructed at program end, crashing
    // the program when ~Foo() is called.
    Foo::Get();
    
    return 0;
}
