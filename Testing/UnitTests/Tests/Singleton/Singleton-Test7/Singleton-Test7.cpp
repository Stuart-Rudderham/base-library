/*
 * Confirm that use after destruction is caught.
 *
 * Author:      Stuart Rudderham
 * Created:     October 16, 2013
 * Last Edited: October 19, 2013
 */

#include "Singleton.hpp"

struct Foo : public Singleton<Foo>
{

};

struct Bar
{
    // Don't access Foo in constructor.
    Bar()
    {

    }

    // Do access Foo in destructor.
    ~Bar()
    {
        Foo::Get();
    }
};

// Global Bar is created before main()
static Bar bar;

int Test()
{
    // COnstruct the global Foo. It will be destructed before the global Bar, so
    // ~Bar() will try and access a destructed object. We should catch this.
    Foo::Get();

    return 0;
}
