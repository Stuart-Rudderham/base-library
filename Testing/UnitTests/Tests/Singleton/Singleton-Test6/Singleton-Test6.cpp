/*
 * Confirm that the Singleton can only be created with Get(), not on the heap.
 *
 * Author:      Stuart Rudderham
 * Created:     October 16, 2013
 * Last Edited: May 20, 2014
 */

#include "Singleton.hpp"

struct Foo : public Singleton<Foo>
{

};

int Test()
{
    // Try and create on heap, should fail at runtime.
    Foo* x = new Foo;
    (void)x;

    return 0;
}
