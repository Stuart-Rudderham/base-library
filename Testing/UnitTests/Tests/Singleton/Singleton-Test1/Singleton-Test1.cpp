/*
 * Confirm that inheriting from the Singleton<T> template class creates a global Singleton of type T.
 *
 * Author:      Stuart Rudderham
 * Created:     October 11, 2013
 * Last Edited: June 8, 2014
 */

#include "Debug/Test.hpp"       // dbAssert
#include "Singleton.hpp"
#include <memory>

struct Foo : public Singleton<Foo>
{
    int Bar()
    {
        return 5;
    }
};

int Test()
{
    // Test getting the object with global getter
    Foo& foo = Foo::Get();

    // Make sure is valid object
    dbAssert( Foo::Get().Bar() == 5, "Unable to call function!" );

    // Make sure it returns the same object each time
    for( int i = 0; i < 10; ++i ) {
        dbAssert(
            std::addressof( foo ) == std::addressof( Foo::Get() ),
            "Different object address!"
        );
    }
    
    return 0;
}
