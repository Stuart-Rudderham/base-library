/*
 * Confirm that Singletons are destroyed in a Last-In-First-Out order.
 *
 * Author:      Stuart Rudderham
 * Created:     October 21, 2013
 * Last Edited: November 2, 2013
 */

#include "Singleton.hpp"
#include "Typename.hpp"

struct Foo : public Singleton<Foo>
{

};

SetTypenameString( Foo );

struct Bar : public Singleton<Bar>
{

};

SetTypenameString( Bar );

int Test()
{
    // Bar is created after Foo, so it should be delete before it.
    Foo::Get();
    Bar::Get();

    return 0;
}
