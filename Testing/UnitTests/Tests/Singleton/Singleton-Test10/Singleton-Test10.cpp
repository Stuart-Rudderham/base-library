/*
 * Confirm that you must inherit from the Singleton<T> base class.
 *
 * Author:      Stuart Rudderham
 * Created:     October 21, 2013
 * Last Edited: October 22, 2013
 */

#include "Singleton.hpp"

// A class that doesn't inherit from the Singleton base class.
struct Foo
{

};

int Test()
{
	// Try and get it using Singleton<T>::Get(). This should result
	// in a compile error.
    Singleton<Foo>::Get();

    return 0;
}
