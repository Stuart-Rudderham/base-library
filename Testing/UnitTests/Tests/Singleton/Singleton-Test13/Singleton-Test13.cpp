/*
 * Confirm that two Singletons recursively accessing each other in their constructors is caught.
 *
 * Author:      Stuart Rudderham
 * Created:     October 21, 2013
 * Last Edited: October 22, 2013
 */

#include "Singleton.hpp"

struct Foo : public Singleton<Foo>
{
    Foo();
};

struct Bar : public Singleton<Bar>
{
    Bar();
};

Foo::Foo()
{
    Bar::Get();
}

Bar::Bar()
{
    Foo::Get();
}

int Test()
{
    // Create Foo, which creates Bar, which tries to access Foo (which hasn't
    // finished being created yet). This is undefined behaviour. The GCC runtime
    // on Linux automatically detects this and throws a __gnu_cxx::recursive_init_error
    // exception. On Windows code in the Singleton class detects this.
    Foo::Get();

    return 0;
}
