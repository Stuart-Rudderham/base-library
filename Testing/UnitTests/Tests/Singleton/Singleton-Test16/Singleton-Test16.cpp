/*
 * Confirm can access Singleton in constructor and destructor.
 *
 * Author:      Stuart Rudderham
 * Created:     October 24, 2013
 * Last Edited: October 25, 2013
 */

#include "Singleton.hpp"

struct Foo : public Singleton<Foo>
{

};

struct Bar : public Singleton<Bar>
{
    // Access global Foo singleton in constructor
    Bar()
    {
        Foo::Get();
    }

    // Also access global Foo singleton in destructor. The global Foo
    // should not have been destructed yet so this should be valid.
    ~Bar()
    {
        Foo::Get();
    }
};

int Test()
{
    // Create the global Bar singleton.
    //
    // The order of operations for construction is:
    //     Enter Singleton<Bar>::Singleton<Bar>()
    //     Exit Singleton<Bar>::Singleton<Bar>()
    //     Enter Bar::Bar()
    //         Enter Foo::Get()
    //             Enter Singleton<Foo>::Singleton<Foo>()
    //             Exit Singleton<Foo>::Singleton<Foo>()
    //             Enter Foo::Foo()
    //             Exit Foo::Foo()
    //             <Foo is now constructed>
    //         Exit Foo::Get()
    //         <Access Foo>
    //     Exit Bar::Bar()
    //     <Bar is now constructed>
    //
    // The order of operations for destruction is:
    //     <Bar is now destructed>
    //     Enter Bar::~Bar()
    //         Enter Foo::Get()
    //         Exit Foo::Get()
    //         <Access Foo>
    //     Exit Bar::~Bar()
    //     Enter Singleton<Bar>::~Singleton<Bar>()
    //     Exit Singleton<Bar>::~Singleton<Bar>()
    //     <Foo is now destructed>
    //     Enter Foo::~Foo()
    //     Exit Foo::~Foo()
    //     Enter Singleton<Foo>::~Singleton<Foo>()
    //     Exit Singleton<Foo>::~Singleton<Foo>()
    //
    // This assumes that an object is "constructed" when all the constructors
    // have finished running, and that an object is "destructed" the instant
    // one of the destructors starts running.
    //
    // At no point do we access an object that hasn't been constructed yet nor
    // access an object that has already been destructed, so there should be no
    // isses.
    Bar::Get();

    return 0;
}
