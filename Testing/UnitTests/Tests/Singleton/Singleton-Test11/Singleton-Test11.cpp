/*
 * Confirm that the Singleton is created lazily the first time you access it.
 *
 * Author:      Stuart Rudderham
 * Created:     October 21, 2013
 * Last Edited: May 13, 2014
 */

#include "Singleton.hpp"
#include "Typename.hpp"
#include "IO/Print.hpp"         // ioCout

struct Foo : public Singleton<Foo>
{

};

SetTypenameString( Foo );

int Test()
{
    ioCout( "Before creation" );
    Foo::Get();
    ioCout( "After creation" );

    return 0;
}
