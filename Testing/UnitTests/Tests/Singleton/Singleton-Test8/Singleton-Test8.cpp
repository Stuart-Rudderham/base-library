/*
 * Confirm that you cannot copy-construct a Singleton.
 *
 * Author:      Stuart Rudderham
 * Created:     October 21, 2013
 * Last Edited: October 21, 2013
 */

#include "Singleton.hpp"

struct Foo : public Singleton<Foo>
{

};

int Test()
{
    // Technically this is an impossible situation since you cannot create
    // a Foo without using Foo::Get(), but this will confirm that it produces
    // a compile error rather than a runtime assertion.
    Foo x = Foo::Get();

    return 0;
}
