#ifdef _WIN32
    #include <windows.h>
#endif

int main()
{
    /*
     * When a program terminates with abort() under Windows an annoying dialog
     * box pops up that must be manually closed. Calling the below function will
     * suppress it.
     *
     * References are:
     *     http://msdn.microsoft.com/en-us/library/ms680621
     *     http://www.zachburlingame.com/2011/04/silently-terminate-on-abortunhandled-exception-in-windows/
     */
    #ifdef _WIN32
        SetErrorMode(
            SEM_FAILCRITICALERRORS          |
            SEM_NOALIGNMENTFAULTEXCEPT      |
            SEM_NOGPFAULTERRORBOX           |
            SEM_NOOPENFILEERRORBOX
        );
    #endif

    return Test();
}
