#
# A library of common functions for used for testing.
#
# Author:      Stuart Rudderham
# Created:     November 6, 2013
# Last Edited: November 12, 2013
#

import logging
import os
import subprocess
import sys
import tempfile
import time

#
# Check to make sure a valid Python version is used.
# This gives us a nice error message rather than compiler errors.
#
if sys.version_info[:2] < (3,3):
    sys.exit( "ERROR: Python 3.3 or higher required!" )

#
# A timer class that can be used in a Python "with" statement.
# It uses time.perf_counter(), so it measures wall time.
#
# Idea/code from here -> http://preshing.com/20110924/timing-your-code-using-pythons-with-statement
#
class WallTimer:
    def __enter__(self):
        self.start = time.perf_counter()
        return self

    def __exit__(self, *args):
        self.end = time.perf_counter()
        self.elapsed  = self.end - self.start
        self.elapsed_ms = int(self.elapsed * 1000)

#
# A function to set up the global logger object.
#
def InitializeGlobalLogger( logFileName ):
    """
    Purpose:
        This function initializes a logger to write all messages to both
        stdout and to a file.

    Parameters:
        [string] logFileName
            - the name of the log file to write to.

    Return:
        [Logger]
            - a reference to the logger object that was created/initialized.
    """
    # Get the logger.
    logger = logging.getLogger('global_logger')
    logger.setLevel( logging.DEBUG )

    # Print messages to the log file. We open the log file in 'w'
    # mode so any existing text is erased.
    logFile = logging.FileHandler( logFileName, mode = 'w' )
    logFile.setLevel( logging.DEBUG )

    # Print messages stderr.
    stdout = logging.StreamHandler( sys.stderr )
    stdout.setLevel( logging.DEBUG )

    # We want to print to both.
    logger.addHandler( stdout  )
    logger.addHandler( logFile )

    return logger

#
# A function to run an executable.
#
def RunProcess( commandLine, compileDirectory = None ):
    """
    Purpose:
        This function runs the given command line, recording the output and return code.

    Parameters:
        [list [string]] commandLine
            - the command line to be executed.

        [string] compileDirectory
            - The directory to invoke the command line in.
            - It is an optional argument, if it is not given the command is invoked in the CWD.

    Return:
        [tuple [string] [int] [float]]
            - The output from the command.
            - The return code.
            - How long the command took. Has a value of -1.0 if there was a timeout.
    """
    elapsedTime = None

    # Run the given command line, capturing the output.
    with WallTimer() as timer:
        try:
            output = subprocess.check_output(
                commandLine,
                stderr  = subprocess.STDOUT,
                cwd     = compileDirectory,
                timeout = 20
            )

            returnCode = 0

        except subprocess.CalledProcessError as e:
            output = e.output
            returnCode = e.returncode

        except subprocess.TimeoutExpired as e:
            output = e.output
            returnCode = -1
            elapsedTime = -1.0

    # If the command didn't timeout then record how long it took to complete.
    if elapsedTime is None:
        elapsedTime = timer.elapsed

    # Convert byte-string to text-string
    output = output.decode( sys.stdout.encoding )

    # If there was a timeout add it to the console output so we know it occured.
    if elapsedTime == -1.0:
        output += '\n<TIMEOUT>'

    # Make sure output uses Unix newlines (i.e. '\n').
    output = '\n'.join( output.splitlines() )

    # Return everything.
    return ( output, returnCode, elapsedTime )

#
# Functions to discover what compilers are installed.
#
def GetSanityTestCommandLine( compilerName, fileName, exeName ):
    """
    Purpose:
        This function returns, for the given compiler, the command line that is used when compiling the sanity test.
        If the given compiler is unrecognized then an attempt is made to guess at a suitable command line.

    Parameters:
        [string] compilerName
            - the name of the compiler executable (e.g. g++, cl.exe)

        [string] fileName
            - the name of the file to be compiled

        [string] exeName
            - the name of the resulting executable

    Return:
        [list [string]]
            - the command line, suitable for use with subprocess.call()
    """
    if compilerName == "g++" or compilerName == "clang++":
        return [ compilerName, "-o", exeName, fileName ]

    elif compilerName == "cl.exe":
        return [ compilerName, "/Fe" + exeName, fileName ]

    else:
        return [ compilerName, fileName ]

def RunSanityTest( compiler ):
    """
    Purpose:
        This function attempts to compile and run a very basic C++ program to confirm that
        the given compiler works.

    Parameters:
        [string] compiler
            - the name of the compiler executable (e.g. g++, cl.exe)

    Return:
        [bool]
            - True if the sanity test passed, False if it failed
    """
    # Create the sanity test C++ program in a temporary directory.
    with tempfile.TemporaryDirectory() as testDirectory:
        sourceFile = tempfile.NamedTemporaryFile( mode = 'w', suffix = ".cpp", dir = testDirectory, delete = False )

        sourceFile.file.write( '#include <iostream>                \n' )
        sourceFile.file.write( 'using namespace std;               \n' )
        sourceFile.file.write( 'int main() {                       \n' )
        sourceFile.file.write( '    cout << "Hello World!" << endl;\n' )
        sourceFile.file.write( '    return 0;                      \n' )
        sourceFile.file.write( '}                                  \n' )

        sourceFile.file.close()

        try:
            # Compile the sanity test.
            executableName = "sanity_test.exe"
            absoluteExecutablePath = os.path.join( testDirectory, executableName )

            commandLine = GetSanityTestCommandLine( compiler, sourceFile.name, executableName )
            compileOutput, compileReturnCode, compileTime = RunProcess( commandLine, testDirectory )

            # Check for failures.
            if compileReturnCode != 0:
                return False

            # Run the sanity test.
            programOutput, programReturnCode, programTime = RunProcess( [absoluteExecutablePath], testDirectory )

            # Check for failures.
            if programReturnCode != 0 or programOutput != "Hello World!":
                return False;

            # Everything succeeded so sanity test has been passed.
            return True

        # Return False if anything failed along the way.
        except Exception as e:
            return False

def GetInstalledCompilers():
    """
    Purpose:
        This function attempts to determine what compilers are installed on the system.

    Parameters:
        [void]

    Return:
        [list [string]]
            - a list of the compiler executables are installed.
    """
    # Try compiling a simply file with all the possible compilers. Also try with
    # foo.exe just to make sure the sanity test can survive being passed a nonsense executable.
    compilersTested = ["g++", 'cl.exe', "clang++", "foo.exe"]
    compilersFound  = []

    for compiler in compilersTested:
        if RunSanityTest( compiler ):
            compilersFound.append( compiler )

    return compilersFound

#
# A function to get the Git revision number of the current commit.
#
def GetGitRevisionNumberHEAD():
    # Get the current git revision
    try:
        gitOutput, gitReturnCode, gitTime = RunProcess( ["git", "rev-parse", "--short", "HEAD"] )

        if gitReturnCode == 0:
            return gitOutput
        else:
            return None
    except Exception as e:
        return None
