//  g++ -std=c++11 -O3 -Wall -Wextra -Wundef Main.cpp -I. -DTEST_POOL_ALLOCATOR=1 -DENABLE_ASSERTS=0 -DRELEASE -DENABLE_MEMORY_CLEARING=0

#include "Debug/Debug.hpp"
#include "Allocator/PoolAllocator.hpp"
#include <cstdlib>
#include <array>
#include <string>

const unsigned int FooPoolSize = MEMORY_POOL_SIZE;

unsigned int NumNewCalls    = 0;
unsigned int NumDeleteCalls = 0;

unsigned int Sum = 0;

struct Foo
{
    #if TEST_POOL_ALLOCATOR
        USE_POOL_ALLOCATOR(Foo, FooPoolSize);
    #endif

    Foo( int v )
        : value( v )
    {

    }

    int GetValue() const
    {
        return value;
    }

    int value;
    char space[OBJECT_SIZE];
};

void CreateFoo( Foo*& foo )
{
    ++NumNewCalls;
    int value = NumNewCalls % 1000;

    #if TEST_POOL_ALLOCATOR
        foo = new ( dbLocation() ) Foo( value );
    #else
        foo = new Foo( value );
    #endif

    Sum += foo->GetValue();
}

void DestroyFoo( Foo*& foo )
{
    Sum -= foo->GetValue();
    ++NumDeleteCalls;

    delete foo;
    foo = nullptr;
}

int main( int argc, char** argv )
{
    srand(RNG_SEED);
    const int numIters = std::stoi(argv[1]);
    std::array<Foo*, FooPoolSize> arr;

    // Fill the array completely.
    for( unsigned int i = 0; i < FooPoolSize; ++i ) {
        CreateFoo( arr[i] );
    }

    // Clear it completely.
    for( unsigned int i = 0; i < FooPoolSize; ++i ) {
        DestroyFoo( arr[i] );
    }

    // Random new and deletes.
    for( int i = 0; i < numIters; ++i ) {
        int index = rand() % FooPoolSize;

        if( arr[index] == nullptr ) {
            CreateFoo( arr[index] );
        }
        else {
            DestroyFoo( arr[index] );
        }
    }

    // Clean up everything.
    for( unsigned int i = 0; i < FooPoolSize; ++i ) {
        if( arr[i] != nullptr ) {
            DestroyFoo( arr[i] );
        }
    }

    // Print summary.
    dbPrintToConsole( "Number of iterations - ", numIters );
    dbPrintToConsole( "sizeof(Foo) = ", sizeof(Foo) );
    dbPrintToConsole( "Number of calls to Foo::operator new - ", NumNewCalls );
    dbPrintToConsole( "Number of calls to Foo::operator delete - ", NumDeleteCalls );
    dbPrintToConsole( "Sum = ", Sum );

    return 0;
}
