#
# The driver file for performance and benchmark testing.
#
# Author:      Stuart Rudderham
# Created:     November 6, 2013
# Last Edited: May 12, 2014
#

import datetime                 # now
import itertools                # product
import matplotlib.pyplot as plt
import numpy
import os                       # getcwd, walk, path.join
import random                   # randint
import sys                      # path, exit, argv

# Add the parent directory to the search path so we can include
# TestingCommon. This feels like a gross solution, but is apparently
# the idiomatic way to include a file located in a parent directory.
sys.path.append( os.pardir )
from TestingCommon import *

#
# Global variables.
#
CurrentTimeString = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

RootDir        = os.getcwd();
BaseTestDir    = os.path.join( RootDir, "Tests" )
BaseResultsDir = os.path.join( RootDir, "Results" )
ResultsDir     = os.path.join( BaseResultsDir, CurrentTimeString )

LogFilePath = os.path.join( ResultsDir, "results.log" )
LogFile     = None

PoolAllocatorPerformanceTest = os.path.join( BaseTestDir, "PoolAllocator", "PoolAllocatorPerformanceTest.cpp" )
SourceDir    = os.path.join( RootDir, os.pardir, os.pardir, "Source" )


#
# A class that holds the results of running a single test with
# a single compiler.
#
class PoolAllocatorTestResult:
    def __init__( self ):
       pass

#
#
#
def GenerateGraphs( testsuiteResults ):
    """
    Purpose:
        This function generates graph(s) summarizing the provided tests results.

    Parameters:
        [list [SingleTestResult]] testsuiteResults
            All the performance test results.

    Return:
        [void]
    """
    for testResult in testsuiteResults:
        pass

def SaveGraph( objectSize, memoryPoolSize, results ):
    # Reset everything.
    plt.clf()

    #...
    plt.xlabel('# of iterations')
    plt.ylabel('Time (ms)')

    title = "sizeof(Foo) = {}, memory pool size = {}".format( objectSize, memoryPoolSize )

    plt.title(title)

    # evenly sampled time at 200ms intervals
    #t = numpy.arange(0., 5., 0.2)

    # red dashes, blue squares and green triangles
    #plt.plot(t, t, 'r--', t, t**2, 'bs', t, t**3, 'g^')


    #plt.plot([ 100000, 1000000, 10000000], [1,4,9], 'ro')
    #plt.axis([0, 10000000, 0, 20])

    #          Navy-blue | Sky-blue | Forest-green | green
    colors = ['#000080'  , '#82CAFF', '#4E9258'    , '#00FF00']

    #[list [list [tuple [list float] int]]]

    for colorIndex, singleResult in enumerate(results):
        xVals = []
        yVals = []
        stddevs = []

        for data in singleResult[2]:
            times = data[0]
            numIters = data[1]

            mean = numpy.mean( times )
            stddev = numpy.std( times )

            xVals.append(numIters)
            yVals.append(mean)
            stddevs.append(stddev)

        plt.xscale('log')

        plt.xlim( min(xVals) / 10, max(xVals) * 10 )
        #plt.xticks([1,2,3,4])

        plt.ylim(0, 2000)

        if singleResult[1] == True:
            labelName = "{}, PoolAllocator".format(singleResult[0])
        else:
            labelName = "{}, ::operator new".format(singleResult[0])

        #plt.plot( xVals, yVals, marker='o', linestyle='--', c = colors[colorIndex], label = 'foo' )
        plt.errorbar( xVals, yVals, yerr = stddevs, marker='o', ms=3, linestyle='--', label = labelName )

    plt.legend( loc = 'upper left' )

    # Save it.
    plt.savefig( 'result_{}_{}.png'.format(objectSize, memoryPoolSize) )

#
#
#
def GetPoolAllocatorPerformanceTestCommandLine( compiler, objectSize, memoryPoolSize, testPoolAllocator, rngSeed, exeName ):
    """
    Purpose:
        This function returns, for the given compiler, the command line that is used for the performance tests.
        If the given compiler is unrecognized then an attempt is made to guess at a suitable command line.

    Parameters:
        [string] compiler
            The name of the compiler executable (e.g. g++, cl.exe)

        ***                           ***
        *** FILL IN THE REST OF THESE ***
        ***                           ***

    Return:
        [list [string]]
            The command line, suitable for use with subprocess.call()
    """
    if compiler == "g++" or compiler == "clang++":
        return [
                 compiler,                      # Compiler
                 PoolAllocatorPerformanceTest,  # File with main()
                 "-Wall",                       # All warnings
                 "-Wextra",
                 "-Wundef",
                 "-O3",                         # Max optimization
                 "-std=c++11",                  # Latest C++ standard
                 "-DRELEASE",                   # Release mode
                 "-DENABLE_BREAKPOINTS=0",      # Not using a debugger, so disable breakpoints
                 "-DENABLE_ASSERTS=0",          # Don't want asserts
                 "-DENABLE_MEMORY_CLEARING=0",  # Don't want to clear memory upon object deletion
                 "-DENABLE_POOL_ALLOCATOR=1",
                 "-DTEST_POOL_ALLOCATOR=" + str(int(testPoolAllocator)),
                 "-DOBJECT_SIZE=" + str(objectSize),
                 "-DMEMORY_POOL_SIZE=" + str(memoryPoolSize),
                 "-DRNG_SEED=" + rngSeed,
                 "-I", SourceDir,               # Add the Source directory to include path
                 "-o", exeName,                 # Set the executable name
               ]

    elif compiler == "cl.exe":
        return [
                 compiler,                      # Compiler
                 PoolAllocatorPerformanceTest,  # File with main()
                 "/W4",                         # All warnings
                 "/O2",                         # Max optimization
                 "/DRELEASE",                   # Release mode
                 "/DENABLE_BREAKPOINTS=0",      # Not using a debugger, so disable breakpoints
                 "/DENABLE_ASSERTS=0",          # Don't want asserts
                 "/DENABLE_MEMORY_CLEARING=0",  # Don't want to clear memory upon object deletion
                 "/DENABLE_POOL_ALLOCATOR=1",
                 "/DTEST_POOL_ALLOCATOR=" + str(int(testPoolAllocator)),
                 "/DOBJECT_SIZE=" + str(objectSize),
                 "/DMEMORY_POOL_SIZE=" + str(memoryPoolSize),
                 "/DRNG_SEED=" + rngSeed,
                 "/I" + SourceDir,               # Add the Source directory to include path
                 "/Fe" + exeName,                 # Set the executable name
               ]

    else:
        return [ compiler, fileName ]

#
#
#
def RunSingleParameterCombinationTest( compilers, objectSize, memoryPoolSize ):
    pass

#
#
#
def RunPoolAllocatorPerformanceTest( compilers ):
    # Build up a list of all the possible combinations of the different parameters

    possibleMemoryPoolSizes    = [ 1, 10, 100, 1000, 10000 ]
    possibleObjectSizes        = [ 1, 10, 100, 1000, 10000 ]
    possibleNumberOfIterations = [ 1000, 10000, 100000, 1000000, 10000000 ]
    #possibleNumberOfIterations = [ 1, 10, 100 ]
    testPoolAllocator          = [ 0, 1 ]
    """
    possibleMemoryPoolSizes    = [ 1000 ]
    possibleObjectSizes        = [ 1000 ]
    possibleNumberOfIterations = [ 10000, 10 ]
    testPoolAllocator          = [ 0, 1 ]
"""

    iterationNum = 0
    rngSeed = str( random.randint(1, 100) )

    exeName = "foo.exe"

    testsuiteResults = []


    # For all combinations of the test parameters...
    for combinationIndex, (objectSize, memoryPoolSize) in enumerate( itertools.product(possibleObjectSizes, possibleMemoryPoolSizes) ):
        LogFile.info( "Parameter combination #{}".format(combinationIndex) )
        LogFile.info( "    Object size = {} bytes".format(objectSize) )
        LogFile.info( "    Memory pool size = {} objects ({} bytes)".format(memoryPoolSize, memoryPoolSize * objectSize) )

        testResult =
        for compiler in compilers:
            for testPoolAllocator in (False, True):
                # Create a temp directory to test in.
                testDirectory = os.path.join( ResultsDir, "{}_{}_{}_{}".format(compiler, objectSize, memoryPoolSize, testPoolAllocator) )
                os.makedirs( testDirectory )

                # Compile the executable with the current parameters.
                compileCommand = GetPoolAllocatorPerformanceTestCommandLine( compiler, objectSize, memoryPoolSize, testPoolAllocator, rngSeed, exeName )

                compileOutput, compileReturnCode, compileTime = RunProcess( compileCommand, testDirectory )

                with open( os.path.join(testDirectory, "compile_output.txt"), "w") as logFile:
                    logFile.write( " ".join(compileCommand) + "\n" )
                    logFile.write( "-------------------\n" )
                    logFile.write( compileOutput )

                if compileReturnCode != 0:
                    LogFile.info( "Compilation failed for {}".format( compileCommand, compileReturnCode ) )
                    continue

                LogFile.info( "Parameter combination #{}".format(iterationNum) )
                LogFile.info( "    Compiler - {}".format(compiler) )
                LogFile.info( "    sizeof(Foo) - {}".format(objectSize) )
                LogFile.info( "    Memory pool size - {}".format(memoryPoolSize) )
                LogFile.info( "    Using PoolAllocator - {!s}".format(testPoolAllocator) )

                # Test its performance.
                singleResults = []

                for numIterations in possibleNumberOfIterations:
                    programCommand = [os.path.join(testDirectory, exeName), str(numIterations)]
                    results = RunGenericPerformanceTest( programCommand, 1 )

                    # Record results.
                    LogFile.info( "        Number of iterations - {}".format(numIterations) )
                    LogFile.info( "        Results - {!s}".format(results) )
                    LogFile.info( "" )

                    singleResults.append( (results, numIterations) )

                testsuiteResults.append( (compiler, testPoolAllocator, singleResults) )

        SaveGraph( objectSize, memoryPoolSize, testsuiteResults )

#
# Function to run performance test on a generic executable
#
def RunGenericPerformanceTest( commandLine, numIterations ):
    """
    Purpose:
        This runs the given command the given number of times, recording how long
        each invocation took to complete.

    Parameters:
        [list [string]] commandLine
            All the performance test results.

        [int] numIterations
            How many times to run the program.

    Return:
        [list [float]]
            A list of times (in milliseconds), one for each program invocation.
    """
    # Run the executable once, to get it loaded into memory.
    RunProcess( commandLine )

    # Run the executable the desired number of times, recording
    # how long each invocation takes.
    times = []

    for i in range( numIterations ):
        with WallTimer() as timer:
           RunProcess( commandLine )

        times.append( timer.elapsed_ms )

    # Sort from fastest to slowest.
    times.sort()

    # Remove the best and worst time (if possible)
    #if len(times) >= 3:
    #    times = times[1:-1]

    return times

#
# The main function.
#
def main():
    # Create the folders where all the output files will be placed.
    try:
        os.makedirs( BaseResultsDir )
    except:
        pass

    if os.path.exists( ResultsDir ):
        sys.exit( "ERROR: {} already exists! Maybe increase timestamp precision?".format(ResultsDir) )

    os.makedirs( ResultsDir )

    # Open the global log file handle for writing.
    # We haven't done anything worth logging before this point, so it doesn't matter
    # if we missed some output.
    global LogFile
    LogFile = InitializeGlobalLogger( LogFilePath )

    # Make sure all the expected paths can be found
    for path in [BaseTestDir, ResultsDir, LogFilePath, PoolAllocatorPerformanceTest, SourceDir]:
        if os.path.exists( path ) == False:
            sys.exit( "ERROR: {} does not exist!".format(path) )

    # Discover what compilers are installed.
    compilersFound = GetInstalledCompilers()

    if compilersFound:
        LogFile.info( "Compilers found: {!s}".format(compilersFound) )
        LogFile.info( "" )
    else:
        LogFile.info( "No compilers passed the sanity test!" )
        return

    # Get the current git revision
    gitRevisionHEAD = GetGitRevisionNumberHEAD()

    if gitRevisionHEAD:
        LogFile.info( "Using commit {}".format(gitRevisionHEAD) )
    else:
        LogFile.info( "Unable to get HEAD revision number" )

    LogFile.info( "" )

    # Run the performance test with all the installed compilers.
    testsuiteResults = RunPoolAllocatorPerformanceTest( compilersFound )

    # Create the graphs summarizing the test results.
    #GenerateGraphs( testsuiteResults )

#
# Call the main() function, timing how long it takes.
#
if __name__ == "__main__":
    with WallTimer() as timer:
        main()

    LogFile.info( "Took {:.2f} seconds.".format(timer.elapsed) )
