About
    This library is a collection of various bits of useful code I've written
    over the years. It has no outside dependencies, but most of the modules use
    code from other modules and thus cannot be used independently (e.g. almost
    all the code uses functions from the Debug module).

    It makes heavy use of C++11 and therefore will not compile with non-modern
    compilers. It is know to compile with:

        GCC 4.8.1
        Clang 3.4
        MSVC 2012 (with CPT)

    The code is platform-independent and should compile on any operating system
    that has a supporting compiler. It has been tested only on Windows and
    Linux, however.

    Compiling the library is fairly simple, there are no special macros or
    compiler flags that are needed. All that is required is to add the Source
    directory to the compiler search path (or copy the code into your source
    directory directly).

    A general Debug or Release mode can be defined by defining either a DEBUG
    or RELEASE macro when compiling. This will enable/disable certain features
    (e.g. asserts)

    These features can also be manually set by defining the relevant macro to
    either 0 or 1 on the command line. These macros are:

        ENABLE_ASSERTS
        ENABLE_WARNINGS
        ENABLE_BREAKPOINTS
        ENABLE_CONSOLE_PRINTING
        ENABLE_CHECKED_CAST

Dependencies
    Building library
        Modern C++ compiler (GCC 4.8+, Clang 3.4+, MSVC 2013+)
        CMake
            http://www.cmake.org/cmake/resources/software.html

    Building Docs
        Doxygen
            http://www.stack.nl/~dimitri/doxygen/download.html

        Graphviz
            http://www.graphviz.org/Download.php

    Running tests
        Python 3.3+
            http://www.python.org/download/

        Performance tests
            Matplotlib
                http://www.lfd.uci.edu/~gohlke/pythonlibs/#matplotlib
                http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy
                http://www.lfd.uci.edu/~gohlke/pythonlibs/#python-dateutil
                http://www.lfd.uci.edu/~gohlke/pythonlibs/#pytz
                http://www.lfd.uci.edu/~gohlke/pythonlibs/#pyparsing
                http://www.lfd.uci.edu/~gohlke/pythonlibs/#six




Testing
    The library comes with a home-grown test harness, stored in the Testsuite
    directory. It requires Python to be installed and added to the PATH.

    The testsuite can be run by invoking the relevant script based on your
    operating system. The results of running the testsuite are stored in
    the Testsuite/Results folder.

    The tests themselves are stored in the Testsuite/Tests directory. New tests
    can be added simply by creating a folder with the test file in it. The test
    file must have a .cpp extension and define a function "int Test()" that
    returns 0 if the test passed and 1 if it failed. Each test directory must
    also have a file that denotes the expected outcome of running the test. This
    file should have no content and should have one of the below names:

        CompileFail
            If the test is expected to not compile

        CompilePass
            If the test is expected to compile. The resulting executable is not
            run.

        TestFail
            If the test is expected to compile and running the executable has a
            non-zero exit code.

        TestPass
            If the test is expected to compile and running the executable has an
            exit code of zero.

    It is also possible to make a test result depend on the output of the
    program. If a file named "MatchingRegex" exists, the contents of this file
    will be used as a regex against the output of the program. If it doesn't
    match anything then the test fails. A similar approach can be used with a
    file named "NonMatchingRegex" to fail the test if certain text is outputted
    by the program. See existing tests for some examples.

Modules
    Debug
        The Debug functions are wrapped in macros to get line numbers. This can cause issues if your arguments have
        commas that aren't wrapped in (), [], or {}, for example dbAssert( Add<1, 2> == 3, "" ); The fix is to just wrap the
        condition in parens, e.g. dbAssert( (Add<1, 2> == 3), "" );

    Macros

    Memory

    Factory
        A class for creating, managing, and deleting objects of a single type. It can be used with an arbitrary type

    Allocator
        One thing to remember is that operator new can be inherited. So if you have a base class use the PoolAllocator
        any child classes will also be allocated from that same memory pool unless you add USE_POOL_ALLOCATOR into their
        class declaration.

        Cannot catch double delete or deleting things not allocated by the PoolAllocator if tracking is turned off. But much faster.

    Singleton
        One thing to remember about inheritance chains with Singleton classes is that the Singleton<T>::Get() function
        returns a reference of type T. So in the example below...

            struct Foo : public Singleton<Foo>
            {

            };

            struct Bar : public Foo
            {
                void F() {}
            };

            Bar::Get().F();

        Bar::Get() actually returns a Foo&, so trying to call F() on it will be a compile error.


    Allocator and Factory have some overlapping functionallity, although they measure different things (Factory deals with objects, Allocator deals with addresses)
        Both can be used to check for memory leaks
        Both can be used to see how many object of a given type currently exist
            Factories allow you to operate on all existing objects (that were created by the factory), Allocators don't.

        Allocator does these slightly better, as it is possible to create objects without using the factory, but you cannot create the objects without using the allocator (assuming they are being created on the heap)

        Right now they are both Singletons, but could be modified so that they take a numeric template parameter so you can create multiple ones. Example

            ArenaAllocator<Foo, 1> is used by objects created by Factory<Foo, 1>
            ArenaAllocator<Foo, 2> is used by objects created by Factory<Foo, 2>
            ...

        An issue with Allocators is that they require access to the code for whatever class you want to use them with. For example, you cannot use an Allocator<int> as "int" is a builtin type that the "new operator" cannot be overloaded for.

        This is not an issue with the Factory class, it can be used with any arbitrary types.

        In the event of a memory leak, the Factory class is able to tell you what file and line the leaked object(s) were created at. The Allocator class is not that smart, and can only tell you whether or not memory was leaked. This is because passing the file and line info to "operator new" is much more of a pain then passing it to the Factory::Create() function

        Allocators and Factories aren't threadsafe.
