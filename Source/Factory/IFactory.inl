/*
 * This file implements a generic factory class that is used to
 * create, manage, and delete objects of a given type hierarchy.
 *
 *
 * The actual implementation is a template parameter so it is possible
 * to "plug-and-play" with different implementations so long as they respect
 * the requirements of the Factory.
 *
 * Author:      Stuart Rudderham
 * Created:     August 5, 2012
 * Last Edited: June 3, 2014
 */

#include "IO/Print.hpp" // ioPrintln, ioCout
#include "Debug/Debug.hpp"
#include "Typename.hpp"
#include <utility>      // std::forward
#include <algorithm>    // std::max
#include <sstream>      // std::ostringstream
#include <type_traits>  // std::is_base_of

/*
 * A type_traits struct that uses SFINAE to detect if a type is being used
 * with the IAllocator class.
 *
 * From here -> http://stackoverflow.com/a/257382
 */
template <typename T>
class UsesIAllocator
{
    private:
        struct No  { char padding[2]; };
        struct Yes { char padding[1]; };

        template<typename U> static Yes test( decltype( new ( L3::Debug::Location{} ) U() ) );
        template<typename U> static No  test(...);

    public:
        enum { value = ( sizeof( test<T>(0) ) == sizeof(Yes) ) };
};

/*
 * Two different creation functions. The correct one is automatically used
 * based on whether or not the type T is being used with the IAllocator class.
 */
template<typename T, typename... Args>
typename std::enable_if<UsesIAllocator<T>::value == true, T*>::type
CreateImpl( const L3::Debug::Location& loc, Args&&... args )
{
    return new ( loc ) T( std::forward<Args>(args)... );
}

template<typename T, typename... Args>
typename std::enable_if<UsesIAllocator<T>::value == false, T*>::type
CreateImpl( const L3::Debug::Location&, Args&&... args )
{
    return new T( std::forward<Args>(args)... );
}

/*
 * Constructor.
 */
template<typename T, template <typename> class Impl>
IFactory<T, Impl>::Factory()
{
    // TODO: Add static_asserts checking that the implementation has the member
    // functions we will use. This will give a nice error message rather than
    // pages of template errors.
    //
    // Functions are:
    //     void AddObject( T* );
    //     void ReturnObject( T* );
    //
    //     unsigned int BatchDestroy( bool (*Pred)(T*), Debug::LocationMap<T>*, Debug::ExistenceSet<T>* );
    //     unsigned int DestroyAll();
    //
    //     unsigned int MaxAllocated() const;
    //     unsigned int CurAllocated() const;
    //
    //     std::string GetStatusString() const;
}

/*
 * Destructor.
 */
template<typename T, template <typename> class Impl>
IFactory<T, Impl>::~Factory()
{
    // Print out a summary.
    ioCout( "\n", GetStatusString() );

    // Make sure we don't have a memory leak.
    if( CurCreated() != 0 ) {
        dbCrash(  "Leaking memory in ", Typename<IFactory<T, Impl>>() );
    }
}

/*
 * Create a new object.
 */
template<typename T, template <typename> class Impl>
template<typename U, typename... Args>
U* IFactory<T, Impl>::Create( const L3::Debug::Location& loc, Args&&... args )
{
    // Make sure the object we are creating is a part of the object hierarchy.
    // This provided a nice error message rather than cryptic errors about
    // not being able to convert U* to T*.
    static_assert(
        std::is_base_of<T, U>::value == true,
        "Trying to create object that's not in the hierarchy!"
    );

    // Create the object and add it to the implementation's data structures.
    U* object = CreateImpl<U>( loc, std::forward<Args>(args)... );
    impl.AddObject( object );

    // Sanity check. This will assert if the object already exists.
    #if ENABLE_EXISTENCE_TRACKING
        createdObjects.Add( object );
    #endif

    // Store the file location where the object was created.
    #if ENABLE_FILE_LOCATION_TRACKING
        creationFileLocations.Add( object, loc );
    #endif

    return object;
}

/*
 * Destroy an object created by the Factory.
 */
template<typename T, template <typename> class Impl>
void IFactory<T, Impl>::Destroy( T* object )
{
    // We shouldn't ever delete a nullptr. There's no reason why we can't
    // just ignore it but I'd rather be alerted if it ever happens, as it
    // could indicate a bug.
    dbAssert(
        object != nullptr,
        "Given a nullptr to delete in ", Typename<IFactory<T, Impl>>()
    );

    // Sanity check. This will assert if the object wasn't created by this Factory.
    #if ENABLE_EXISTENCE_TRACKING
        createdObjects.Remove( object );
    #endif

    // Delete the object and remove it from the implementation's data structures.
    delete object;
    impl.RemoveObject( object );

    // Discard the file location since the object no longer exists.
    #if ENABLE_FILE_LOCATION_TRACKING
        creationFileLocations.Remove( object );
    #endif
}

/*
 * Destroy all objects for which the given predicate function returns true. Returns
 * the number of objects destroyed.
 */
template<typename T, template <typename> class Impl>
unsigned int IFactory<T, Impl>::BatchDestroy( bool (*Pred)(T*) )
{
    // The debugging data structures do not even exist in Release mode
    // so we have to be flexible. THe implementation should be able
    // to handle these pointers being NULL.
    Debug::ExistenceSet<T>* existenceSet = nullptr;
    Debug::LocationMap<T>* locationMap = nullptr;

    #if ENABLE_EXISTENCE_TRACKING
        existenceSet = &createdObjects;
    #endif

    #if ENABLE_FILE_LOCATION_TRACKING
        locationMap = &creationFileLocations;
    #endif

    // Delegate to the implementation.
    return impl.BatchDestroy( Pred, existenceSet, locationMap );
}

/*
 * Destroy all objects created by this Factory. Returns the number of objects destroyed.
 * Hope you don't leave any dangling pointers...
 */
template<typename T, template <typename> class Impl>
unsigned int IFactory<T, Impl>::DestroyAll()
{
    // Update debugging data structures if necessary.
    #if ENABLE_EXISTENCE_TRACKING
        createdObjects.Clear();
    #endif

    #if ENABLE_FILE_LOCATION_TRACKING
        creationFileLocations.Clear();
    #endif

    // Delegate to the implementation.
    return impl.DestroyAll();
}

/*
 * Returns the highest number of simultaneously created objects (the high-water
 * mark).
 */
template<typename T, template <typename> class Impl>
unsigned int IFactory<T, Impl>::MaxCreated() const
{
    // Delegate to the implementation.
    return impl.MaxCreated();
}

/*
 * Returns the current number of created objects.
 */
template<typename T, template <typename> class Impl>
unsigned int IFactory<T, Impl>::CurCreated() const
{
    // Delegate to the implementation.
    return impl.CurCreated();
}

/*
 * Get a summary of the current status of the Factory.
 */
template<typename T, template <typename> class Impl>
std::string IFactory<T, Impl>::GetStatusString() const
{
    std::ostringstream stream;

    // Print out a summary.
    //
    // We can't display object size or memory used because we are creating
    // polymorphic objects and all we know is the base class size, so it's
    // not as simple as using sizeof(T).
    ioPrintln( stream, Typename<IFactory<T, Impl>>(), " Summary"             );
    ioPrintln( stream,  "    Max Number Of Active Objects : ", MaxCreated() );
    ioPrintln( stream,  "    Currently Created Objects    : ", CurCreated() );

    #if ENABLE_FILE_LOCATION_TRACKING
        auto v = creationFileLocations.GetContents();
        for( const auto& i : v ) {
            ioPrintln( stream, ""                              );
            ioPrintln( stream, "        Address  : ", i.first  );
            ioPrintln( stream, "        Location : ", i.second );
        }
    #endif

    return stream.str();
}

