/*
 * This header defines a generic factory class implementation that
 * uses a hash map internally to handle objects. As a result it can
 * delete an arbitrary object in O(1) time.
 *
 * Author:      Stuart Rudderham
 * Created:     November 12, 2012
 * Last Edited: November 17, 2013
 */

#ifndef FACTORYSETIMPL_HPP_INCLUDED
#define FACTORYSETIMPL_HPP_INCLUDED

#include "Debug/Debug.hpp"
#include "Typename.hpp"
#include <unordered_set>

template<typename T>
class FactorySetImpl
{
    public:
        FactorySetImpl();                                   // Constructor.

        ~FactorySetImpl();                                  // Destructor. Checks to make sure there are no created objects left.

        void AddObject( T* object );

        void RemoveObject( T* object );

        bool Created( T* object ) const;                    // Return true if the object was created by this Factory.

        unsigned int BatchDestroy( bool (*Pred)(T*) );      // Destroy all objects for which the given predicate function
                                                            // returns true. Returns the number of objects destroyed.

        unsigned int DestroyAll();                          // Destroy all currently created objects.

        unsigned int MaxCreated() const;                    // Returns the highest number of objects that existed at the same time.

        unsigned int CurCreated() const;                    // Returns the current number of created objects.

    private:
        std::unordered_set<T*> createdObjects;              // A table of the objects that have been created but not destroyed yet.
};

/*
 * Set the typename string.
 */
SetTypenameString_TemplateT( FactorySetImpl );

/*
 * Pull in the implementation file as well, so we don't get linker errors when
 * only including the header file.
 */
#include "Factory/FactorySetImpl.inl"

#endif // FACTORYSETIMPL_HPP_INCLUDED
