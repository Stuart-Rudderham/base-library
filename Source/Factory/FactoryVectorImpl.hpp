/*
 * This header defines a generic factory class that is used to
 * create, manage, and delete objects of a given type hierarchy.
 *
 * Author:      Stuart Rudderham
 * Created:     November 12, 2013
 * Last Edited: June 3, 2014
 */

#ifndef FACTORYVECTORIMPL_HPP_INCLUDED
#define FACTORYVECTORIMPL_HPP_INCLUDED

#include "Singleton.hpp"
#include "Debug/Debug.hpp"
#include "Typename.hpp"
#include <vector>

template<typename T>
class FactoryVectorImpl
{
    public:
        FactoryVectorImpl();                                                                      // Constructor.

        ~FactoryVectorImpl();                                                                     // Destructor. Checks to make sure there are no created objects left.

        template<typename U, typename... Args>                                                      // Create a new an object, recording the location in the code where
        U* Create( const L3::Debug::Location& loc, Args&&... args );                               // it was created.

        void Destroy( T* object );                                                      // Destructs the given object.

        bool Created( T* object ) const;                                                // Return true if the object was created by this Factory.

        unsigned int BatchDestroy( bool (*Pred)(T*) );    // Destroy all objects for which the given predicate function
                                                                                        // returns true. Returns the number of objects destroyed.

        unsigned int DestroyAll();                                               // Destroy all currently created objects.

        unsigned int CurCreated() const;                                                // Returns the current number of created objects.

    private:
        typedef std::vector<T*> ObjectList;
        ObjectList createdObjects;                                                      // A table of the objects that have been created but not destroyed yet.
                                                                                        // Associated with each object is the location in the code where the
                                                                                        // object was created. This is useful for debugging memory leaks.
};

/*
 * Set the typename string.
 */
SetTypenameString_TemplateT( FactoryVectorImpl );

/*
 * Pull in the implementation file as well, so we don't get linker errors when
 * only including the header file.
 */
#include "Factory/FactoryVectorImpl.inl"

#endif // FACTORYVECTORIMPL_HPP_INCLUDED
