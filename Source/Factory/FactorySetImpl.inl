/*
 * This file implements a generic factory class that is used to
 * create, manage, and delete objects of a given type hierarchy.
 *
 * Author:      Stuart Rudderham
 * Created:     November 12, 2012
 * Last Edited: June 3, 2014
 */

#include "Debug/Debug.hpp"
#include "Typename.hpp"
#include <algorithm>
#include <cstdlib>
#include <utility>      // std::forward
#include <algorithm>    // std::max

/*
 * Constructor.
 */
template<typename T>
FactorySetImpl<T>::FactorySetImpl()
{

}

/*
 * Destructor.
 */
template<typename T>
FactorySetImpl<T>::~FactorySetImpl()
{

}

/*
 * Create a new object.
 */
template<typename T>
template<typename U, typename... Args>
U* FactorySetImpl<T>::Create( const L3::Debug::Location& loc, Args&&... args )
{
    // Make sure the object we are creating is a part of the object hierarchy.
    // This provided a nice error message rather than cryptic errors about
    // not being able to convert U* to T*.
    static_assert(
        std::is_base_of<T, U>::value == true,
        "Trying to create object that's not in the hierarchy!"
    );

    // Create the object.
    U* newObject = new U( std::forward<Args>(args)... );

    // Sanity check.
    dbAssert(
        Created( newObject ) == false,
        "Given object already created by ", Typename<FactorySetImpl<T>>()
    );

    // Add it to the set of created objects.
    createdObjects.insert( newObject );

    // Sanity check.
    dbAssert(
        Created( newObject ) == true,
        "Given object wasn't created by ", Typename<FactorySetImpl<T>>()
    );

    return newObject;
}

/*
 * Destroy an object created by the Factory.
 */
template<typename T>
void FactorySetImpl<T>::Destroy( T* object )
{
    // Delete the object and remove it from the set of created objects.
    delete object;
    createdObjects.erase( object );
}

/*
 * Destroy all objects for which the given predicate function returns true. Returns
 * the number of objects destroyed.
 */
template<typename T>
unsigned int FactorySetImpl<T>::BatchDestroy( bool (*Pred)(T*) )
{
    unsigned int oldSize = CurCreated();

    // Iterate over all created objects, destroying them if the predicate returns true.
    for( auto i = createdObjects.begin(); i != createdObjects.end(); ) {
        if( Pred( *i ) == true ) {
            delete *i;
            //createdObjectLocations.Remove( *i );
            i = createdObjects.erase( i );
        } else {
            ++i;
        }
    }

    // Return how many objects were destroyed
    return oldSize - CurCreated();
}

/*
 * Destroy all objects created by this Factory. Returns the number of objects destroyed.
 * Hope you don't leave any dangling pointers...
 */
template<typename T>
unsigned int FactorySetImpl<T>::DestroyAll()
{
    // Store the old size so we know how many objects we deleted.
    unsigned int numObjectsDestroyed = CurCreated();

    // Delete all the objects and clear the table.
    for( const auto& i : createdObjects ) {
        delete i;
    }

    createdObjects.clear();
    //createdObjectLocations.Clear();

    return numObjectsDestroyed;
}

/*
 * Returns the current number of created objects.
 */
template<typename T>
unsigned int FactorySetImpl<T>::CurCreated() const
{
    return createdObjects.size();
}
