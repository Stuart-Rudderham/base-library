/*
 * This header defines a generic factory class that is used to
 * create, manage, and delete objects of a given type hierarchy.
 *
 * The actual implementation is a template parameter so it is possible
 * to "plug-and-play" with different implementations so long as they respect
 * the requirements of the Factory.
 *
 * Author:      Stuart Rudderham
 * Created:     August 5, 2012
 * Last Edited: June 3, 2014
 */

#ifndef IFACTORY_HPP_INCLUDED
#define IFACTORY_HPP_INCLUDED

#include "Debug/Debug.hpp"
#include "Debug/LocationMap.hpp"
#include "Debug/ExistenceSet.hpp"
#include "Typename.hpp"
#include <string>

template<typename T, template <typename> class Impl>
class IFactory
{
    public:
        IFactory();                                         // Constructor.

        ~IFactory();                                        // Destructor. Checks to make sure there are no created objects left.

        template<typename U, typename... Args>              // Create a new an object, recording the location in the code where
        U* Create( const L3::Debug::Location& loc, Args&&... args );   // it was created.

        void Destroy( T* object );                          // Destructs the given object.

        unsigned int BatchDestroy( bool (*Pred)(T*) );      // Destroy all objects for which the given predicate function
                                                            // returns true. Returns the number of objects destroyed.

        unsigned int DestroyAll();                          // Destroy all currently created objects.

        std::string GetStatusString() const;                // Get the current status of the Factory as a string.

        unsigned int MaxCreated() const;                    // Returns the highest number of objects that existed at the same time.

        unsigned int CurCreated() const;                    // Returns the current number of created objects.

    private:
        Impl<T> impl;                                       // The data-structure that holds references to all the
                                                            // created objects. The exact characteristics of the data
                                                            // structure depend on the implementation used.

        // The two variables below are used for debugging purposes.
        // The core Factory functionality does not depend on
        // them (since they are disabled in Release mode).
        //
        // They have some overlapping functionality (e.g. they both
        // keep track of all the created objects) but this is
        // necessary as each can be enabled/disabled independently
        // of the other.

        #if ENABLE_FILE_LOCATION_TRACKING
            Debug::LocationMap<T> creationFileLocations;    // A map of all the objects that are currently exist.
                                                            // Each object is associated with the file location where
                                                            // it was created.
        #endif

        #if ENABLE_EXISTENCE_TRACKING
            Debug::ExistenceSet<T> createdObjects;          // A set of all the objects that are currently exist.
                                                            // It is used to do O(1) sanity checks since the underlying
                                                            // implementation doesn't have any performance requirements.
        #endif
};

/*
 * Set the typename string.
 */
SetTypenameString_TemplateTAndTemplateTemplateT( IFactory );

/*
 * Pull in the implementation file as well, so we don't get linker errors when
 * only including the header file.
 */
#include "Factory/IFactory.inl"

/*
 * Macros to automatically add the code location to the Create() function.
 *
 * An example of their use would be
 *
 *     int* x = FactoryCreate(int, 10);
 *     ...
 *     FactoryDestroy(int, x);
 */
#define FactoryCreate(Type, ...) \
    Factory<Type>::Get().Create( dbLocation(), ##__VA_ARGS__ )

#define FactoryDestroy(Type, object) \
    Factory<Type>::Get().Destroy( object )

#endif // IFACTORY_HPP_INCLUDED
