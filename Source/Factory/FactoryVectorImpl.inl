/*
 * This file implements a generic factory class that is used to
 * create, manage, and delete objects of a given type hierarchy.
 *
 * Author:      Stuart Rudderham
 * Created:     November 13, 2013
 * Last Edited: June 3, 2014
 */

#include "Debug/Debug.hpp"
#include "Typename.hpp"
#include <algorithm>
#include <cstdlib>
#include <cstring>      // memset
#include <utility>      // std::forward
#include <algorithm>    // std::max
#include <sstream>      // std::ostringstream

/*
 * Constructor.
 */
template<typename T>
FactoryVectorImpl<T>::FactoryVectorImpl()
{

}

/*
 * Destructor.
 */
template<typename T>
FactoryVectorImpl<T>::~FactoryVectorImpl()
{
    // If we haven't leaked any memory then there should be no objects we need
    // to destroy.
    dbAssert( DestroyAll() == 0, "Still had objects to destroy!" );
}

/*
 * Create a new object.
 */
template<typename T>
template<typename U, typename... Args>
U* FactoryVectorImpl<T>::Create( const L3::Debug::Location& loc, Args&&... args )
{
    // Make sure the object we are creating is a part of the object hierachy.
    static_assert(
        std::is_base_of<T, U>::value == true,
        "Trying to create object that's not in the hierachy!"
    );

    // Create the object.
    U* newObject = new U( std::forward<Args>(args)... );

    createdObjects.push_back( newObject );

    return newObject;
}

/*
 * Destroy an object created by the FactoryVectorImpl.
 */
template<typename T>
void FactoryVectorImpl<T>::Destroy( T* object )
{
    auto i = std::find( createdObjects.begin(), createdObjects.end(), object );

    delete *i;
    *i = createdObjects.back();

    createdObjects.pop_back();
}

/*
 * Return true if the given object was created by the FactoryVectorImpl.
 */
template<typename T>
bool FactoryVectorImpl<T>::Created( T* object ) const
{
    return std::find( createdObjects.begin(), createdObjects.end(), object ) != createdObjects.end();
}

/*
 * Destroy all objects for which the given predicate function returns true. Returns
 * the number of objects destroyed.
 */
template<typename T>
unsigned int FactoryVectorImpl<T>::BatchDestroy( bool (*Pred)(T*) )
{
    unsigned int oldSize = CurCreated();

    auto iter( std::remove_if(createdObjects.begin(), createdObjects.end(), Pred) );
myVector.erase(iter);

#if 0
    // Iterate over all created objects, destroying them if the predicate returns true.
    for( auto i = createdObjects.begin(); i != createdObjects.end(); ) {
        if( Pred( i->first ) == true ) {
            delete i->first;
            i = createdObjects.erase( i );
        } else {
            ++i;
        }
    }
#endif

    // Return how many objects were destroyed
    return oldSize - CurCreated();
}

/*
 * Destroy all objects created by this FactoryVectorImpl. Returns the number of objects destroyed.
 * Hope you don't leave any dangling pointers...
 */
template<typename T>
unsigned int FactoryVectorImpl<T>::DestroyAll()
{
    // Store the old size so we know how many objects we deleted.
    unsigned int numObjectsDestroyed = ForEach(
        []( T* object ) {
            delete object;
        }
    );

    createdObjects.clear();

    return numObjectsDestroyed;
}

/*
 * Returns the current number of created objects.
 */
template<typename T>
unsigned int FactoryVectorImpl<T>::CurCreated() const
{
    return createdObjects.size();
}
