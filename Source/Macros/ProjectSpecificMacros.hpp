/*
 * This header defines a bunch of utility macros and defines that are used in
 * a specific project.
 *
 * Author:      Stuart Rudderham
 * Created:     September 13, 2013
 * Last Edited: September 13, 2013
 */

#ifndef PROJECTSPECIFICMACROS_HPP_INCLUDED
#define PROJECTSPECIFICMACROS_HPP_INCLUDED

// Nothing right now...

#endif // PROJECTSPECIFICMACROS_HPP_INCLUDED
