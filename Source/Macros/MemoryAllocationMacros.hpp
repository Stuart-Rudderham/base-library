/*
 * This header defines macros that are used to override operator new and
 * operator delete so that a custom allocator can be used.
 *
 * Author:      Stuart Rudderham
 * Created:     January 30, 2013
 * Last Edited: June 7, 2014
 */

#ifndef MEMORYALLOCATIONMACROS_HPP_INCLUDED
#define MEMORYALLOCATIONMACROS_HPP_INCLUDED

#include "Debug/Location.hpp"               // Debug::Location
#include "Debug/Test.hpp"                   // dbAssert
#include "Allocator/IAllocator.hpp"
#include "Allocator/PoolAllocator.hpp"
#include "Allocator/BasicAllocator.hpp"

/*
 * Delete all the different new and delete operators that are automatically
 * created for a class, except for one. We don't remove the default operator
 * delete(void*) because we override it instead.
 */
#define REMOVE_IMPLICIT_NEW_AND_DELETE_OPERATORS()                             \
    /* Throwing operator new.                          */                      \
    /* Don't remove the corresponding operator delete. */                      \
    void* operator new( std::size_t )                          = delete;       \
                                                                               \
    /* Non-throwing operator new. */                                           \
    void* operator new( std::size_t, const std::nothrow_t& )   = delete;       \
    void operator delete( void*, const std::nothrow_t& )       = delete;       \
                                                                               \
    /* Placement operator new. */                                              \
    void* operator new( std::size_t, void* )                   = delete;       \
    void operator delete( void*, void* )                       = delete;       \
                                                                               \
    /* Throwing operator new[]. */                                             \
    void* operator new[]( std::size_t )                        = delete;       \
    void operator delete[]( void* )                            = delete;       \
                                                                               \
    /* Non-throwing operator new[]. */                                         \
    void* operator new[]( std::size_t, const std::nothrow_t& ) = delete;       \
    void operator delete[]( void*, const std::nothrow_t& )     = delete;       \
                                                                               \
    /* Placement operator new[]. */                                            \
    void* operator new[](std::size_t, void*)                   = delete;       \
    void operator delete[](void*, void*)                       = delete;


/*
 * Generic template for overriding the new and delete operators. Provide the
 * Allocator, Type that you are overriding the functions for, and whether or
 * not the allocator should actually be used.
 *
 * The "ShouldUse" boolean is constant, so the branch should be removed on any
 * decent compiler when optimizations are enabled.
 */
#define CUSTOM_NEW_AND_DELETE(Allocator, Type, ShouldUse)                      \
    /* Remove all builtin new and delete operators so that the */              \
    /* only usable ones are the operators that we define.      */              \
    REMOVE_IMPLICIT_NEW_AND_DELETE_OPERATORS();                                \
                                                                               \
    /* To catch typos and copy/paste errors we make sure that  */              \
    /* the type given for the Allocator and the class type are */              \
    /* the same. This has to be in a member function since we  */              \
    /* use the "this" pointer, so we can't put it in any of    */              \
    /* the "operator new" or "operator delete" functions we're */              \
    /* adding. Hopefully there are no name collisions.         */              \
    void CustomNewAndDeleteTypeCheckingSanityTestFunctionDoNotCall()           \
    {                                                                          \
        typedef Type AllocatorType;                                            \
        typedef std::remove_pointer<decltype(this)>::type ClassType;           \
                                                                               \
        static_assert(                                                         \
            std::is_same<AllocatorType, ClassType>::value,                     \
            "Type '" #Type "' given for Allocator does not match class type!"  \
        );                                                                     \
    }                                                                          \
                                                                               \
    /* Getter for the Allocator. Returns a const reference so it can't */      \
    /* be messed with.                                                 */      \
    static const Allocator& GetAllocator()                                     \
    {                                                                          \
        return Allocator::Get();                                               \
    }                                                                          \
                                                                               \
    /* Declare another placement new operator which has the */                 \
    /* location of the function call as an extra parameter. */                 \
    void* operator new( std::size_t size, const L3::Debug::Location& loc )     \
    {                                                                          \
        (void)size;                                                            \
        (void)loc;                                                             \
                                                                               \
        dbAssert(                                                              \
            size == sizeof(Type),                                              \
            "Tried to allocate ", size, " bytes "                              \
            "but sizeof(", #Type, ") is ", sizeof(Type), "."                   \
        );                                                                     \
                                                                               \
        if( ShouldUse ) {                                                      \
            return Allocator::Get().Allocate( loc );                           \
        }                                                                      \
        else {                                                                 \
            return ::operator new( size );                                     \
        }                                                                      \
    }                                                                          \
                                                                               \
    /* We need to create a placement operator delete with the same */          \
    /* arguments as our placement operator new so that things will */          \
    /* be cleaned up correctly if the object constructor throws an */          \
    /* exception.                                                  */          \
    void operator delete( void* addr, const L3::Debug::Location& )             \
    {                                                                          \
        if( ShouldUse ) {                                                      \
            Allocator::Get().Deallocate( static_cast<Type*>(addr) );           \
        }                                                                      \
        else {                                                                 \
            ::operator delete( addr );                                         \
        }                                                                      \
    }                                                                          \
                                                                               \
    /* Override the operator delete that is used in non-exceptional */         \
    /* cases to destroy the created object. It has the same code as */         \
    /* the placement operator delete above.                         */         \
    void operator delete( void* addr )                                         \
    {                                                                          \
        if( ShouldUse ) {                                                      \
            Allocator::Get().Deallocate( static_cast<Type*>(addr) );           \
        }                                                                      \
        else {                                                                 \
            ::operator delete( addr );                                         \
        }                                                                      \
    }

/*
 * Macro to override "new" and "delete" with functions that use an
 * PoolAllocator<T, N> instead.
 *
 * If ENABLE_POOL_ALLOCATOR is false then regular "new" and "delete" are used
 * instead.
 *
 * We delete all other implicitly generated operator new and operator delete
 * functions so that they aren't accidentally used.
 */
#define USE_POOL_ALLOCATOR(T, N)                                               \
    template<typename Type>                                                    \
    using AllocatorImpl = PoolAllocator<Type, N>;                              \
                                                                               \
    using Allocator = IAllocator<T, AllocatorImpl>;                            \
                                                                               \
    CUSTOM_NEW_AND_DELETE( Allocator, T, ENABLE_POOL_ALLOCATOR )

/*
 * Macro to override "new" and "delete" with functions that use an
 * BasicAllocator<T> instead.
 *
 * If ENABLE_BASIC_ALLOCATOR is false then regular "new" and "delete" are used
 * instead.
 *
 * We delete all other implicitly generated operator new and operator delete
 * functions so that they aren't accidentally used.
 */
#define USE_BASIC_ALLOCATOR(T)                                                 \
    template<typename Type>                                                    \
    using AllocatorImpl = BasicAllocator<Type>;                                \
                                                                               \
    using Allocator = IAllocator<T, AllocatorImpl>;                            \
                                                                               \
    CUSTOM_NEW_AND_DELETE( Allocator, T, ENABLE_BASIC_ALLOCATOR )

/*
 * Macro to wrap calls to "new" and "delete" with static functions Create(...)
 * and Destroy() so we can easily search for where we are allocating
 * memory (since new and delete are very common words using "grep" can be
 * difficult).
 */
#define WRAPPED_OPERATOR_NEW(Type)                                             \
    template<typename... Args>                                                 \
    static Type* Create( const L3::Debug::Location& loc, Args&&... args ) {    \
        return new( loc ) Type( std::forward<Args>(args)... );                 \
    }                                                                          \

#define WRAPPED_OPERATOR_DELETE(Type)                                          \
    static void Destroy( Type* object ) {                                      \
        delete object;                                                         \
    }

#define WRAPPED_NEW_AND_DELETE(Type)                                           \
    WRAPPED_OPERATOR_NEW(Type)                                                 \
    WRAPPED_OPERATOR_DELETE(Type)

/*
 * An example would be:
 *
 * class Foo
 * {
 *     public:
 *         // Wrap calls to "new" and "delete" with static functions
 *         // Create(...) and Destroy() for easy grepping.
 *         WRAPPED_NEW_AND_DELETE(Foo)
 *
 *     private:
 *         // Override "new" and "delete" to use the PoolAllocator.
 *         //
 *         // Marked as private because we want to force using the wrapped
 *         // calls, but doesn't have to be.
 *         USE_POOL_ALLOCATOR(Foo, 10);
 * };
 */

#endif // MEMORYALLOCATIONMACROS_HPP_INCLUDED
