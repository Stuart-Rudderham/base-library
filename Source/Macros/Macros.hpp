/*
 * This header defines a bunch of common utility macros and defines.
 *
 * Author:      Stuart Rudderham
 * Created:     September 13, 2013
 * Last Edited: June 30, 2014
 */

#ifndef MACROS_HPP_INCLUDED
#define MACROS_HPP_INCLUDED

#include "Preprocessor/Preprocessor.hpp"

/*
 * Toggle features based on whether we are compiling a Debug or Release build.
 * Only define the macro if it isn't already, so that these can be set from the
 * command line.
 */

// If disabled then Debug::Location structs will be left uninitialized
// and Debug::LocationMap::Add() and Debug::LocationMap::Remove() will be NOPS.
#ifndef ENABLE_FILE_LOCATION_TRACKING
    #define ENABLE_FILE_LOCATION_TRACKING       ( DEBUG_MODE && 1 ) || ( RELEASE_MODE && 0 )
#endif

// If disabled then Debug::Existence operations become NOPS.
#ifndef ENABLE_EXISTENCE_TRACKING
    #define ENABLE_EXISTENCE_TRACKING           ( DEBUG_MODE && 1 ) || ( RELEASE_MODE && 0 )
#endif

// If enabled then memory will be overwritten with an arbitrary bit pattern when it
// is deallocated by PoolAllocator or BasicAllocator.
#ifndef ENABLE_MEMORY_CLEARING
    #define ENABLE_MEMORY_CLEARING              ( DEBUG_MODE && 1 ) || ( RELEASE_MODE && 0 )
#endif

// If disabled then the PoolAllocator will use the default ::operator new() and ::operator delete() functions.
#ifndef ENABLE_POOL_ALLOCATOR
    #define ENABLE_POOL_ALLOCATOR               ( DEBUG_MODE && 1 ) || ( RELEASE_MODE && 1 )
#endif

// If disabled then the BasicAllocator will use the default ::operator new() and ::operator delete() functions.
#ifndef ENABLE_BASIC_ALLOCATOR
    #define ENABLE_BASIC_ALLOCATOR              ( DEBUG_MODE && 1 ) || ( RELEASE_MODE && 1 )
#endif

#endif // MACROS_HPP_INCLUDED
