#pragma once

/**
 * @file       Source/Debug/Crash.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       January 29, 2012
 * @date       Last Modified: June 5, 2014
 * @brief      Definition & implementation of L3::Debug::Crash
 */

#include "Debug/Location.hpp"           // Debug::SourceLocation, dbLocation
#include "Debug/Breakpoint.hpp"         // dbBreak
#include "IO/Print.hpp"                 // ioCerr
#include <utility>                      // std::forward
#include <cstdlib>                      // abort

/**
 * @brief Calls L3::Debug::Crash with the appropriate location information. Cannot be disabled.
 *
 * @param message   A message explaining the crash.
 * @param ...       Any additional arguments needed for the message.
 */
#define dbCrash( message, ... )                 \
    L3::Debug::Crash(                           \
        dbLocation(),                           \
        message,                                \
        ##__VA_ARGS__                           \
    )

namespace L3
{
namespace Debug
{
    /**
     * @brief   A function that crashes the program.
     *
     * @details Should be called using #dbCrash to get correct source location.
     *
     * @note    Does not destruct global/static variables before exit.
     *
     * @param   loc     The source location the crash occurred at.
     * @param   msg     A variable number of arguments that explain the crash.
     */
    template<typename... Args>
    void Crash( const Location& loc, Args&&... msg )
    {
        // Print out the given message.
        ioCerr( ""                                            );
        ioCerr( "CRASHING"                                    );
        ioCerr( "    Message  : ", std::forward<Args>(msg)... );
        ioCerr( "    Location : ", loc                        );
        ioCerr( ""                                            );

        // Insert a breakpoint.
        dbBreak( "Crashing" );

        // Flush output streams.
        std::cout.flush();
        std::cerr.flush();

        // Die. We use abort() rather than exit() because we don't
        // want the destructors for any static variables to run, since
        // that could trigger another crash.
        abort();
    }
}
}
