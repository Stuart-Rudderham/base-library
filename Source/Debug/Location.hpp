#pragma once

/**
 * @file       Source/Debug/Location.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       November 12, 2013
 * @date       Last Modified: June 3, 2014
 * @brief      Definition & implementation of L3::Debug::Location
 */

#include "Preprocessor/BuildType.hpp"       // DEBUG_MODE, RELEASE_MODE
#include "Preprocessor/CompilerInfo.hpp"    // FUNCTION_NAME
#include <iostream>                         // std::ostream

/**
 * @brief A switch for ignoring L3::Debug::Location objects. For example, a function could choose
 *        not to store a L3::Debug::Location in Release mode to reduce unnecessary overhead.
 */
#ifndef ENABLE_SOURCE_LOCATION_TRACKING
    #define ENABLE_SOURCE_LOCATION_TRACKING     ( DEBUG_MODE && 1 ) || ( RELEASE_MODE && 1 )
#endif

/**
 * @brief A macro that creates a L3::Debug::Location struct filled with the appropriate
 *        information regarding where the macro has been placed in the source file.
 */
#define dbLocation()              	\
    L3::Debug::Location(         	\
        __FILE__,                 	\
        FUNCTION_NAME,             	\
        __LINE__                  	\
    )

namespace L3
{
namespace Debug
{
    /**
     * @brief   A struct that holds information related to the location that something
     *          appears within the program.
     *
     * @details Should be created using #dbLocation to automatically get correct source
     *          location information.
     *
     * @todo    Rename to Debug::SourceLocation
     */
    struct Location
    {
        /**
         * @brief Constructor.
         *
         * @param fi    A filename.
         * @param fn    A function name.
         * @param l     A line number.
         */
        Location( const char* fi, const char* fn, int l )
            : file( fi )
            , func( fn )
            , line( l  )
        {

        }

        /// The file where the struct was created.
        const char* file;

        /// The function the struct was created inside.
        const char* func;

        /// The line the stuct was created on.
        int line;
    };
}
}

/**
 * @brief A printing function for Debug::Location.
 *
 * @param stream    The std::ostream to print to.
 * @param loc       The source location to print.
 */
std::ostream& operator<<( std::ostream& stream, const L3::Debug::Location& loc )
{
    stream << loc.file << ":" << loc.line /*<< " : " << loc.func*/;
    return stream;
}
