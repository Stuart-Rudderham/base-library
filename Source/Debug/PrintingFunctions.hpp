/*
 * This header defines all the operator<< overloads on std::ostream for various
 * non-builtin types that are used in a specific project.
 *
 * Author:      Stuart Rudderham
 * Created:     September 13, 2013
 * Last Edited: September 13, 2013
 */

#ifndef PRINTINGFUNCTIONS_HPP_INCLUDED
#define PRINTINGFUNCTIONS_HPP_INCLUDED

// Nothing right now...

#endif // PRINTINGFUNCTIONS_HPP_INCLUDED
