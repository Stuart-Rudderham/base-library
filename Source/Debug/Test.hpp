#pragma once

/**
 * @file       Source/Debug/Test.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       January 29, 2012
 * @date       Last Modified: June 6, 2014
 * @brief      Definition & implementation of L3::Debug::Test
 */

#include "Debug/Location.hpp"           // Debug::SourceLocation, dbLocation
#include "Debug/Crash.hpp"              // dbCrash
#include "IO/Print.hpp"                 // ioCerr
#include "Preprocessor/BuildType.hpp"   // DEBUG_MODE, RELEASE_MODE
#include <utility>                      // std::forward

/**
 * @brief If false then calls to #dbAssert are not emitted.
 */
#ifndef ENABLE_ASSERTS
    #define ENABLE_ASSERTS                      ( DEBUG_MODE && 1 ) || ( RELEASE_MODE && 1 )
#endif

/**
 * @brief If false then calls to #dbWarn are not emitted.
 */
#ifndef ENABLE_WARNINGS
    #define ENABLE_WARNINGS                     ( DEBUG_MODE && 1 ) || ( RELEASE_MODE && 1 )
#endif

/**
 * @brief If false then calls to #dbErrorCheck will execute the condition but not check the result.
 */
#ifndef ENABLE_ERROR_CHECKING
    #define ENABLE_ERROR_CHECKING               ( DEBUG_MODE && 1 ) || ( RELEASE_MODE && 1 )
#endif

/**
 * @brief Calls L3::Debug::Test as an assert. If the condition is false a message is
 *        printed and the program will crash. Controlled by the macro #ENABLE_ASSERTS.
 *
 * @note  If #ENABLE_ASSERTS is false the function call with not be emitted, so do not
 *        supply a condition that has side-effects as they will not occur. The macro
 *        #dbErrorCheck should be used instead in this case.
 *
 * @param condition   The boolean condition to be tested.
 * @param message     A message explaining the assert.
 * @param ...         Any additional arguments needed for the message.
 */
#if ENABLE_ASSERTS
    #define dbAssert( condition, message, ... )         \
        L3::Debug::Test(                                \
            condition,                                  \
            true,                                       \
            dbLocation(),                               \
            #condition,                                 \
            message,                                    \
            ##__VA_ARGS__                               \
        )
#else
    #define dbAssert( condition, message, ... )
#endif

/**
 * @brief Calls L3::Debug::Test as an warning. If the condition is false a message is
 *        printed and the program will continue execution. Controlled by the macro
 *        #ENABLE_WARNINGS.
 *
 * @note  If #ENABLE_WARNINGS is false the function call with not be emitted, so do not
 *        supply a condition that has side-effects as they will not occur.
 *
 * @note  This seems like a useful thing to have but I've never had a situation where
 *        I didn't want to crash immediately if the condition was false.
 *
 * @param condition   The boolean condition to be tested.
 * @param message     A message explaining the warning.
 * @param ...         Any additional arguments needed for the message.
 */
#if ENABLE_WARNINGS
    #define dbWarn( condition, message, ... )           \
        L3::Debug::Test(                                \
            condition,                                  \
            false,                                      \
            dbLocation(),                               \
            #condition,                                 \
            message,                                    \
            ##__VA_ARGS__                               \
        )
#else
    #define dbWarn( condition, message, ... )
#endif

/**
 * @brief Identical to #dbAssert when enabled, but will still evaluate the condition when
 *        disabled. This is useful for checking the return code of functions with side
 *        effects, such as \c std::set::erase. Controlled by the macro #ENABLE_ERROR_CHECKING.
 *
 * @note  I'm not convinced this is a good thing to have. It is convenient but encourages
 *        poor programming practises. If a return code is important enough to be checked
 *        in debug mode then it should probably also be checked in release mode.
 *
 * @param condition   The boolean condition to be tested.
 * @param message     A message explaining the warning.
 * @param ...         Any additional arguments needed for the message.
 */
#if ENABLE_ERROR_CHECKING
    #define dbErrorCheck( condition, message, ... )     \
        L3::Debug::Test(                                \
            condition,                                  \
            true,                                       \
            dbLocation(),                               \
            #condition,                                 \
            message,                                    \
            ##__VA_ARGS__                               \
        )
#else
    #define dbErrorCheck( condition, message, ... )     \
        (condition)
#endif

namespace L3
{
namespace Debug
{
    /**
     * @brief A custom assert function. Will print a message and optionally crash
     *        the program if the given condition is false.
     *
     * @note  This function shouldn't really be called directly, use the #dbAssert,
     *        #dbWarn, or #dbErrorCheck macros to automatically supply the appropriate
     *        arguments that provide the desired behaviour.
     *
     * @param test          The boolean condition to be tested.
     * @param crashIfFail   If true then if \c test is false the program will crash.
     * @param loc           The source location where the test occurred.
     * @param testString    The condition tested string-ified.
     * @param msg           A variable number of arguments that compose a message explaining the test.
     */
    template<typename... Args>
    void Test( bool test, bool crashIfFail, const Location& loc, const char* testString, Args&&... msg )
    {
        // If the test fails print out an error message.
        if( test == false )
        {
            ioCerr( ""                                            );
            ioCerr( crashIfFail ? "ASSERT" : "WARNING"            );
            ioCerr( "    Message  : ", std::forward<Args>(msg)... );
            ioCerr( "    Location : ", loc                        );
            ioCerr( "    Function : ", loc.func                   );
            ioCerr( "    Test     : ", testString                 );
            ioCerr( ""                                            );

            // Then terminate the program (if desired).
            if( crashIfFail )
            {
                dbCrash( "Assertion failed!" );
            }
        }
    }
}
}
