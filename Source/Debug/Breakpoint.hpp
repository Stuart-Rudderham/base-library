#pragma once

/**
 * @file       Source/Debug/Breakpoint.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       January 29, 2012
 * @date       Last Modified: June 5, 2014
 * @brief      Definition & implementation of L3::Debug::Breakpoint
 */

#include "Debug/Location.hpp"               // Debug::Location, dbLocation
#include "IO/Print.hpp"                     // ioCerr
#include "Preprocessor/BuildType.hpp"       // DEBUG_MODE, RELEASE_MODE
#include "Preprocessor/CompilerInfo.hpp"    // USING_MSVC, USING_GCC, USING_CLANG
#include <utility>                          // std::forward

/**
 * @brief If false then calls to #dbBreak are not emitted.
 */
#ifndef ENABLE_BREAKPOINTS
    #define ENABLE_BREAKPOINTS      ( DEBUG_MODE && 1 ) || ( RELEASE_MODE && 1 )
#endif

/**
 * @brief Calls L3::Debug::Breakpoint with the appropriate location information. Controlled
 *        by the macro #ENABLE_BREAKPOINTS.
 *
 * @param message   A message explaining the breakpoint.
 * @param ...       Any additional arguments needed for the message.
 */
#if ENABLE_BREAKPOINTS
    #define dbBreak( message, ... )             \
        L3::Debug::Breakpoint(                  \
            dbLocation(),                       \
            message,                            \
            ##__VA_ARGS__                       \
        )
#else
    #define dbBreak( message, ... )
#endif

namespace L3
{
namespace Debug
{
    /**
     * @brief   A function that pause the program with a breakpoint, with hooks for a debugger.
     *
     * @details Should be called using #dbBreak to get correct source location.
     *
     * @param   loc     The source location the breakpoint was inserted at.
     * @param   msg     A variable number of arguments that explain the breakpoint.
     */
    template<typename... Args>
    void Breakpoint( const Location& loc, Args&&... msg )
    {
        // Print out the given message.
        ioCerr( ""                                            );
        ioCerr( "BREAKPOINT"                                  );
        ioCerr( "    Message  : ", std::forward<Args>(msg)... );
        ioCerr( "    Location : ", loc                        );
        ioCerr( ""                                            );

        // Insert a breakpoint.
        #if USING_MSVC
            __debugbreak();
        #elif USING_CLANG
            __builtin_debugtrap();
        #elif USING_GCC
            __asm__( "int $0x3" );
        #else
            #error "Unknown compiler!"
        #endif
    }
}
}
