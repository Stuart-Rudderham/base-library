#pragma once

/**
 * @file       Source/Debug/Cast.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       January 29, 2012
 * @date       Last Modified: June 7, 2014
 * @brief      Definition & implementation of ::poly_cast.
 */

#include "Debug/Test.hpp"                   // dbAssert
#include "Preprocessor/BuildType.hpp"       // DEBUG_MODE, RELEASE_MODE

/**
 * @brief If false then ::poly_cast will not confirm that the cast is valid
 *        before doing it (similar to \c static_cast).
 */
#ifndef ENABLE_POLY_CAST
    #define ENABLE_POLY_CAST    ( DEBUG_MODE && 1 ) || ( RELEASE_MODE && 1 )
#endif

/**
 * @brief   A function for down-casting pointers through an inheritance hierarchy. If
 *          #ENABLE_POLY_CAST is true then a check is performed confirming that cast
 *          is valid first.
 *
 * @details Idea from <a href="http://aegisknight.livejournal.com/101698.html">here</a>
 *
 * @note    Asserts must be enabled for ::poly_cast to be enabled.
 * @note    ::poly_cast cannot be used with virtual inheritance.
 *
 * @tparam  ToType      The type being casted to.
 * @tparam  FromType    The type being casted from.
 *
 * @param   ptr         The pointer to be casted.
 */
template<typename ToType, typename FromType>
ToType poly_cast( FromType* ptr )
{
    // Make sure we're not casting a NULL pointer, since that is
    // what dynamic_cast returns if it fails.
    dbAssert( ptr != nullptr, "poly_cast cannot be used with a NULL pointer!" );

    #if ENABLE_POLY_CAST
        // Check that cast is valid using dynamic_cast.
        dbAssert( dynamic_cast<ToType>( ptr ) != nullptr, "poly_cast failed!" );
    #endif

    return static_cast<ToType>( ptr );
}
