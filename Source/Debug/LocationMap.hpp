/*
 * This header defines a class that matches addresses/objects with
 * the file location they were allocated/created.
 *
 * Author:      Stuart Rudderham
 * Created:     November 19, 2013
 * Last Edited: June 7, 2014
 */

#ifndef LOCATIONMAP_HPP_INCLUDED
#define LOCATIONMAP_HPP_INCLUDED

#include "Macros/Macros.hpp"
#include "Debug/Location.hpp"   // Debug::Location
#include "Debug/Test.hpp"       // dbAssert

#include <unordered_map>
#include <tuple>            // std::forward_as_tuple
#include <vector>
#include <algorithm>        // std::sort
#include <utility>          // std::make_pair

namespace Debug
{
    /*
     * This class can be disabled using the ENABLE_FILE_LOCATION_TRACKING
     * macro to have zero performance impact in release mode. It will still
     * have a non-zero size however.
     *
     * TODO: This might not have to be templated if we use just track void*s. Look
     * into how that works with virtual inheritance. It will still have to stay in
     * the header file though, so that the optimizer can see if the function bodies
     * are empty and optimize the calls away.
     */
    template<typename T>
    class LocationMap
    {
        public:
            void Add( T* addr, const L3::Debug::Location& loc )
            {
                #if ENABLE_FILE_LOCATION_TRACKING
                    dbAssert(
                        locations.count( addr ) == 0,
                        "Address ", addr, " already in map!"
                    );

                    locations.insert( std::make_pair(addr, loc) );
                #else
                    // Suppress compiler warnings.
                    (void)addr;
                    (void)loc;
                #endif
            }

            void Remove( T* addr )
            {
                #if ENABLE_FILE_LOCATION_TRACKING
                    dbAssert(
                        locations.count( addr ) != 0,
                        "Address ", addr, " not in map!"
                    );

                    locations.erase( addr );
                #else
                    // Suppress compiler warnings.
                    (void)addr;
                #endif
            }

            std::vector<std::pair<T*, L3::Debug::Location>> GetContents() const
            {
                std::vector<std::pair<T*, L3::Debug::Location>> sortedContents;

                #if ENABLE_FILE_LOCATION_TRACKING
                    sortedContents.reserve( locations.size() );

                    for( const auto& i : locations ) {
                        sortedContents.push_back( i );
                    }

                    // Since the map keys are object addresses the iteration order isn't
                    // predictable. So to keep deterministic results we sort the contents by
                    // file location first. This function isn't called very often (mostly
                    // just at program end) so the overhead isn't important.
                    //
                    // TODO: use C++14 generic lambdas when possible, would make the
                    // function signature a whole lot prettier. Also this is a good place
                    // for an std::dynarray as opposed to an std::vector.
                    std::sort(
                        sortedContents.begin(),
                        sortedContents.end(),
                        [&](const std::pair<T*, L3::Debug::Location>& lhs, const std::pair<T*, L3::Debug::Location>& rhs)
                        {
                            return std::forward_as_tuple(lhs.second.file, lhs.second.line, lhs.first) <
                                   std::forward_as_tuple(rhs.second.file, rhs.second.line, rhs.first);
                        }
                    );
                #endif

                return sortedContents;
            }

            unsigned int GetSize() const
            {
                return locations.size();
            }

            void Clear()
            {
                locations.clear();
            }

        private:
            std::unordered_map<T*, L3::Debug::Location> locations;
    };
}

#endif // LOCATIONMAP_HPP_INCLUDED
