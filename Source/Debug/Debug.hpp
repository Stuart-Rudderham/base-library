#pragma once

/**
 * @file       Source/Debug/Debug.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       June 7, 2014
 * @date       Last Modified: June 7, 2014
 * @brief      A file that includes all classes, functions, and macros
 *             defined in the L3::Debug namespace.
 * @details    A convenience file for including the entire Debug module with
 *             one command.
 */

#include "Debug/Breakpoint.hpp"
#include "Debug/Cast.hpp"
#include "Debug/Crash.hpp"
#include "Debug/Location.hpp"
#include "Debug/Test.hpp"
