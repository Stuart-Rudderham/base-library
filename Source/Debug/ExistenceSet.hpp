/*
 * This header a class that maintains a set of addresses/objects. Its
 * purpose is to detect double insertions or deletions in O(1) time. This
 * is useful when dealing with containers like std::vector, which would
 * normally take O(n) time.
 *
 * Author:      Stuart Rudderham
 * Created:     November 19, 2013
 * Last Edited: June 7, 2014
 */

#ifndef EXISTENCESET_HPP_INCLUDED
#define EXISTENCESET_HPP_INCLUDED

#include "Macros/Macros.hpp"
#include "Debug/Test.hpp"       // dbAssert

#include <unordered_set>

namespace Debug
{
    /*
     * It can be disabled using the ENABLE_EXISTENCE_TRACKING macro to have zero
     * performance impact in release mode.
     *
     * TODO: This might not have to be templated if we use just track void*s. Look
     * into how that works with virtual inheritance. It will still have to stay in
     * the header file though, so that the optimizer can see if the function bodies
     * are empty and optimize the calls away.
     */
    template<typename T>
    class ExistenceSet
    {
        public:
            void Add( T* addr )
            {
                #if ENABLE_EXISTENCE_TRACKING
                    dbAssert(
                        objects.count( addr ) == 0,
                        "Address ", addr, " already in set!"
                    );

                    objects.insert( addr );
                #else
                    // Supress compiler warnings.
                    (void)addr;
                #endif
            }

            void Remove( T* addr )
            {
                #if ENABLE_EXISTENCE_TRACKING
                    dbAssert(
                        objects.count( addr ) != 0,
                        "Address ", addr, " not in set!"
                    );

                    objects.erase( addr );
                #else
                    // Supress compiler warnings.
                    (void)addr;
                #endif
            }

            unsigned int GetSize() const
            {
                return objects.size();
            }

        private:
            std::unordered_set<T*> objects;
    };
}

#endif // EXISTENCESET_HPP_INCLUDED
