#pragma once

/**
 * @file       Source/IO/Print.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       January 29, 2012
 * @date       Last Modified: June 30, 2014
 * @brief      Definition & implementation of L3::IO::Print
 *
 * @details    The purpose of this file is to provide a basic wrapper around C++ ostreams. It makes
 *             print calls look slightly neater as well as providing some helpful functionality such
 *             as automatically appending newlines. Enabling optimizations will inline all the
 *             function calls so there is no performance overhead compared to using raw ostreams.
 *
 * @example    Examples/IO/Print.cpp
 */

#include "Preprocessor/BuildType.hpp"   // DEBUG_MODE, RELEASE_MODE
#include <iostream>                     // std::ostream, std::cout, std::cerr
#include <utility>                      // std::forward

/**
 * @brief A wrapper around \c L3::IO::Print that gives a nice user interface. Exists
 *        mostly to be consistent with L3::IO::Printf and #ioPrintf.
 *
 * @param stream    The std::ostream to be printed to.
 * @param ...       Zero or more extra arguments to be inserted into \c stream.
 */
#define ioPrint( stream, ... )                              \
    L3::IO::Print( stream, ##__VA_ARGS__ )

/**
 * @brief Identical to #ioPrint except that it also inserts a newline character after all the
 *        arguments.
 *
 * @param stream    The std::ostream to be printed to.
 * @param ...       Zero or more extra arguments to be inserted into \c stream.
 *
 * @todo Should append "\n" on Linux and "\r\n" on Windows
 */
#define ioPrintln( stream, ... )                            \
    L3::IO::Print( stream, ##__VA_ARGS__, "\n" )

/**
 * @brief Calls #ioPrintln with \c std::cout.
 *
 * @note  GCC has an extension that removes the previous comma in the event that the \c __VA_ARGS__
 *        macro is empty. However, it does not do this if you do not provide any arguments. So
 *        calling ioPrintln(std::cout) will compile but calling ioCout() will not. That is the only
 *        difference between them. MSVC does not have this issue.
 *
 * @param ...   Zero or more arguments to be printed to \c std::cout.
 */
#define ioCout( ... )                                       \
    ioPrintln( std::cout, ##__VA_ARGS__ )

/**
 * @brief Calls #ioPrintln with \c std::cerr.
 *
 * @note  GCC has an extension that removes the previous comma in the event that the \c __VA_ARGS__
 *        macro is empty. However, it does not do this if you do not provide any arguments. So
 *        calling ioPrintln(std::cerr) will compile but calling ioCerr() will not. That is the only
 *        difference between them. MSVC does not have this issue.
 *
 * @param ...       Zero or more arguments to be printed to \c std::cerr.
 */
#define ioCerr( ... )                                       \
    ioPrintln( std::cerr, ##__VA_ARGS__ )

/**
 * @brief If false then calls to #ioVerbose are not emitted.
 */
#ifndef ENABLE_VERBOSE_OUTPUT
    #define ENABLE_VERBOSE_OUTPUT   ( DEBUG_MODE && 1 ) || ( RELEASE_MODE && 1 )
#endif

/**
 * @brief A call to #ioCout that can be disabled. Useful for output that doesn't
 *        always need to be printed, such as debugging text. Controlled by the
 *        macro #ENABLE_VERBOSE_OUTPUT.
 *
 * @param ...   Zero or more arguments to be printed to \c std::cout.
 */
#if ENABLE_VERBOSE_OUTPUT
    #define ioVerbose( ... ) ioCout( __VA_ARGS__ )
#else
    #define ioVerbose( ... )
#endif

namespace L3
{
namespace IO
{
    namespace Internal
    {
        /**
         * @brief   Base case for IO::Print.
         *
         * @details Called when there are no arguments left to print. Does nothing.
         */
        inline void PrintHelper( std::ostream& )
        {
            // Nothing to do.
        }

        /**
         * @brief   Recursive case for IO::Print.
         *
         * @details Called when there are one or more arguments left to print. Prints the first
         *          argument and then recurses on the rest.
         *
         * @tparam  T           The type of the first argument.
         * @tparam  Args        The types of the rest of the arguments.
         *
         * @param   stream      The \c std::ostream that the arguments are inserted into.
         * @param   value       The first arguments to be printed.
         * @param   args        The rest of the arguments to be printed. May be empty.
         */
        template<typename T, typename... Args>
        inline void PrintHelper( std::ostream& stream, T&& value, Args&&... args )
        {
            // Print first argument.
            stream << std::forward<T>(value);

            // Recurse on rest of arguments.
            PrintHelper( stream, std::forward<Args>(args)... );
        }
    }

    /**
     * @brief   A function that inserts zero of more arguments into the given \c std::ostream.
     *
     * @details This function simply wraps C++ iostreams, so when optimizations are enabled
     *          everything should be inlined and the generated code will be identical to using
     *          iostreams directly. There's no real need to call this function directly, the macros
     *          #ioPrint and #ioPrintln are the expected interface that should be used instead.
     *
     * @tparam  Args      The types of the arguments to be inserted.
     *
     * @param   stream    The \c std::ostream that the arguments are inserted into.
     * @param   args      The zero or more arguments to be inserted into \c stream.
     */
    template<typename... Args>
    inline void Print( std::ostream& stream, Args&&... args )
    {
        // Format the stream properly.
        //stream << std::noskipws;                            // Don't skip leading whitespace.
        //stream << std::fixed << std::setprecision( 2 );     // Print out 2 digits after the decimal place.
        //stream << std::boolalpha;                           // Print True/False instead of 1/0

        // Helper function does the actual printing.
        Internal::PrintHelper( stream, std::forward<Args>(args)... );
    }
}
}

// Stuff for prefixing a timestamp
#if 0
    using namespace std::chrono;

    auto timeSinceEpoch = high_resolution_clock::now().time_since_epoch();

    auto timeSinceEpochInMicroseconds = duration_cast<microseconds>(timeSinceEpoch).count();

    auto microsecondsPerSecond = duration_cast<microseconds>( seconds(1) ).count();

    auto  secPortion = timeSinceEpochInMicroseconds / microsecondsPerSecond;
    auto usecPortion = timeSinceEpochInMicroseconds % microsecondsPerSecond;

    // We format the timestamp so the microsecond portion always has the full 6 digits.
    ioPrintf( stream, "~.~~~ | ", secPortion, std::setw(6), std::setfill('0'), usecPortion );
#endif