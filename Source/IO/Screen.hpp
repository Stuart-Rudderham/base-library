#pragma once

/**
 * @file       Source/IO/Screen.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       January 29, 2012
 * @date       Last Modified: June 7, 2014
 * @brief      Definition & implementation of #ioScreen
 */

#include "IO/Print.hpp"     // ioPrintln
#include <sstream>          // std::stringstream

/**
 * @brief Prints the given arguments to a text buffer which can be drawn to the screen at
 *        a later point in time.
 *
 * @param ...   Zero or more arguments to be printed to the screen.
 */
#define ioScreen( ... )                                 \
    ioPrintln( L3::IO::ScreenString, ##__VA_ARGS__ )

namespace L3
{
namespace IO
{
    /*
     * The global string that is used to buffer debug text that is to be drawn
     * to the screen.
     *
     * It is cleared each time the string is drawn to the screen.
     */
    extern std::stringstream ScreenString;

    /*
     * A function to draw the debug text onto the screen.
     *
     * How this is done and what parameters are needed depends
     * on what drawing libraries are being used, so that will
     * need to be implemented on a per-project basis.
     */
    void DrawScreenString();
}
}
