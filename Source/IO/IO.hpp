#pragma once

/**
 * @file       Source/IO/IO.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       Feburary 28, 2014
 * @date       Last Modified: June 7, 2014
 * @brief      A file that includes all classes, functions, and macros
 *             defined in the L3::IO namespace.
 * @details    A convenience file for including the entire IO module with
 *             one command.
 */

#include "IO/Print.hpp"
#include "IO/Screen.hpp"

#include "IO/PrettyPrint/Array.hpp"
#include "IO/PrettyPrint/Deque.hpp"
#include "IO/PrettyPrint/ForwardList.hpp"
#include "IO/PrettyPrint/GenericContainer.hpp"
#include "IO/PrettyPrint/InitializerList.hpp"
#include "IO/PrettyPrint/List.hpp"
#include "IO/PrettyPrint/Map.hpp"
#include "IO/PrettyPrint/Pair.hpp"
#include "IO/PrettyPrint/Set.hpp"
#include "IO/PrettyPrint/Tuple.hpp"
#include "IO/PrettyPrint/UnorderedMap.hpp"
#include "IO/PrettyPrint/UnorderedSet.hpp"
#include "IO/PrettyPrint/Vector.hpp"
