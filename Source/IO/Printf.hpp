#pragma once

/**
 * @file       Source/IO/Printf.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       Feburary 6, 2014
 * @date       Last Modified: July 28, 2014
 * @brief      Definition & implementation of formatted printing functions.
 *
 * @example    Examples/IO/Printf.cpp
 */

#include "IO/Print.hpp"                 // ioPrint
#include "IO/Formatter.hpp"             // L3::IO::MakeFormatter
#include <tuple>                        // std::forward_as_tuple

/**
 * @brief A version of #ioPrint that uses formatted output.
 *
 * @todo  Make unit tests.
 *
 * @param stream    The std::ostream to be printed to.
 * @param formatStr The format string to be used.
 * @param ...       Zero or more extra arguments to be inserted into \c stream.
 */
#define ioPrintf( stream, formatStr, ... )                                                      \
    {                                                                                           \
        PasteFormatStringStructDefinition( FormatStringStruct, formatStr );                     \
                                                                                                \
        ioPrint(                                                                                \
            stream,                                                                             \
            L3::IO::MakeFormatter<FormatStringStruct>( std::forward_as_tuple(__VA_ARGS__) )     \
        );                                                                                      \
    }

/**
 * @brief A version of #ioPrintln that uses formatted output.
 *
 * @todo  Make unit tests.
 *
 * @param stream    The std::ostream to be printed to.
 * @param formatStr The format string to be used.
 * @param ...       Zero or more extra arguments to be inserted into \c stream.
 *
 * @todo Should append "\n" on Linux and "\r\n" on Windows
 */
#define ioPrintlnf( stream, formatStr, ... )                \
    ioPrintf( stream, formatStr "\n", ##__VA_ARGS__ );

/**
 * @brief A version of #ioCout that uses formatted output.
 *
 * @todo  Make unit tests.
 *
 * @param formatStr The format string to be used.
 * @param ...       Zero or more arguments to be printed to \c std::cout.
 */
#define ioCoutf( formatStr, ... )                           \
    ioPrintlnf( std::cout, formatStr, ##__VA_ARGS__ )

/**
 * @brief A version of #ioCerr that uses formatted output.
 *
 * @todo  Make unit tests.
 *
 * @param formatStr The format string to be used.
 * @param ...       Zero or more arguments to be printed to \c std::cerr.
 */
#define ioCerrf( formatStr, ... )                           \
    ioPrintlnf( std::cerr, formatStr, ##__VA_ARGS__ )

/**
 * @brief A version of #ioVerbose that uses formatted output. Also controlled by the
 *        macro #ENABLE_VERBOSE_OUTPUT.
 *
 * @todo  Make unit tests.
 *
 * @param formatStr The format string to be used.
 * @param ...       Zero or more arguments to be printed to \c std::cout.
 */
#if ENABLE_VERBOSE_OUTPUT
    #define ioVerbosef( formatStr, ... ) ioCoutf( formatStr, ##__VA_ARGS__ )
#else
    #define ioVerbosef( formatStr, ... )
#endif
