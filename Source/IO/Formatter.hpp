#pragma once

/**
 * @file       Source/IO/Formatter.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       Feburary 28, 2014
 * @date       Last Modified: July 28, 2014
 * @brief      Definition & implementation of L3::IO::Formatter
 */

#include "Metaprogramming/IndexSequence.hpp"    // IndexSequence
#include <iostream>                             // std::ostream
#include <utility>                              // std::forward
#include <tuple>                                // std::tuple, std::forward_as_tuple, std::get, tuple_size

namespace L3
{
namespace IO
{
    /**
     * @brief   The character in the format string that is replaced by a value when calling \c IO::Printf().
     *
     * @details Do not change this constant unless absolutely necessary, as it will mean having to change
     *          all existing format strings to use the new character. It won't silently fail, but there
     *          will be a lot of compile errors to fix.
     */
    extern constexpr char PrintfReplacementCharacter = '~';

    namespace Internal
    {
        /**
         * @brief   A function that returns the minimum of two values. Only needed because std::min() is not
         *          marked \c constexpr.
         */
        template<typename T>
        constexpr const T& Min( const T& left, const T& right )
        {
            return left < right ? left : right;
        }

        /**
         * @brief   A helper function that returns, at compile-time, the index of the first occurance of
         *          the character \c within the range [begin, end). If the character \c does not appear inside
         *          the given range the index corresponding to the null-terminating character of the string is
         *          returned. Note that this is different from the behaviour of the function std::find_first_of().
         *
         * @tparam N    The length of the string being inspected (including the null-terminating character).
         *
         * @param str   The string to be inspected. Must be a compile time value of known length.
         * @param c     The character that is being searched for.
         * @param begin The index of the start of the range being searched. Defaults to 0 (i.e. the beginning of the string).
         * @param end   The index of the end of the range being searched. Defaults to N (i.e. one past the end of the string).
         *
         * @return  The index of the first character in the range [begin, end) that is equal to \c.
         *          If the character \c isn't found anywhere in the range then N is returned.
         *
         * @todo    Replace recursion with loop when C++14 is available.
         *          This will also allow us to remove the Min function above.
         *          Will also allow us to change return value to match std::find_first_of behaviour.
         */
        template<std::size_t N>
        constexpr std::size_t FindFirstOf( const char (&str)[N], char c, std::size_t begin = 0, std::size_t end = N)
        {
            /*
             * Equivalent to the following code:
             *
             *    if( begin >= end ) {
             *        return N;
             *    } else {
             *        if( end - begin == 1 ) {
             *            if( str[begin] == c ) {
             *                return begin;
             *            } else {
             *                return N;
             *            }
             *        } else {
             *            std::size_t mid = begin + (end - begin) / 2;
             *            return Min( FindFirstOf(str, c, begin, mid), FindFirstOf(str, c, mid, end) );
             *        }
             *    }
             */
            return (begin >= end)
                       ? N
                       : ( (end - begin == 1)
                               ? ( str[begin] == c
                                       ? begin
                                       : N )
                               : Min( FindFirstOf(str, c, begin, begin + (end - begin) / 2), FindFirstOf(str, c, begin + (end - begin) / 2, end) ) );
        }

        /**
         * @brief   Base case.
         */
        template<typename FormatString, std::size_t Begin, std::size_t End>
        inline void PrintfHelper( std::ostream& stream )
        {
            // Sanity check.
            static_assert( Begin <= End, "Invalid index range!" );

            // Make sure there aren't any format specifiers left over.
            constexpr std::size_t FirstChar = FindFirstOf( FormatString::Str(), PrintfReplacementCharacter, Begin, End );
            static_assert( FirstChar == End, "Not enough arguments to IO::Printf() function!" );

            constexpr std::streamsize Length = End - 1 - Begin;
            static_assert( Length >= 0, "Negative length!" );

            if( Length > 0 ) {
                stream.write( FormatString::Str() + Begin, Length );
            }
        }

        /**
         * @brief   Recursive case.
         */
        template<typename FormatString, std::size_t Begin, std::size_t End, typename T, typename... Args>
        inline void PrintfHelper( std::ostream& stream, T&& value, Args&&... args )
        {
            // Sanity check.
            static_assert( Begin <= End, "Invalid index range!" );

            // Find the index of the first format specifier. If there isn't one then we have too many arguments.
            constexpr std::size_t FirstChar = FindFirstOf( FormatString::Str(), PrintfReplacementCharacter, Begin, End );
            static_assert( FirstChar != End, "Too many arguments to IO::Printf() function!" );

            // Print out the section of the format string before the format specifier.
            constexpr std::streamsize Length = FirstChar - Begin;
            static_assert( Length >= 0, "Negative length!" );

            if( Length > 0 ) {
                stream.write( FormatString::Str() + Begin, Length );
            }

            // Replace the format specifier with the first argument.
            stream << std::forward<T>(value);

            // Recurse with the section of the format string after the format specifier and the rest of the arguments.
            PrintfHelper<FormatString, Begin + Length + 1, End>( stream, std::forward<Args>(args)... );
        }

        /**
         * @brief   Interface.
         */
        template<typename FormatString, typename... Args>
        inline void Printf( std::ostream& stream, Args&&... args )
        {
            // Sanity check to try and make error messages prettier.
            // Checks to make sure the given FormatString struct has a function Str() that
            // returns something equivalent to a const char* (i.e. a const char[N]).
            static_assert( std::is_same<const char*, typename std::decay<decltype(FormatString::Str())>::type>::value, "FormatString does not wrap a const char[]" );

            // Helper function does the actual printing.
            constexpr std::size_t Begin = 0u;
            constexpr std::size_t End   = std::extent< typename std::remove_reference<decltype( FormatString::Str() )>::type >::value;

            PrintfHelper<FormatString, Begin, End>( stream, std::forward<Args>(args)... );
        }
    }

    // Be careful, if you give it rvalue references and don't print right away you may be left with dangling references.
    // Doesn't check for correct # of args until you try and print it.
    template<typename FormatString, typename ArgTuple>
    struct Formatter
    {
        Formatter( ArgTuple&& t )
            : argTuple( std::forward<ArgTuple>(t) )
        {
            //std::cout << "Formatter::Formatter()" << std::endl;
        }

        ~Formatter()
        {
            //std::cout << "Formatter::~Formatter()" << std::endl;
        }

        // In theory this needs to be defined so that MakePrinter can return a Formatter object by value.
        // In practise Return-Value-Optimization kicks in and the copy constructor call is elided, so this never actually gets called.
        /*
        Formatter( const Formatter& other )
            : argTuple( std::forward<ArgTuple>(other.argTuple) )
        {
            //std::cout << "Formatter::Formatter( const Formatter& other )" << std::endl;
        }
        */

        Formatter( const Formatter& ) = delete;
        Formatter& operator=( const Formatter& ) = delete;

        Formatter( Formatter&& ) = default;
        Formatter& operator=( Formatter&& ) = delete;

        template<unsigned int... Indices>
        void Print( std::ostream& stream, IndexSequence<Indices...> )
        {
            Internal::Printf<FormatString>( stream, std::get<Indices>(std::forward<ArgTuple>(argTuple))... );
        }

        ArgTuple&& argTuple;
    };

    // Creation function for IO::Formatter, deducing the target type from the types of arguments.
    // The arguments provided should be wrapped using std::forward_as_tuple.
    // Example usage is: MakeFormatter<FormatStringStruct>(std::forward_as_tuple(1, "2", 3.5))
    template<typename FormatString, typename ArgTuple>
    Formatter<FormatString, ArgTuple> MakeFormatter( ArgTuple&& t )
    {
        return Formatter<FormatString, ArgTuple>( std::forward<ArgTuple>(t) );
    }
}
}

// We only want to accept rvalue Formatter objects because they are created
// using std::forward_as_tuple, so it's an error to use them past the next sequence point.
// They can still be std::move'd but it reduces the chance of accidently doing something wrong.
template<typename FormatString, typename ArgTuple>
std::ostream& operator<<( std::ostream& stream, L3::IO::Formatter<FormatString, ArgTuple>&& p )
{
    // http://stackoverflow.com/a/10767451
    // Need to forward it to the member function so that the indices can be inferred.
    p.Print( stream, typename MakeIndexSequence<std::tuple_size<ArgTuple>::value>::Sequence() );

    return stream;
}

/**
 *
 */
#define PasteFormatStringStructDefinition( StructName, formatStr )          \
    struct StructName                                                       \
    {                                                                       \
        /* TODO: Remove trailing return type when C++14 is available. */    \
        constexpr static auto Str() -> decltype(formatStr)                  \
        {                                                                   \
            return formatStr;                                               \
        }                                                                   \
    };
