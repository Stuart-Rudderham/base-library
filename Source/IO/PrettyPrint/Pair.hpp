#pragma once

/**
 * @file       Source/IO/PrettyPrint/Pair.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       March 18, 2014
 * @date       Last Modified: June 8, 2014
 * @brief      Implementation for pretty-printing an std::pair.
 */

#include "IO/PrettyPrint/GenericContainer.hpp"      // L3::IO::PrettyPrint::FormatOptions
#include <iostream>                                 // std::ostream
#include <utility>                                  // std::pair

namespace L3
{
namespace IO
{
    namespace PrettyPrint
    {
        /**
         * @brief The std::pair is printed with the format (1, 2).
         * @todo  Add \c constexpr and replace with in-class initialization when MSVC supports it.
         */
        template<typename T1, typename T2>
        struct FormatOptions<std::pair<T1, T2>>
        {
            /*constexpr*/ static const char* Open;
            /*constexpr*/ static const char* Close;
            /*constexpr*/ static const char* Delim;
        };

        template<typename T1, typename T2> const char* FormatOptions<std::pair<T1, T2>>::Open  = "(";
        template<typename T1, typename T2> const char* FormatOptions<std::pair<T1, T2>>::Close = ")";
        template<typename T1, typename T2> const char* FormatOptions<std::pair<T1, T2>>::Delim = ", ";
    }
}
}

/**
 * @brief Pretty-print an std::pair to an std::ostream.
 *
 * @todo  Not sure things are forwarded correctly.
 *
 * @param stream    The std::ostream to be printed to.
 * @param p         The std::pair to be printed.
 */
template<typename T1, typename T2>
std::ostream& operator<<( std::ostream& stream, const std::pair<T1, T2>& p )
{
    stream << L3::IO::PrettyPrint::FormatOptions<std::pair<T1, T2>>::Open;
    stream << p.first;
    stream << L3::IO::PrettyPrint::FormatOptions<std::pair<T1, T2>>::Delim;
    stream << p.second;
    stream << L3::IO::PrettyPrint::FormatOptions<std::pair<T1, T2>>::Close;

    return stream;
}
