#pragma once

/**
 * @file       Source/IO/PrettyPrint/InitializerList.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       March 18, 2014
 * @date       Last Modified: June 8, 2014
 * @brief      Implementation for pretty-printing an std::initializer_list.
 */

#include "IO/PrettyPrint/GenericContainer.hpp"      // IO::PrettyPrint::PrintGenericContainer
#include <ostream>                                  // std::ostream
#include <initializer_list>                         // std::initializer_list

namespace L3
{
namespace IO
{
    namespace PrettyPrint
    {
        /**
         * @brief The std::initializer_list is printed with the format {1, 2, 3, ...}.
         * @todo  Add \c constexpr and replace with in-class initialization when MSVC supports it.
         */
        template<typename T>
        struct FormatOptions<std::initializer_list<T>>
        {
            /*constexpr*/ static const char* Open;
            /*constexpr*/ static const char* Close;
            /*constexpr*/ static const char* Delim;
        };

        template<typename T> const char* FormatOptions<std::initializer_list<T>>::Open  = "{";
        template<typename T> const char* FormatOptions<std::initializer_list<T>>::Close = "}";
        template<typename T> const char* FormatOptions<std::initializer_list<T>>::Delim = ", ";
    }
}
}

/**
 * @brief Pretty-print an std::initializer_list to an std::ostream.
 *
 * @param stream    The std::ostream to be printed to.
 * @param il        The std::initializer_list to be printed.
 */
template<typename T>
std::ostream& operator<<( std::ostream& stream, const std::initializer_list<T>& il )
{
    return L3::IO::PrettyPrint::PrintGenericContainer<T>( stream, il );
}
