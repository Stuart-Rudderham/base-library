#pragma once

/**
 * @file       Source/IO/PrettyPrint/Set.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       March 18, 2014
 * @date       Last Modified: June 8, 2014
 * @brief      Implementation for pretty-printing an std::set or std::multiset.
 */

#include "IO/PrettyPrint/GenericContainer.hpp"      // IO::PrettyPrint::PrintGenericContainer
#include <ostream>                                  // std::ostream
#include <set>                                      // std::set, std::multiset

namespace L3
{
namespace IO
{
    namespace PrettyPrint
    {
        /**
         * @brief The std::set is printed with the format {1, 2, 3, ...}.
         * @todo  Add \c constexpr and replace with in-class initialization when MSVC supports it.
         * @todo  Modify unit test to check that custom allocators work correctly.
         */
        template<typename T, typename Compare, typename Alloc>
        struct FormatOptions<std::set<T, Compare, Alloc>>
        {
            /*constexpr*/ static const char* Open;
            /*constexpr*/ static const char* Close;
            /*constexpr*/ static const char* Delim;
        };

        template<typename Key, typename Compare, typename Alloc> const char* FormatOptions<std::set<Key, Compare, Alloc>>::Open  = "{";
        template<typename Key, typename Compare, typename Alloc> const char* FormatOptions<std::set<Key, Compare, Alloc>>::Close = "}";
        template<typename Key, typename Compare, typename Alloc> const char* FormatOptions<std::set<Key, Compare, Alloc>>::Delim = ", ";

        /**
         * @brief The std::multiset is printed with the format {1, 2, 3, ...}.
         * @todo  Add \c constexpr and replace with in-class initialization when MSVC supports it.
         * @todo  Modify unit test to check that custom allocators work correctly.
         */
        template<typename T, typename Compare, typename Alloc>
        struct FormatOptions<std::multiset<T, Compare, Alloc>>
        {
            /*constexpr*/ static const char* Open;
            /*constexpr*/ static const char* Close;
            /*constexpr*/ static const char* Delim;
        };

        template<typename Key, typename Compare, typename Alloc> const char* FormatOptions<std::multiset<Key, Compare, Alloc>>::Open  = "{";
        template<typename Key, typename Compare, typename Alloc> const char* FormatOptions<std::multiset<Key, Compare, Alloc>>::Close = "}";
        template<typename Key, typename Compare, typename Alloc> const char* FormatOptions<std::multiset<Key, Compare, Alloc>>::Delim = ", ";
    }
}
}

/**
 * @brief Pretty-print an std::set to an std::ostream.
 *
 * @param stream    The std::ostream to be printed to.
 * @param s         The std::set to be printed.
 */
template<typename T, typename Compare, typename Alloc>
std::ostream& operator<<( std::ostream& stream, const std::set<T, Compare, Alloc>& s )
{
    return L3::IO::PrettyPrint::PrintGenericContainer<T>( stream, s );
}

/**
 * @brief Pretty-print an std::multiset to an std::ostream.
 *
 * @param stream    The std::ostream to be printed to.
 * @param ms        The std::multiset to be printed.
 */
template<typename T, typename Compare, typename Alloc>
std::ostream& operator<<( std::ostream& stream, const std::multiset<T, Compare, Alloc>& ms )
{
    return L3::IO::PrettyPrint::PrintGenericContainer<T>( stream, ms );
}
