#pragma once

/**
 * @file       Source/IO/PrettyPrint/UnorderedSet.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       March 18, 2014
 * @date       Last Modified: June 8, 2014
 * @brief      Implementation for pretty-printing an std::unordered_set or std::unordered_multiset.
 */

#include "IO/PrettyPrint/GenericContainer.hpp"      // IO::PrettyPrint::PrintGenericContainer
#include <ostream>                                  // std::ostream
#include <unordered_set>                            // std::unordered_set, std::unordered_multiset

namespace L3
{
namespace IO
{
    namespace PrettyPrint
    {
        /**
         * @brief The std::unordered_set is printed with the format {1, 2, 3, ...}.
         * @todo  Add \c constexpr and replace with in-class initialization when MSVC supports it.
         * @todo  Modify unit test to check that custom allocators work correctly.
         */
        template<typename Key, typename Hash, typename Pred, typename Alloc>
        struct FormatOptions<std::unordered_set<Key, Hash, Pred, Alloc>>
        {
            /*constexpr*/ static const char* Open;
            /*constexpr*/ static const char* Close;
            /*constexpr*/ static const char* Delim;
        };

        template<typename Key, typename Hash, typename Pred, typename Alloc> const char* FormatOptions<std::unordered_set<Key, Hash, Pred, Alloc>>::Open  = "{";
        template<typename Key, typename Hash, typename Pred, typename Alloc> const char* FormatOptions<std::unordered_set<Key, Hash, Pred, Alloc>>::Close = "}";
        template<typename Key, typename Hash, typename Pred, typename Alloc> const char* FormatOptions<std::unordered_set<Key, Hash, Pred, Alloc>>::Delim = ", ";

        /**
         * @brief The std::unordered_multiset is printed with the format {1, 2, 3, ...}.
         * @todo  Add \c constexpr and replace with in-class initialization when MSVC supports it.
         * @todo  Modify unit test to check that custom allocators work correctly.
         */
        template<typename Key, typename Hash, typename Pred, typename Alloc>
        struct FormatOptions<std::unordered_multiset<Key, Hash, Pred, Alloc>>
        {
            /*constexpr*/ static const char* Open;
            /*constexpr*/ static const char* Close;
            /*constexpr*/ static const char* Delim;
        };

        template<typename Key, typename Hash, typename Pred, typename Alloc> const char* FormatOptions<std::unordered_multiset<Key, Hash, Pred, Alloc>>::Open  = "{";
        template<typename Key, typename Hash, typename Pred, typename Alloc> const char* FormatOptions<std::unordered_multiset<Key, Hash, Pred, Alloc>>::Close = "}";
        template<typename Key, typename Hash, typename Pred, typename Alloc> const char* FormatOptions<std::unordered_multiset<Key, Hash, Pred, Alloc>>::Delim = ", ";
    }
}
}

/**
 * @brief Pretty-print an std::unordered_set to an std::ostream.
 *
 * @param stream    The std::ostream to be printed to.
 * @param us        The std::unordered_set to be printed.
 */
template<typename Key, typename Hash, typename Pred, typename Alloc>
std::ostream& operator<<( std::ostream& stream, const std::unordered_set<Key, Hash, Pred, Alloc>& us )
{
    return L3::IO::PrettyPrint::PrintGenericContainer<Key>( stream, us );
}

/**
 * @brief Pretty-print an std::unordered_multiset to an std::ostream.
 *
 * @param stream    The std::ostream to be printed to.
 * @param ums       The std::unordered_multiset to be printed.
 */
template<typename Key, typename Hash, typename Pred, typename Alloc>
std::ostream& operator<<( std::ostream& stream, const std::unordered_multiset<Key, Hash, Pred, Alloc>& ums )
{
    return L3::IO::PrettyPrint::PrintGenericContainer<Key>( stream, ums );
}
