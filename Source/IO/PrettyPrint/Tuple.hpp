#pragma once

/**
 * @file       Source/IO/PrettyPrint/Tuple.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       May 14, 2014
 * @date       Last Modified: June 30, 2014
 * @brief      Implementation for pretty-printing an std::tuple.
 */

#include "IO/PrettyPrint/GenericContainer.hpp"      // L3::IO::PrettyPrint::FormatOptions
#include "Metaprogramming/IndexSequence.hpp"        // MakeIndexSequence, IndexSequence
#include <iostream>                                 // std::ostream
#include <tuple>                                    // std::tuple

namespace L3
{
namespace IO
{
    namespace PrettyPrint
    {
        /**
         * @brief The std::tuple is printed with the format (1, 2, 3, ...).
         * @todo  Add \c constexpr and replace with in-class initialization when MSVC supports it.
         */
        template<typename... T>
        struct FormatOptions<std::tuple<T...>>
        {
            /*constexpr*/ static const char* Open;
            /*constexpr*/ static const char* Close;
            /*constexpr*/ static const char* Delim;
        };

        template<typename... T> const char* FormatOptions<std::tuple<T...>>::Open  = "(";
        template<typename... T> const char* FormatOptions<std::tuple<T...>>::Close = ")";
        template<typename... T> const char* FormatOptions<std::tuple<T...>>::Delim = ", ";

        namespace Internal
        {
            /**
             * @brief   Base case for PrintTuple.
             *
             * @details Called when there are no arguments left to print. Does nothing.
             */
            void PrintTupleImpl( std::ostream& )
            {
                // Nothing to do.
            }

            /**
             * @brief   Base case for PrintTuple.
             *
             * @details Called when there are is only one argument left to print.
             */
            template<typename T>
            void PrintTupleImpl( std::ostream& stream, T&& value )
            {
                // Print the only argument.
                stream << std::forward<T>(value);
            }

            /**
             * @brief Recursive case for PrintTuple.
             *
             * @details Called when there are one or more arguments left to print.
             *          Prints the first argument and then recurses on the rest.
             */
            template<typename T, typename... Args>
            void PrintTupleImpl( std::ostream& stream, T&& value, Args&&... args )
            {
                // Print first argument.
                stream << std::forward<T>(value);

                // Print delimiter.
                stream << L3::IO::PrettyPrint::FormatOptions<std::tuple<T, Args...>>::Delim;

                // Recurse on rest of arguments.
                PrintTupleImpl( stream, std::forward<Args>(args)... );
            }
        }

        /**
         * @brief A function for printing an std::tuple.
         *
         * @todo Make documentation better.
         */
        template<typename... Args, unsigned int... Indices>
        void PrintTuple( std::ostream& stream, const std::tuple<Args...>& t, IndexSequence<Indices...> )
        {
            // When t is the empty tuple (and thus Indices is empty) MSVC complains
            // that t is not being used. This suppresses that warning.
            (void)t;

            Internal::PrintTupleImpl( stream, std::get<Indices>(t)... );
        }
    }
}
}

/**
 * @brief Pretty-print an std::tuple to an std::ostream.
 *
 * @todo  Try and replace explicit IndexSequence stuff with a generic
 *        std::tuple -> variadic template calling framework (i.e. the
 *        opposite of std::forward_to_tuple).
 *
 * @todo  Not sure things are forwarded correctly.
 *
 * @param stream    The std::ostream to be printed to.
 * @param t         The std::tuple to be printed.
 */
template<typename... Args>
std::ostream& operator<<( std::ostream& stream, const std::tuple<Args...>& t )
{
    // Print opening delimiter.
    stream << L3::IO::PrettyPrint::FormatOptions<std::tuple<Args...>>::Open;

    // Try and make this possible!
    //TupleCall( L3::IO::PrettyPrint::PrintTuple<Args...>( stream, t ) );

    // Forward to the actual printing function with an IndexSequence for tuple unpacking.
    L3::IO::PrettyPrint::PrintTuple( stream, t, typename MakeIndexSequence<sizeof...(Args)>::Sequence());

    // Print closing delimiter.
    stream << L3::IO::PrettyPrint::FormatOptions<std::tuple<Args...>>::Close;

    return stream;
}
