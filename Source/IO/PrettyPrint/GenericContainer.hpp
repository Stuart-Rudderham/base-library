#pragma once

/**
 * @file       Source/IO/PrettyPrint/GenericContainer.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       March 18, 2014
 * @date       Last Modified: June 8, 2014
 * @brief      Definition & implementation for pretty-printing any STL container.
 * @details    Based off of a similar library found <a href="http://louisdx.github.io/cxx-prettyprint/">here</a>
 */

#include <ostream>          // std::ostream
#include <iterator>         // std::begin, std::end, std::next

namespace L3
{
namespace IO
{
    namespace PrettyPrint
    {
        /**
         * @brief   A struct that maintains the opening, closing, and delimiting strings when
         *          printing an object.
         *
         * @details It is specialized for each type, so an std::vector is
         *          printed [1, 2, 3] while an std::set is printed {1, 2, 3}.
         *
         * @todo    Add \c constexpr and replace with in-class initialization when MSVC supports it.
         */
        template<typename Container>
        struct FormatOptions
        {
            /*constexpr*/ static const char* Open;
            /*constexpr*/ static const char* Close;
            /*constexpr*/ static const char* Delim;
        };

        template<typename Container> const char* FormatOptions<Container>::Open  = "|";
        template<typename Container> const char* FormatOptions<Container>::Close = "|";
        template<typename Container> const char* FormatOptions<Container>::Delim = " ";

        /**
         * @brief Pretty-print an object that supports \c std::begin, \c std::end, \c std::next.
         *
         * @param stream    The std::ostream to be printed to.
         * @param container The object to be printed.
         */
        template<typename T, typename Container>
        std::ostream& PrintGenericContainer( std::ostream& stream, const Container& container )
        {
            stream << FormatOptions<Container>::Open;

            /// @todo Replace with std::cbegin and std::cend when C++14 is available.
            auto begin = std::begin(container);
            auto end   = std::end  (container);

            if( begin != end )
            {
                stream << *begin;

                for( auto i = std::next(begin); i != end; ++i )
                {
                    stream << FormatOptions<Container>::Delim << *i;
                }

                // Faster version but requires the container have BidirectionalIterators (for std::prev)
                /*
                std::copy(
                    container.cbegin()                          ,
                    std::prev( container.cend() )               ,
                    std::ostream_iterator<T>( stream, delim )
                );

                stream << *container.crend();
                */
            }

            stream << FormatOptions<Container>::Close;

            return stream;
        }
    }
}
}
