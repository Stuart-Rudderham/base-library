#pragma once

/**
 * @file       Source/IO/PrettyPrint/Vector.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       March 3, 2014
 * @date       Last Modified: June 8, 2014
 * @brief      Implementation for pretty-printing an std::vector.
 */

#include "IO/PrettyPrint/GenericContainer.hpp"      // IO::PrettyPrint::PrintGenericContainer
#include <ostream>                                  // std::ostream
#include <vector>                                   // std::vector

namespace L3
{
namespace IO
{
    namespace PrettyPrint
    {
        /**
         * @brief The std::vector is printed with the format [1, 2, 3, ...].
         * @todo  Add \c constexpr and replace with in-class initialization when MSVC supports it.
         * @todo  Modify unit test to check that custom allocators work correctly.
         */
        template<typename T, typename Alloc>
        struct FormatOptions<std::vector<T, Alloc>>
        {
            /*constexpr*/ static const char* Open;
            /*constexpr*/ static const char* Close;
            /*constexpr*/ static const char* Delim;
        };

        template<typename T, typename Alloc> const char* FormatOptions<std::vector<T, Alloc>>::Open  = "[";
        template<typename T, typename Alloc> const char* FormatOptions<std::vector<T, Alloc>>::Close = "]";
        template<typename T, typename Alloc> const char* FormatOptions<std::vector<T, Alloc>>::Delim = ", ";
    }
}
}

/**
 * @brief Pretty-print an std::vector to an std::ostream.
 *
 * @param stream    The std::ostream to be printed to.
 * @param v         The std::vector to be printed.
 */
template<typename T, typename Alloc>
std::ostream& operator<<( std::ostream& stream, const std::vector<T, Alloc>& v )
{
    return L3::IO::PrettyPrint::PrintGenericContainer<T>( stream, v );
}
