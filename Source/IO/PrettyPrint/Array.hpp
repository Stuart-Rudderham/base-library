#pragma once

/**
 * @file       Source/IO/PrettyPrint/Array.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       March 18, 2014
 * @date       Last Modified: June 8, 2014
 * @brief      Implementation for pretty-printing an std::array.
 */

#include "IO/PrettyPrint/GenericContainer.hpp"      // IO::PrettyPrint::PrintGenericContainer
#include <ostream>                                  // std::ostream
#include <array>                                    // std::array
#include <cstddef>                                  // std::size_t

namespace L3
{
namespace IO
{
    namespace PrettyPrint
    {
        /**
         * @brief The std::array is printed with the format [1, 2, 3, ...].
         * @todo  Add \c constexpr and replace with in-class initialization when MSVC supports it.
         */
        template<typename T, std::size_t N>
        struct FormatOptions<std::array<T, N>>
        {
            /*constexpr*/ static const char* Open;
            /*constexpr*/ static const char* Close;
            /*constexpr*/ static const char* Delim;
        };

        template<typename T, std::size_t N> const char* FormatOptions<std::array<T, N>>::Open  = "[";
        template<typename T, std::size_t N> const char* FormatOptions<std::array<T, N>>::Close = "]";
        template<typename T, std::size_t N> const char* FormatOptions<std::array<T, N>>::Delim = ", ";
    }
}
}

/**
 * @brief Pretty-print an std::array to an std::ostream.
 *
 * @param stream    The std::ostream to be printed to.
 * @param a         The std::array to be printed.
 */
template<typename T, std::size_t N>
std::ostream& operator<<( std::ostream& stream, const std::array<T, N>& a )
{
    return L3::IO::PrettyPrint::PrintGenericContainer<T>( stream, a );
}
