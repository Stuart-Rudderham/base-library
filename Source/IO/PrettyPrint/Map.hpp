#pragma once

/**
 * @file       Source/IO/PrettyPrint/Map.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       March 18, 2014
 * @date       Last Modified: June 8, 2014
 * @brief      Implementation for pretty-printing an std::map or std::multimap.
 */

#include "IO/PrettyPrint/GenericContainer.hpp"      // IO::PrettyPrint::PrintGenericContainer
#include "IO/PrettyPrint/Pair.hpp"                  // operator<<( std::ostream&, const std::pair<T1, T2>& )
#include <ostream>                                  // std::ostream
#include <map>                                      // std::map, std::multimap

namespace L3
{
namespace IO
{
    namespace PrettyPrint
    {
        /**
         * @brief The std::map is printed with the format {(1, "1"), (2, "2"), ...}.
         * @todo  Add \c constexpr and replace with in-class initialization when MSVC supports it.
         * @todo  Modify unit test to check that custom allocators work correctly.
         */
        template<typename Key, typename T, typename Compare, typename Alloc>
        struct FormatOptions<std::map<Key, T, Compare, Alloc>>
        {
            /*constexpr*/ static const char* Open;
            /*constexpr*/ static const char* Close;
            /*constexpr*/ static const char* Delim;
        };

        template<typename Key, typename T, typename Compare, typename Alloc> const char* FormatOptions<std::map<Key, T, Compare, Alloc>>::Open  = "{";
        template<typename Key, typename T, typename Compare, typename Alloc> const char* FormatOptions<std::map<Key, T, Compare, Alloc>>::Close = "}";
        template<typename Key, typename T, typename Compare, typename Alloc> const char* FormatOptions<std::map<Key, T, Compare, Alloc>>::Delim = ", ";

        /**
         * @brief The std::multimap is printed with the format {(1, "1"), (2, "2"), ...}.
         * @todo  Add \c constexpr and replace with in-class initialization when MSVC supports it.
         * @todo  Modify unit test to check that custom allocators work correctly.
         */
        template<typename Key, typename T, typename Compare, typename Alloc>
        struct FormatOptions<std::multimap<Key, T, Compare, Alloc>>
        {
            /*constexpr*/ static const char* Open;
            /*constexpr*/ static const char* Close;
            /*constexpr*/ static const char* Delim;
        };

        template<typename Key, typename T, typename Compare, typename Alloc> const char* FormatOptions<std::multimap<Key, T, Compare, Alloc>>::Open  = "{";
        template<typename Key, typename T, typename Compare, typename Alloc> const char* FormatOptions<std::multimap<Key, T, Compare, Alloc>>::Close = "}";
        template<typename Key, typename T, typename Compare, typename Alloc> const char* FormatOptions<std::multimap<Key, T, Compare, Alloc>>::Delim = ", ";

    }
}
}

/**
 * @brief Pretty-print an std::map to an std::ostream.
 *
 * @param stream    The std::ostream to be printed to.
 * @param m         The std::map to be printed.
 */
template<typename Key, typename T, typename Compare, typename Alloc>
std::ostream& operator<<( std::ostream& stream, const std::map<Key, T, Compare, Alloc>& m )
{
    using MapEntryType = typename std::map<Key, T, Compare, Alloc>::value_type;

    return L3::IO::PrettyPrint::PrintGenericContainer<MapEntryType>( stream, m );
}

/**
 * @brief Pretty-print an std::multimap to an std::ostream.
 *
 * @param stream    The std::ostream to be printed to.
 * @param mm        The std::multimap to be printed.
 */
template<typename Key, typename T, typename Compare, typename Alloc>
std::ostream& operator<<( std::ostream& stream, const std::multimap<Key, T, Compare, Alloc>& mm )
{
    using MapEntryType = typename std::multimap<Key, T, Compare, Alloc>::value_type;

    return L3::IO::PrettyPrint::PrintGenericContainer<MapEntryType>( stream, mm );
}
