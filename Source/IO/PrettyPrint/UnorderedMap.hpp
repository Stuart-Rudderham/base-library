#pragma once

/**
 * @file       Source/IO/PrettyPrint/UnorderedMap.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       March 18, 2014
 * @date       Last Modified: June 8, 2014
 * @brief      Implementation for pretty-printing an std::unordered_map or std::unordered_multimap.
 */

#include "IO/PrettyPrint/GenericContainer.hpp"      // IO::PrettyPrint::PrintGenericContainer
#include "IO/PrettyPrint/Pair.hpp"                  // operator<<( std::ostream&, const std::pair<T1, T2>& )
#include <ostream>                                  // std::ostream
#include <unordered_map>                            // std::unordered_map, std::unordered_multimap

namespace L3
{
namespace IO
{
    namespace PrettyPrint
    {
        /**
         * @brief The std::unordered_map is printed with the format {(1, "1"), (2, "2"), ...}.
         * @todo  Add \c constexpr and replace with in-class initialization when MSVC supports it.
         * @todo  Modify unit test to check that custom allocators work correctly.
         */
        template<typename Key, typename T, typename Hash, typename Pred, typename Alloc>
        struct FormatOptions<std::unordered_map<Key, T, Hash, Pred, Alloc>>
        {
            /*constexpr*/ static const char* Open;
            /*constexpr*/ static const char* Close;
            /*constexpr*/ static const char* Delim;
        };

        template<typename Key, typename T, typename Hash, typename Pred, typename Alloc> const char* FormatOptions<std::unordered_map<Key, T, Hash, Pred, Alloc>>::Open  = "{";
        template<typename Key, typename T, typename Hash, typename Pred, typename Alloc> const char* FormatOptions<std::unordered_map<Key, T, Hash, Pred, Alloc>>::Close = "}";
        template<typename Key, typename T, typename Hash, typename Pred, typename Alloc> const char* FormatOptions<std::unordered_map<Key, T, Hash, Pred, Alloc>>::Delim = ", ";

        /**
         * @brief The std::unordered_multimap is printed with the format {(1, "1"), (2, "2"), ...}.
         * @todo  Add \c constexpr and replace with in-class initialization when MSVC supports it.
         * @todo  Modify unit test to check that custom allocators work correctly.
         */
        template<typename Key, typename T, typename Hash, typename Pred, typename Alloc>
        struct FormatOptions<std::unordered_multimap<Key, T, Hash, Pred, Alloc>>
        {
            /*constexpr*/ static const char* Open;
            /*constexpr*/ static const char* Close;
            /*constexpr*/ static const char* Delim;
        };

        template<typename Key, typename T, typename Hash, typename Pred, typename Alloc> const char* FormatOptions<std::unordered_multimap<Key, T, Hash, Pred, Alloc>>::Open  = "{";
        template<typename Key, typename T, typename Hash, typename Pred, typename Alloc> const char* FormatOptions<std::unordered_multimap<Key, T, Hash, Pred, Alloc>>::Close = "}";
        template<typename Key, typename T, typename Hash, typename Pred, typename Alloc> const char* FormatOptions<std::unordered_multimap<Key, T, Hash, Pred, Alloc>>::Delim = ", ";
    }
}
}

/**
 * @brief Pretty-print an std::unordered_map to an std::ostream.
 *
 * @param stream    The std::ostream to be printed to.
 * @param um        The std::unordered_map to be printed.
 */
template<typename Key, typename T, typename Hash, typename Pred, typename Alloc>
std::ostream& operator<<( std::ostream& stream, const std::unordered_map<Key, T, Hash, Pred, Alloc>& um )
{
    using MapEntryType = typename std::unordered_map<Key, T, Hash, Pred, Alloc>::value_type;

    return L3::IO::PrettyPrint::PrintGenericContainer<MapEntryType>( stream, um );
}

/**
 * @brief Pretty-print an std::unordered_multimap to an std::ostream.
 *
 * @param stream    The std::ostream to be printed to.
 * @param umm       The std::unordered_multimap to be printed.
 */
template<typename Key, typename T, typename Hash, typename Pred, typename Alloc>
std::ostream& operator<<( std::ostream& stream, const std::unordered_multimap<Key, T, Hash, Pred, Alloc>& umm )
{
    using MapEntryType = typename std::unordered_multimap<Key, T, Hash, Pred, Alloc>::value_type;

    return L3::IO::PrettyPrint::PrintGenericContainer<MapEntryType>( stream, umm );
}
