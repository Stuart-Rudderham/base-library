/*
 * This header defines functions to stringify the name of a type.
 *
 * One possible usecase could be getting the typename from a template parameter.
 *
 * We could use std::typeid but the string returned from that in compiler
 * dependent (and usually not very pretty for printing).
 *
 * Author:      Stuart Rudderham
 * Created:     September 14, 2013
 * Last Edited: November 20, 2013
 */

#ifndef TYPENAME_HPP_INCLUDED
#define TYPENAME_HPP_INCLUDED

#include <string>
#include <type_traits>
#include <typeinfo>

/*
 * All the helper functions are wrapped in the TypenameDetails namespace.
 * You shouldn't use or even look at any of these functions.
 */
namespace TypenameDetails
{
    /*
     * The templated functor that returns the string associated with the given type.
     * We use a functor rather than a free function because functors can be
     * partially-specialized while free functions cannot (for whatever reason...).
     *
     * The default implementation uses typeid(T).name() to return *something* that might
     * be useful, but it should be specialized to return correct strings for each desired
     * type.
     *
     * Two known problems with using typeid(T).name() are:
     *     1) it doesn't work with forward declared types.
     *     2) it ignores references (i.e. typeid(int&) == typeid(int) is true)
     */
    template<typename T>
    struct HelperBase
    {
        const std::string operator()()
        {
            //return "|UNKNOWN|";
            return typeid(T).name();
        }
    };

    /*
     * A helper function for handling arrays (e.g. int[1][2][3])
     */
    template<typename T>
    const std::string ArrayHelper()
    {
        // Base case, we've removed all the array dimensions.
        if( std::rank<T>::value == 0 ) {
            return std::string("");
        }
        // Recursive case, strip off the first array dimension and recurse.
        else {
            std::string str( "[" + std::to_string(std::extent<T>::value) + "]" );
            str += ArrayHelper< typename std::remove_extent<T>::type>();

            return str;
        }
    }

    /*
     * The function that does all the work creating the string representations.
     *
     * It recurses until it reaches a simple builtin C++ type (e.g. int, class, enum) with no attributes.
     * At each level an attribute (e.g. const, pointer, reference, etc...) is removed and an appropriate
     * string added (e.g. "*" for pointers).
     */
    template<typename T>
    const std::string Helper()
    {
        // Handle "const".
        if( std::is_const<T>::value ) {
            if( std::is_pointer<T>::value ) {
                return Helper< typename std::remove_const<T>::type>() + std::string(" const");
            }
            else {
                return std::string("const ") + Helper< typename std::remove_const<T>::type>();
            }
        }
        // Handle "volatile".
        else if( std::is_volatile<T>::value ) {
            if( std::is_pointer<T>::value ) {
                return Helper< typename std::remove_volatile<T>::type>() + std::string(" volatile");
            }
            else {
                return std::string("volatile ") + Helper< typename std::remove_volatile<T>::type>();
            }
        }
        // Handle pointers.
        // This has to be done after "const" and "volatile" because std::remove_pointer<T> will also remove
        // any pointer attributes (i.e. std::is_same< int, std::remove_pointer<int* const>::type > == true)
        else if( std::is_pointer<T>::value ) {
            return Helper< typename std::remove_pointer<T>::type>() + std::string("*");
        }
        // Handle lvalue references.
        else if( std::is_lvalue_reference<T>::value ) {
            return Helper< typename std::remove_reference<T>::type>() + std::string("&");
        }
        // Handle rvalue references.
        else if( std::is_rvalue_reference<T>::value ) {
            return Helper< typename std::remove_reference<T>::type >() + std::string("&&");
        }
        // Handle arrays (both single and multi-dimensional)
        else if( std::is_array<T>::value ) {
            return Helper<typename std::remove_all_extents<T>::type>() + ArrayHelper<T>();
        }
        // Handle enum and enum classes
        else if( std::is_enum<T>::value ) {
            return /*std::string( "enum " ) +*/ HelperBase<T>()();
        }
        // Handle classes and structs.
        // Unfortunately there is no way to differentiate between a class and a struct because
        // std::is_class<T> returns true for both and there is no std::is_struct<T>
        else if( std::is_class<T>::value ) {
            return /*std::string( "class " ) +*/ HelperBase<T>()();
        }
        // Base case.
        else {
            return HelperBase<T>()();
        }
    }
}

/*
 * The function used to get the string associated with the given type.
 *
 * The string is cached the first time the function is called so we only
 * calculate it once.
 */
template<typename T>
const std::string& Typename()
{
    static std::string str = TypenameDetails::Helper<T>();
    return str;
}

/*
 * A macro to easily create the functor that specifies the string
 * associated with the given type.
 */
#define SetTypenameString(Type)                                                \
    namespace TypenameDetails {                                                \
        template<>                                                             \
        struct HelperBase<Type>                                                \
        {                                                                      \
            const std::string operator()()                                     \
            {                                                                  \
                return std::string( #Type );                                   \
            }                                                                  \
        };                                                                     \
    }

/*
 * Macros specify the string for templated types. There's no automatic way to
 * do this for all the various ways templates can be used so macros have been
 * manually created for the common use cases.
 */

/*
 * Templates with one type parameter
 */
#define SetTypenameString_TemplateT(Type)                                      \
    namespace TypenameDetails {                                                \
        template<typename T>                                                   \
        struct HelperBase< Type<T> >                                           \
        {                                                                      \
            const std::string operator()()                                     \
            {                                                                  \
                return std::string( #Type )             +                      \
                       "<"                              +                      \
                           Typename<T>()                +                      \
                       ">";                                                    \
            }                                                                  \
        };                                                                     \
    }                                                                          \

/*
 * Templates with one unsigned int parameter
 */
#define SetTypenameString_TemplateUInt(Type)                                   \
    namespace TypenameDetails {                                                \
        template<unsigned int N>                                               \
        struct HelperBase< Type<N> >                                           \
        {                                                                      \
            const std::string operator()()                                     \
            {                                                                  \
                return std::string( #Type )             +                      \
                       "<"                              +                      \
                           std::to_string(N)            +                      \
                       ">";                                                    \
            }                                                                  \
        };                                                                     \
    }

/*
 * Templates with one type parameter and one unsigned int parameter
 */
#define SetTypenameString_TemplateTAndUInt(Type)                               \
    namespace TypenameDetails {                                                \
        template<typename T, unsigned int N>                                   \
        struct HelperBase< Type<T, N> >                                        \
        {                                                                      \
            const std::string operator()()                                     \
            {                                                                  \
                return std::string( #Type )             +                      \
                       "<"                              +                      \
                           Typename<T>()                +                      \
                           ", "                         +                      \
                           std::to_string(N)            +                      \
                       ">";                                                    \
            }                                                                  \
        };                                                                     \
    }

/*
 * Templates with one type parameter and templated type parameter
 */
#define SetTypenameString_TemplateTAndTemplateTemplateT(Type)                  \
    namespace TypenameDetails {                                                \
        template<typename T, template <typename> class Template>               \
        struct HelperBase< Type<T, Template> >                                 \
        {                                                                      \
            const std::string operator()()                                     \
            {                                                                  \
                return std::string( #Type )             +                      \
                       "<"                              +                      \
                           Typename<T>()                +                      \
                           ", "                         +                      \
                           Typename<Template<T>>()      +                      \
                       ">";                                                    \
            }                                                                  \
        };                                                                     \
    }

/*
 * All the builtin C++ types
 */
SetTypenameString( bool )

SetTypenameString( char )
SetTypenameString( unsigned char )
SetTypenameString( signed char )

SetTypenameString( short )
SetTypenameString( unsigned short )

SetTypenameString( int )
SetTypenameString( unsigned int )

SetTypenameString( long )
SetTypenameString( unsigned long )

SetTypenameString( long long )
SetTypenameString( unsigned long long )

SetTypenameString( float )
SetTypenameString( double )
SetTypenameString( long double )

SetTypenameString( wchar_t )

/*
 * Common STL containers.
 *
 * We have to include all the headers since we can't forward declare these types. Not
 * sure what kind of effect that will have on compile time (probably nothing good though).
 */
//#include <vector>
//StoreTemplateTypename( std::vector );



#endif // TYPENAME_HPP_INCLUDED
