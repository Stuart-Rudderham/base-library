/*
 * This file defines the interface for a class-level memory allocator.
 *
 * All objects of type T will be allocated and deallocated using the same
 * global Allocator.
 *
 * The actual implementation is a template parameter so it is possible
 * to "plug-and-play" with different implementations so long as they respect
 * the requirements of the Allocator.
 *
 * This Allocator class only handles memory addresses, not the construction
 * or destruction of objects, and is not an allocator for STL containers.
 *
 * Author:      Stuart Rudderham
 * Created:     November 20, 2013
 * Last Edited: June 7, 2014
 */

#include "IO/Print.hpp"         // ioPrintln, ioCout
#include "Macros/Macros.hpp"
#include "Debug/Crash.hpp"      // dbCrash
#include <cstring>              // memset
#include <sstream>              // std::ostringstream

/*
 * Constructor.
 */
template<typename T, template <typename> class Impl>
IAllocator<T, Impl>::IAllocator()
{
    // TODO: Add static_asserts checking that the implementation has the member
    // functions we will use. This will give a nice error message rather than
    // pages of template errors.
    //
    // Functions are:
    //     T* GetAddress();
    //     void ReturnAddress( T* );
    //
    //     unsigned int MaxAllocated() const;
    //     unsigned int CurAllocated() const;
    //
    //     std::string GetStatusString() const;
}

/*
 * Destructor.
 */
template<typename T, template <typename> class Impl>
IAllocator<T, Impl>::~IAllocator()
{
    // Print out a summary.
    ioCout( "\n", GetStatusString() );

    // Make sure we don't have a memory leak.
    if( CurAllocated() != 0 ) {
        dbCrash(  "Leaking memory in ", Typename<IAllocator<T, Impl>>() );
    }
}

/*
 * Returns a memory address where an object of type T can be constructed.
 */
template<typename T, template <typename> class Impl>
T* IAllocator<T, Impl>::Allocate( const L3::Debug::Location& loc )
{
    // Get an address from the implementation.
    T* address = impl.GetAddress();

    // Sanity check. This will assert if the address is already allocated.
    #if ENABLE_EXISTENCE_TRACKING
        allocatedAddresses.Add( address );
    #endif

    // Set the object's memory to an invalid bit-pattern, if desired.
    //
    // Cast to void* to suppress compiler warnings, since in this case
    // we do actually want to overwrite the vtable pointer (among other
    // thing).
    #if ENABLE_MEMORY_CLEARING
        memset( static_cast<void*>(address), 0xCD, sizeof(T) );
    #endif

    // Store the file location where the allocation is occuring.
    #if ENABLE_FILE_LOCATION_TRACKING
        allocationFileLocations.Add( address, loc );
    #else
        // Supress compiler warnings.
        (void)loc;
    #endif

    return address;
}

/*
 * Return the given address back to the Allocator.
 */
template<typename T, template <typename> class Impl>
void IAllocator<T, Impl>::Deallocate( T* address )
{
    // Nothing to do if nullptr.
    if( address == nullptr ) {
        return;
    }

    // Sanity check. This will assert if the address isn't currently allocated.
    #if ENABLE_EXISTENCE_TRACKING
        allocatedAddresses.Remove( address );
    #endif

    // Set the object's memory to an invalid bit-pattern, if desired.
    //
    // This must be done before returning the address to the memory system
    // since writing to the memory after returning it is undefined behavior.
    //
    // Cast to void* to suppress compiler warnings, since in this case
    // we do actually want to overwrite the vtable pointer (among other
    // thing).
    #if ENABLE_MEMORY_CLEARING
        memset( static_cast<void*>(address), 0xCD, sizeof(T) );
    #endif

    // Return the address to the implementation.
    impl.ReturnAddress( address );

    // Discard the file location since the address is no longer allocated.
    #if ENABLE_FILE_LOCATION_TRACKING
        allocationFileLocations.Remove( address );
    #endif
}

/*
 * Returns the highest number of simultaneously allocated addresses
 * (the high-water mark).
 */
template<typename T, template <typename> class Impl>
unsigned int IAllocator<T, Impl>::MaxAllocated() const
{
    // Delegate to the implementation.
    return impl.MaxAllocated();
}

/*
 * Returns the current number of allocated addresses.
 */
template<typename T, template <typename> class Impl>
unsigned int IAllocator<T, Impl>::CurAllocated() const
{
    // Delegate to the implementation.
    return impl.CurAllocated();
}

/*
 * Get a summary of the current status of the Allocator.
 */
template<typename T, template <typename> class Impl>
std::string IAllocator<T, Impl>::GetStatusString() const
{
    std::ostringstream stream;

    // TODO: Since we use ioPrintln to construct the implementation's
    // status string there are two newlines added. Change ioPrintln to not
    // add the newline to the end, and change the dbPrintToConsole macro to add the
    // newline.
    ioPrintln( stream, Typename<IAllocator<T, Impl>>(), " Summary"            );
    ioPrintln( stream, "    Object Size (Bytes)           : ", sizeof(T)      );
    ioPrintln( stream, impl.GetStatusString()                                 );
    ioPrintln( stream, "    Max Allocated Addresses       : ", MaxAllocated() );
    ioPrintln( stream, "    Currently Allocated Addresses : ", CurAllocated() );

    #if ENABLE_FILE_LOCATION_TRACKING
        auto v = allocationFileLocations.GetContents();
        for( const auto& i : v ) {
            ioPrintln( stream, ""                              );
            ioPrintln( stream, "        Address  : ", i.first  );
            ioPrintln( stream, "        Location : ", i.second );
        }
    #endif

    return stream.str();
}
