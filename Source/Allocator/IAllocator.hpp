/*
 * This header defines the interface for a class-level memory allocator.
 *
 * All objects of type T will be allocated and deallocated using the same
 * global Allocator.
 *
 * The actual implementation is a template parameter so it is possible
 * to "plug-and-play" with different implementations so long as they respect
 * the requirements of the Allocator.
 *
 * This Allocator class only handles memory addresses, not the construction
 * or destruction of objects, and is not an allocator for STL containers.
 *
 * Author:      Stuart Rudderham
 * Created:     November 20, 2013
 * Last Edited: June 7, 2014
 */

#ifndef IALLOCATOR_HPP_INCLUDED
#define IALLOCATOR_HPP_INCLUDED

#include "Singleton.hpp"
#include "Debug/Location.hpp"
#include "Debug/LocationMap.hpp"
#include "Debug/ExistenceSet.hpp"
#include "Typename.hpp"
#include <string>

template<typename T, template <typename> class Impl>
class IAllocator : public Singleton< IAllocator<T, Impl> >
{
    public:
        IAllocator();                                       // Constructor.

        ~IAllocator();                                      // Destructor. Checks for memory leaks.

        T* Allocate( const L3::Debug::Location& loc );      // Returns a memory address where an object of type T
                                                            // can be constructed. Does *not* call the constructor.

        void Deallocate( T* address );                      // Return the given address back to the Allocator.
                                                            // Does *not* call the destructor.

        unsigned int MaxAllocated() const;                  // Returns the highest number of simultaneously
                                                            // allocated addresses (the high-water mark).

        unsigned int CurAllocated() const;                  // Returns the current number of allocated addresses.

        std::string GetStatusString() const;                // Get a summary of the current status of the Allocator.

    private:
        Impl<T> impl;                                       // The implementation object.

        // The two variables below are used for debugging purposes.
        // The core Allocator functionality does not depend on
        // them (since they are disabled in Release mode).
        //
        // They have some overlapping functionality (e.g. they both
        // keep track of all the allocated addresses) but this is
        // necessary as each can be enabled/disabled independently
        // of the other.

        #if ENABLE_FILE_LOCATION_TRACKING
            Debug::LocationMap<T> allocationFileLocations;  // A map of all the addresses that are currently allocated.
                                                            // Each address is associated with the file location where
                                                            // it was allocated.
        #endif

        #if ENABLE_EXISTENCE_TRACKING
            Debug::ExistenceSet<T> allocatedAddresses;      // A set of all the addresses that are currently allocated.
                                                            // It is used to do O(1) sanity checks since the underlying
                                                            // implementation doesn't have any performance requirements.
        #endif
};

/*
 * Set the typename string.
 */
SetTypenameString_TemplateTAndTemplateTemplateT( IAllocator );

/*
 * Pull in the implementation file as well, so we don't get linker errors when
 * only including the header file.
 */
#include "Allocator/IAllocator.inl"

/*
 * Also pull in some wrapper macros, since you're pretty much always going to use
 * them rather than the Allocator directly.
 */
#include "Macros/MemoryAllocationMacros.hpp"

#endif // IALLOCATOR_HPP_INCLUDED
