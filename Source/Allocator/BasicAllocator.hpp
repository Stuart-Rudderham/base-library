/*
 * This header defines an implementation for a class-level memory allocator.
 *
 * It is a reference implementation that simply uses the regular "::operator new"
 * and "::operator delete" functions. There is nothing special about it other
 * than the debugging functionality that the IAllocator interface provides.
 *
 * Author:      Stuart Rudderham
 * Created:     September 14, 2013
 * Last Edited: November 20, 2013
 */

#ifndef BASICALLOCATOR_HPP_INCLUDED
#define BASICALLOCATOR_HPP_INCLUDED

#include "Typename.hpp"
#include <string>

template<typename T>
class BasicAllocator
{
    public:
        BasicAllocator();                               // Constructor.
        ~BasicAllocator();                              // Destructor. Checks for memory leaks.

        T* GetAddress();                                // Returns a memory address where an object of
                                                        // type T can be constructed.
                                                        // Does *not* construct an object of type T.

        void ReturnAddress( T* address );               // Return the given address back to the memory pool.
                                                        // Does *not* destruct the object located at that address.

        unsigned int MaxAllocated() const;              // Returns the highest number of simultaneously
                                                        // allocated addresses (the high-water mark).

        unsigned int CurAllocated() const;              // Returns the current number of allocated addresses.

        std::string GetStatusString() const;            // Get a summary of the current status of the BasicAllocator.

    private:
        unsigned int curNumAllocatedAddresses;          // The current number of simultaneously allocated addresses.
        unsigned int maxNumAllocatedAddresses;          // The highest number of simultaneously allocated addresses.
};

/*
 * Set the typename string.
 */
SetTypenameString_TemplateT( BasicAllocator );

/*
 * Pull in the implementation file as well, so we don't get linker errors when
 * only including the header file.
 */
#include "Allocator/BasicAllocator.inl"

/*
 * Also pull in some wrapper macros, since you're pretty much always going to use
 * them rather than the BasicAllocator directly.
 */
#include "Macros/MemoryAllocationMacros.hpp"

#endif // BASICALLOCATOR_HPP_INCLUDED
