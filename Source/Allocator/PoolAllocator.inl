/*
 * This file implements an implementation for a class-level memory allocator.
 *
 * It allocates and deallocates from a contiguous pool of pre-allocated memory
 * so objects of the same type will be allocated near each other in memory. In
 * theory this should improve cache-locality when iterating over all objects of
 * the given type (for example, when doing garbage collection). The
 * pre-allocated memory pool is of a fixed size, so only N addresses may be
 * simultaneously allocated at the same time.
 *
 * Author:      Stuart Rudderham
 * Created:     August 5, 2012
 * Last Edited: June 7, 2014
 */

#include "IO/Print.hpp"         // ioPrintln
#include "Macros/Macros.hpp"
#include "Debug/Crash.hpp"      // dbCrash
#include "Debug/Test.hpp"       // dbWarn
#include <cstring>              // memset
#include <new>                  // std::nothrow
#include <sstream>              // std::ostringstream

/*
 * Constructor.
 */
template<typename T, unsigned int N>
PoolAllocator<T, N>::PoolAllocator()
    : memoryPool( nullptr )
    , nextAddressToAllocate( nullptr )
{
    // Create the memory pool. This assumes that there are no special alignment
    // requirements.
    //
    // Uses the global "::operator new" since we don't actually want to
    // construct any objects, just allocate raw memory. Use the version that
    // doesn't throw an exception on failure since we check for NULL explictly.
    memoryPool = static_cast<T*>( ::operator new( sizeof(T) * N, std::nothrow ) );

    // Make sure we were actually able to allocate it.
    if( memoryPool == nullptr ) {
        dbCrash( "Unable to allocate memory pool of size ", sizeof(T) * N );
    }

    // Initialize the pool to an invalid bit-pattern, if desired.
    // Cast to void* to suppress compiler warnings.
    #if ENABLE_MEMORY_CLEARING
        memset( static_cast<void*>(memoryPool), 0xCD, sizeof(T) * N );
    #endif

    // We haven't allocated anything yet.
    nextAddressToAllocate = memoryPool;
}

/*
 * Destructor.
 */
template<typename T, unsigned int N>
PoolAllocator<T, N>::~PoolAllocator()
{
    // Emit a warning if there are still addresses allocated. The IAllocator
    // destructor should crash if we're leaking memory so this shouldn't
    // actually ever be triggered.
    dbWarn(
        CurAllocated() == 0,
        "Leaking memory in ", Typename<PoolAllocator<T, N>>()
    );

    // Free the memory pool.
    // Use "::operator delete" since pool was created with "::operator new".
    ::operator delete( memoryPool );
}

/*
 * Returns a memory address where an object of type T can be constructed.
 */
template<typename T, unsigned int N>
T* PoolAllocator<T, N>::GetAddress()
{
    // If we've already allocated all possible addresses then all we can do is crash.
    if( CurAllocated() >= N ) {
        dbCrash( "No free space in ", Typename<PoolAllocator<T, N>>() );
    }

    T* address;

    // If we have a free address within the high-water mark bounds then use that.
    if( freeList.empty() == false ) {
        address = freeList.top();
        freeList.pop();
    }
    // Otherwise we have to raise the high-water mark.
    else {
        address = nextAddressToAllocate;
        ++nextAddressToAllocate;
    }

    return address;
}

/*
 * Return the given address back to the memory pool.
 */
template<typename T, unsigned int N>
void PoolAllocator<T, N>::ReturnAddress( T* address )
{
    // Add the address to the free list.
    freeList.push( address );
}

/*
 * Returns the highest number of simultaneously allocated addresses
 * (the high-water mark).
 */
template<typename T, unsigned int N>
unsigned int PoolAllocator<T, N>::MaxAllocated() const
{
    return nextAddressToAllocate - memoryPool;
}

/*
 * Returns the current number of allocated addresses.
 */
template<typename T, unsigned int N>
unsigned int PoolAllocator<T, N>::CurAllocated() const
{
    return MaxAllocated() - freeList.size();
}

/*
 * Get a summary of the current status of the PoolAllocator.
 */
template<typename T, unsigned int N>
std::string PoolAllocator<T, N>::GetStatusString() const
{
    std::ostringstream stream;

    ioPrintln( stream, "    Memory Pool Size (Addresses)  : ", N                         );
    ioPrintln( stream, "    Memory Pool Size (Bytes)      : ", sizeof(T) * N             );
    ioPrintln( stream, "    Memory Pool Size (KB)         : ", sizeof(T) * N / 1024.f    );
    ioPrintln( stream, "    Memory Pool Size (MB)         : ", sizeof(T) * N / 1048576.f );
    ioPrintln( stream, "    Number Of Wasted Addresses    : ", N - MaxAllocated()        );

    return stream.str();
}
