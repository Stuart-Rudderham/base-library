/*
 * This file implements an implementation for a class-level memory allocator.
 *
 * It is a reference implementation that simply uses the regular "::operator new"
 * and "::operator delete" functions. There is nothing special about it other
 * than the debugging functionality that the IAllocator interface provides.
 *
 * Author:      Stuart Rudderham
 * Created:     September 14, 2013
 * Last Edited: June 7, 2014
 */

#include "IO/Print.hpp"         // ioPrintln
#include "Macros/Macros.hpp"
#include "Debug/Test.hpp"       // dbWarn
#include <algorithm>            // std::max
#include <sstream>              // std::ostringstream

/*
 * Constructor.
 */
template<typename T>
BasicAllocator<T>::BasicAllocator()
    : curNumAllocatedAddresses( 0u )
    , maxNumAllocatedAddresses( 0u )
{

}

/*
 * Destructor.
 */
template<typename T>
BasicAllocator<T>::~BasicAllocator()
{
    // Emit a warning if there are still addresses allocated. The IAllocator
    // destructor should crash if we're leaking memory so this shouldn't
    // actually ever be triggered.
    dbWarn(
        CurAllocated() == 0,
        "Leaking memory in ", Typename<BasicAllocator<T>>()
    );
}

/*
 * Returns a free memory address where an object of type T can be created.
 */
template<typename T>
T* BasicAllocator<T>::GetAddress()
{
    // Delegate to the global "::operator new" function.
    T* address = static_cast<T*>( ::operator new( sizeof(T) ) );

    // Update bookkeeping.
    ++curNumAllocatedAddresses;
    maxNumAllocatedAddresses = std::max( MaxAllocated(), CurAllocated() );

    return address;
}

/*
 * Marks the given address in the memory pool as free.
 */
template<typename T>
void BasicAllocator<T>::ReturnAddress( T* address )
{
    // Delegate to the global "::operator delete" function.
    ::operator delete( address );

    // Update bookkeeping.
    --curNumAllocatedAddresses;
}

/*
 * Returns the highest number of simultaneously allocated addresses
 * (the high-water mark).
 */
template<typename T>
unsigned int BasicAllocator<T>::MaxAllocated() const
{
    return maxNumAllocatedAddresses;
}

/*
 * Returns the current number of allocated addresses.
 */
template<typename T>
unsigned int BasicAllocator<T>::CurAllocated() const
{
    return curNumAllocatedAddresses;
}

/*
 * Get a summary of the current status of the BasicAllocator.
 */
template<typename T>
std::string BasicAllocator<T>::GetStatusString() const
{
    std::ostringstream stream;

    ioPrintln( stream, "    Max Memory Used (Bytes)       : ", MaxAllocated() * sizeof(T)             );
    ioPrintln( stream, "    Max Memory Used (KB)          : ", MaxAllocated() * sizeof(T) / 1024.f    );
    ioPrintln( stream, "    Max Memory Used (MB)          : ", MaxAllocated() * sizeof(T) / 1048576.f );

    return stream.str();
}
