/*
 * This header defines an implementation for a class-level memory allocator.
 *
 * It allocates and deallocates from a contiguous pool of pre-allocated memory
 * so objects of the same type will be allocated near each other in memory. In
 * theory this should improve cache-locality when iterating over all objects of
 * the given type (for example, when doing garbage collection). The
 * pre-allocated memory pool is of a fixed size, so only N addresses may be
 * simultaneously allocated at the same time.
 *
 * Author:      Stuart Rudderham
 * Created:     August 5, 2012
 * Last Edited: November 20, 2013
 */

#ifndef POOLALLOCATOR_HPP_INCLUDED
#define POOLALLOCATOR_HPP_INCLUDED

#include "Typename.hpp"
#include <stack>
#include <string>

template<typename T, unsigned int N>
class PoolAllocator
{
    public:
        PoolAllocator();                                // Constructor.  Allocates the memory pool.

        ~PoolAllocator();                               // Destructor. Checks for memory leaks before
                                                        // deallocating the memory pool.

        T* GetAddress();                                // Returns a memory address where an object of
                                                        // type T can be constructed.
                                                        // Does *not* construct an object of type T.

        void ReturnAddress( T* address );               // Return the given address back to the memory pool.
                                                        // Does *not* destruct the object located at that address.

        unsigned int MaxAllocated() const;              // Returns the highest number of simultaneously
                                                        // allocated addresses (the high-water mark).

        unsigned int CurAllocated() const;              // Returns the current number of allocated addresses.

        std::string GetStatusString() const;            // Get a summary of the current status of the PoolAllocator.

    private:
        T* memoryPool;                                  // A pointer to the big block of memory the PoolAllocator
                                                        // allocates from.

        T* nextAddressToAllocate;                       // The address that will be given the next time Allocate()
                                                        // is called, assuming we can't reuse an earlier address.

        std::stack<T*> freeList;                        // All the addresses that have been previously allocated and
                                                        // are now unused. These are all less than the high-water mark.
                                                        //
                                                        // Previously was a min-heap to try and keep allocated
                                                        // addresses contiguous but the overhead of updating the
                                                        // heap was too much.
};

/*
 * Set the typename string.
 */
SetTypenameString_TemplateTAndUInt( PoolAllocator );

/*
 * Pull in the implementation file as well, so we don't get linker errors when
 * only including the header file.
 */
#include "Allocator/PoolAllocator.inl"

/*
 * Also pull in some wrapper macros, since you're pretty much always going to use
 * them rather than the PoolAllocator directly.
 */
#include "Macros/MemoryAllocationMacros.hpp"

#endif // POOLALLOCATOR_HPP_INCLUDED
