#pragma once

/**
 * @file       Source/Preprocessor/Preprocessor.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       April 16, 2014
 * @date       Last Modified: May 16, 2014
 * @brief      A file that includes all classes, functions, and macros defined in the
 *             Preprocessor module.
 * @details    A convenience file for including the entire Preprocessor module with one
 *             command.
 */

#include "Preprocessor/BuildType.hpp"
#include "Preprocessor/CompilerInfo.hpp"
#include "Preprocessor/Misc.hpp"
