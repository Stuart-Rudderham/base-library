#pragma once

/**
 * @file       Source/Preprocessor/Misc.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       September 13, 2013
 * @date       Last Modified: May 16, 2014
 * @brief      Various useful macros.
 */

/**
 * @brief To stringify the result of a macro expansion we need another level of indirection.
 *        Code from here -> http://gcc.gnu.org/onlinedocs/cpp/Stringification.html
 */
#define STRINGIFY(s) STR(s)

/**
 * @brief Convert the argument to a string.
 */
#define STR(s) #s

/**
 * @brief Prevent a class from being copyable.
 */
#define NO_COPY(Type)                                       \
    Type( const Type& original ) = delete;

/**
 * @brief Prevent a class from being assignable.
 */
#define NO_ASSIGN(Type)                                     \
    Type& operator= ( const Type &original ) = delete;

/**
 * @brief Prevent a class from being copyable nor assignable.
 */
#define NO_COPY_OR_ASSIGN(Type)                             \
    NO_COPY(Type);                                          \
    NO_ASSIGN(Type);
