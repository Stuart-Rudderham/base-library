#pragma once

/**
 * @file       Source/Preprocessor/CompilerInfo.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       September 13, 2013
 * @date       Last Modified: May 16, 2014
 * @brief      Stores information related to the compiler used to build the program.
 * @details    Also defines some compiler-specific macros.
 */

#include "Preprocessor/Misc.hpp"    // STRINGIFY

/*
 * Detect what compiler is being used.
 *
 * We also define some common macros that each compiler implements
 * differently, such as FUNCTION_NAME which gets the name of the function
 * the macro is expanded in, similar to the __FILE__ and __LINE__ macros. We
 * don't want to use the standard __func__ as it doesn't print the full
 * function prototype.
 *
 * GCC detection must be last because many compilers like to define __GNUC__
 * to claim they have GCC compatibility (e.g. Clang).
 */
#if defined( _MSC_VER )
    #define USING_MSVC  1
    #define USING_CLANG 0
    #define USING_GCC   0

    #define COMPILER_NAME           "MSVC"
    #define COMPILER_VERSION        STRINGIFY(_MSC_FULL_VER)

    #define FUNCTION_NAME           __FUNCSIG__

#elif defined( __clang__ )
    #define USING_MSVC  0
    #define USING_CLANG 1
    #define USING_GCC   0

    #define COMPILER_NAME           "Clang"
    #define COMPILER_VERSION        __clang_version__

    #define FUNCTION_NAME           __PRETTY_FUNCTION__

#elif defined( __GNUC__ )
    #define USING_MSVC  0
    #define USING_CLANG 0
    #define USING_GCC   1

    #define COMPILER_NAME           "GCC"
    #define COMPILER_VERSION        __VERSION__

    #define FUNCTION_NAME           __PRETTY_FUNCTION__

#else
    #error "Couldn't detect a compiler version!"
#endif

/**
 * @brief   Get the compiler name and version used to build the file.
 */
#define COMPILER_INFO (COMPILER_NAME " " COMPILER_VERSION)

/**
 * @brief   Get the time and date the file was compiled.
 *
 * @details Pretty much the same as the standard __TIMESTAMP__ macro, but with slightly
 *          better formatting.
 */
#define COMPILE_TIME (__DATE__ " at " __TIME__)
