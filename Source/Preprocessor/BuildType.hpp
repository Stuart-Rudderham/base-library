#pragma once

/**
 * @file       Source/Preprocessor/BuildType.hpp
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       September 13, 2013
 * @date       Last Modified: May 16, 2014
 * @brief      Controls if the project is built in DEBUG or RELEASE mode.
 */

/*
 * The DEBUG_MODE and RELEASE_MODE macros should be checked with "#if" rather than "ifdef".
 * This avoids a silent failure in the case where a macro should be defined but isn't, for
 * example if this file is accidentally not included. Many compilers have a flag (-Wundef
 * on GCC) that will print a warning if an undefined macro is used in a #if statement.
 *
 * Another description of the problem is here -> http://ozlabs.org/~rusty/index.cgi/tech/2008-01-04.html
 */

/*
 * Detect if we're compiling in Debug or Release mode. If nothing is
 * specified then default to Debug mode.
 */
#if defined( DEBUG ) && defined ( RELEASE )
    #error "Debug mode (-DDEBUG) and Release mode (-DRELEASE) mode are mutually exclusive, please set just one."
#elif !defined( DEBUG ) && !defined( RELEASE )
    #define DEBUG_MODE              1
    #define RELEASE_MODE            0
#else
    #if defined ( DEBUG )
        #define DEBUG_MODE          1
    #else
        #define DEBUG_MODE          0
    #endif

    #if defined ( RELEASE )
        #define RELEASE_MODE        1
    #else
        #define RELEASE_MODE        0
    #endif
#endif

/*
 * Make sure we define one (and only one) of DEBUG_MODE or
 * RELEASE_MODE. This does an XNOR of the two macros.
 */
#if (DEBUG_MODE && RELEASE_MODE) || (!DEBUG_MODE && !RELEASE_MODE)
    #error "Define one (and only one) of DEBUG_MODE or RELEASE_MODE"
#endif
