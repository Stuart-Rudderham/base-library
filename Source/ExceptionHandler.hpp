/*
 * This header defines a function to setup the C++ runtime exception handling
 * functions to do something more useful than call abort() when faced with an
 * uncaught exception.
 *
 * Author:      Stuart Rudderham
 * Created:     October 6, 2013
 * Last Edited: June 7, 2014
 */

#ifndef EXCEPTIONHANDLER_HPP_INCLUDED
#define EXCEPTIONHANDLER_HPP_INCLUDED

#include "Debug/Crash.hpp"          // dbCrash
#include <exception>                // std::set_unexpected, std::set_terminate
#include <new>                      // std::set_new_handler

/*
 * If uncaught exceptions are thrown it can be a hassle to find the line that
 * threw them. This function sets the handler functions to crash in a way that a
 * debugger can hook into, so that it is easier to debug.
 *
 * The error message will have the wrong line number, since the __LINE__ macro is
 * expanded right here, as opposed to where the exception was thrown. To find
 * the correct line use the debugger to walk up the call stack, looking for
 * anything suspicious.
 */
inline void SetupExceptionHandler()
{
    // This will be "automatically called when a function throws an
    // exception that is not in its dynamic-exception-specification (i.e. in its
    // throw specifier)".
    //
    // http://www.cplusplus.com/reference/exception/set_unexpected/
    //
    // The default behavior was to call std::terminate().
    std::set_unexpected(
        []() {
            dbCrash("Unexected handler");
        }
    );

    // This will be called "when the exception handling process has to be
    // abandoned for some reason. This happens when no catch handler can be
    // found for a thrown exception, or for some other exceptional circumstance
    // that makes impossible to continue the exception handling process."
    //
    // http://www.cplusplus.com/reference/exception/set_terminate/
    //
    // The default behavior was to call abort().
    std::set_terminate(
        []() {
            dbCrash("Terminate handler");
        }
    );

    // This will be called when "the default allocation functions (operator new
    // and operator new[]) fail to allocate storage".
    //
    // http://www.cplusplus.com/reference/new/set_new_handler/
    //
    // The default behavior was to throw an std::bad_alloc exception.
    std::set_new_handler(
        []() {
            dbCrash("Failed to allocate memory");
        }
    );
}

#endif // EXCEPTIONHANDLER_HPP_INCLUDED
