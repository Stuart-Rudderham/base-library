/*
 * This header defines a base class that forces any class that inherits from it
 * to be a global singleton.
 *
 * Author:      Stuart Rudderham
 * Created:     August 28, 2012
 * Last Edited: June 30, 2014
 */

#ifndef SINGLETON_HPP_INCLUDED
#define SINGLETON_HPP_INCLUDED

#include "IO/Print.hpp"         // ioVerbose
#include "Macros/Macros.hpp"
#include "Debug/Crash.hpp"      // dbCrash
#include "Debug/Test.hpp"       // dbAssert
#include "Typename.hpp"
#include <type_traits>

/*
 * The templated singleton class.
 *
 * There are many different ways to make a singleton class, and each has its
 * advantages and disadvantages. The way this implementation works is that a
 * class will inherit from the Singleton<T> class using the Curiously Recurring
 * Template Pattern.
 *
 * Example use:
 *     class Foo : public Singleton<Foo> {
 *         ...
 *     };
 *
 * There are a few reasons for doing it this way.
 *     1) It forces the child class to be a true singleton. Regardless of how or
 *        where the object is created, using inheritance means that the
 *        Singleton<T> constructor will always be called, and the Singleton<T>
 *        constructor is the function that enforces the singleton requirement.
 *
 *     2) It makes it clear in the child class definition that the class is
 *        meant to be a global singleton.
 *
 * The singleton instance is a static variable in the Get() function. This
 * ensures that it is lazily created the first time it is used, avoiding the
 * static initialization problem. Some references on that issue are below.
 *
 *      http://www.parashift.com/c++-faq-lite/static-init-order.html
 *      http://www.parashift.com/c++-faq-lite/static-init-order-on-first-use.html
 *      http://www.parashift.com/c++-faq-lite/construct-on-first-use-v2.html
 *
 * This static variables then exists until the end of the program. Once the
 * singleton instance is created there is *no* way to destroy it manually. This
 * is a known disadvantage of this implementation, but one that hasn't caused
 * any trouble yet. It is also a simple fix to provide manual destruction,
 * simply replace the static variable with a static std::unique_ptr<T>. The use
 * of std::unique_ptr will ensure that the instance will be deleted at the end
 * of the program if it still exists, and the addition of a
 * Singleton<T>::Delete() would provide the ability to manually delete the
 * instance at any time.
 *
 * Another known disadvantage of using a static instance variable is that it can
 * suffer from the static *de-initialization* problem. If another static object
 * _doesn't_ access the Singleton in its constructor but _does_ access the
 * Singleton in its destructor then there will be a problem, because the
 * Singleton will be used after it has been destructed. There is an an assert
 * that will catch this issue (assuming asserts are enabled). Another
 * explanation can be found at the below link.
 *
 * http://stackoverflow.com/a/335746
 */
template<typename T>
class Singleton {
    public:
        /*
         * Getter for the Singleton instance.
         */
        static inline T& Get()
        {
            // Enable flag to signal that object is created using Get().
            CreatedWithGetFn = true;

            // Confirm that the Singleton instance isn't in the middle of
            // construction. This catches bugs like two Singletons accessing
            // the other in their constructors.
            dbAssert(
                Exists == false || FinishedConstruction == true,
                "Accessing Singleton before it has finished construction!"
            );

            // Local static variable initialization is thread-safe in C++11 so
            // we don't need any locks.
            static T Instance;

            // Enable flag to signal that the instance has finished construction.
            FinishedConstruction = true;

            // This assert enforces the constraint that Singleton<T> must be
            // inherited from.
            static_assert(
                std::is_base_of<Singleton<T>, T>::value == true,
                "Didn't inherit from Singleton!"
            );

            // Confirm that the Singleton<T> constructor ran properly, and that
            // the destructor hasn't been run yet.
            dbAssert(
                Exists == true,
                "Cannot Get(), Singleton has already been destructed!"
            );

            // Return the instance.
            return Instance;
        }

    /*
     * Constructor and destructor are marked "protected" as another way of
     * enforcing the inheritance constraint.
     */
    protected:
        Singleton()
        {
            dbAssert(
                CreatedWithGetFn == true,
                "Always use the Get() function to access the Singleton, "
                "never create an instance manually."
            );

            if( Exists == true ) {
                dbCrash( "Cannot create more than one instance of Singleton!" );
            }

            Exists = true;

            // This print statement technically shouldn't be in the Singleton constructor
            // because the instance isn't completely constructed until *all* the
            // constructors finish running, and the derived class constructor runs after
            // the Singleton one. It's not a big deal, but may result in some confusing
            // log output if one Singleton access another in its constructor. For example
            // if Bar::Bar() calls Foo::Get() the output would be:
            //
            //     Created Singleton<Bar>
            //     Created Singleton<Foo>
            //     Destroyed Singleton<Bar>
            //     Destroyed Singleton<Foo>
            //
            // In this case while Singleton<Bar>::Singleton<Bar>() ran before
            // Singleton<Foo>::Singleton<Foo>() the Foo instance is finishes construction
            // before the Bar instance, contrary to what the output shows.
            ioVerbose( "Created ", Typename<Singleton<T>>() );
        }

        ~Singleton()
        {
            dbAssert(
                Exists == true,
                "Cannot delete, Singleton doesn't 'Exist'. "
                "Shouldn't be possible."
            );

            Exists = false;

            // Similar to the explanation above, this print statement technically shouldn't
            // be in the Singleton destructor because an object is "destructed" the instant
            // any of the destructors run, and the derived class constructor is run before
            // the Singleton one. Again, not a huge deal but something to keep in mind.
            ioVerbose( "Destroyed ", Typename<Singleton<T>>() );
        }

    private:
        static bool Exists;                 // Flag used to detect if an instance already exists.

        static bool CreatedWithGetFn;       // Flag used to force creation with Get().

        static bool FinishedConstruction;   // Flag used to detect access to instance before
                                            // construction has completed.

        NO_COPY_OR_ASSIGN(Singleton);       // Can't copy a singleton, obviously.
};

/*
 * Having the variable in the header file is fine because of the way that
 * templates work, there will only be one copy per type T.
 */
template<typename T>
bool Singleton<T>::Exists = false;

template<typename T>
bool Singleton<T>::CreatedWithGetFn = false;

template<typename T>
bool Singleton<T>::FinishedConstruction = false;

/*
 * Store the Singleton type string.
 */
SetTypenameString_TemplateT( Singleton );

#endif // SINGLETON_HPP_INCLUDED

/*
 * Implementation that uses composition rather than inheritance.
 */
#if 0
template<typename T>
class Singleton
{
    public:
        static T& Get()
        {
            static Instance mInstance;
            return mInstance.Get();
        }

    private:
        class Instance
        {
            public:
                Instance()
                {
                    // Sanity check.
                    dbAssert(
                        mFinishedConstruction == false,
                        "Singleton::Instance<~> constructor has already been run!", Typename<T>()
                    );

                    // Order of operations is:
                    //     1) mInstance finishes construction
                    //     2) Set flag
                    //     3) Print message
                    //
                    // This is the reverse of the order for destruction.
                    mFinishedConstruction = true;
                    ioVerbose( "Singleton::Instance<~> constructor completed", Typename<T>() );
                }

                ~Instance()
                {
                    // Sanity check.
                    dbAssert(
                        mStartedDestruction == false,
                        "Singleton::Instance<~> destructor has already been run!", Typename<T>()
                    );

                    // Order of operations is:
                    //     1) Print message
                    //     2) Set flag
                    //     3) mInstance starts destruction
                    //
                    // This is the reverse of the order for construction.
                    ioVerbose( "Singleton::Instance<~> destructor started", Typename<T>() );
                    mStartedDestruction = true;
                }

                T& Get()
                {
                    // Sanity checks.
                    dbAssert(
                        mFinishedConstruction == true,
                        "Cannot access Singleton::Instance<~> before constructor has completed!", Typename<T>()
                    );

                    dbAssert(
                        mStartedDestruction == false,
                        "Cannot access Singleton::Instance<~> after destructor has started!", Typename<T>()
                    );

                    return mInstance;
                }

            private:
                T mInstance;

                static bool mFinishedConstruction;
                static bool mStartedDestruction;
        };
};

// These don't suffer from the static initialization problem
//     -> http://stackoverflow.com/a/16088748
//     -> http://en.cppreference.com/w/cpp/language/constant_initialization
//     -> http://akrzemi1.wordpress.com/2012/05/27/constant-initialization/
template<typename T>
bool Singleton<T>::Instance::mFinishedConstruction = false;

template<typename T>
bool Singleton<T>::Instance::mStartedDestruction = false;
#endif