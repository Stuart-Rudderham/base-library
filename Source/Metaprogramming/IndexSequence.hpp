#pragma once

/**
 * @file       Source/Metaprogramming/IndexSequence.hpp
 *
 * @author     Stuart Rudderham <smr85@rogers.com>
 * @date       Created:       Feburary 10, 2014
 * @date       Last Modified: June 8, 2014
 *
 * @brief      Template metaprogramming to create a sequence of integers from from [0, N).
 *
 * @details    When dealing with template metaprogramming is can be useful in certain situations
 *             to be able to generate a sequence of numbers (e.g. when expanding a tuple). This
 *             functionality has been added into the C++14 standard under the name
 *             std::integer_sequence but until it is widely available a custom implementation must
 *             be used.
 *
 *             References
 *                 http://en.cppreference.com/w/cpp/utility/integer_sequence, http://stackoverflow.com/a/12707595
 *
 * @todo       Remove and replace with std::integer_sequence when C++14 is available.
 */

/**
 * @brief   A simple sequence of non-negative numbers.
 *
 * @details The numbers are intrinsic to the type so they aren't actually stored anywhere, but can
 *          be inferred when doing template type inference.
 */
template<unsigned int... Indices>
struct IndexSequence
{

};

/**
 * @brief   A helper struct to generate an IndexSeqeuence from [0, N).
 *
 * @details The type recurses until N = 0, adding the value N - 1 to the beginning
 *          of the sequence each iteration.
 *
 * @todo    Currently this creates a linear number of template instantiations, which means
 *          that it will overflow when N > ~900. Could be reworked to be logarithmic.
 */
template<unsigned int N, unsigned int... Indices>
struct MakeIndexSequence
{
	using Sequence = typename MakeIndexSequence<N - 1, N - 1, Indices...>::Sequence;
};

/**
 * @brief The base case for MakeIndexSequence.
 */
template<unsigned int... Indices>
struct MakeIndexSequence<0, Indices...>
{
	using Sequence = IndexSequence<Indices...>;
};
